﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ScreenshotManager : MonoBehaviour {

	public bool now = false;
	public string path;
	public bool finished = false;

	void OnPostRender()
	{
		if (now) {
			Texture2D screenshot = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, true);
			screenshot.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);
			screenshot.Apply ();
			
			Texture2D newScreenshot = new Texture2D (screenshot.width / 8, screenshot.height / 8);
			newScreenshot.SetPixels (screenshot.GetPixels (3));
			newScreenshot.Apply ();
			
			byte[] bytes = newScreenshot.EncodeToPNG ();
			string localURL = path;
			File.WriteAllBytes (localURL, bytes);

			now = false;
			finished = true;
		}
	}
}
