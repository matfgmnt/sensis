﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FGMNT {

	public class DatabaseManager : MonoBehaviour {

		private DatabaseEntry[] dbEntries;

		void Awake()
		{
			
			dbEntries = GetComponentsInChildren<DatabaseEntry>();
		}

		public List<DatabaseEntry> findBusinessInfo(string metaData)
		{
			List<DatabaseEntry> result = new List<DatabaseEntry>();

			foreach(DatabaseEntry dbEntry in dbEntries)
			{
				Debug.Log(metaData + "==" + dbEntry);
				if(dbEntry.SensisID.ToLower() == metaData.ToLower())
				{
					result.Add(dbEntry);
				} 
			}
			return result;
		}
	}
}