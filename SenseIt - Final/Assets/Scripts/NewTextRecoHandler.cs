using UnityEngine;
using System.Collections;
using Vuforia;

namespace FGMNT {

	public class NewTextRecoHandler : MonoBehaviour, ITextRecoEventHandler {
	
	    private ApplicationManager applicationManager;
	    private TextRecoBehaviour mTextRecoBehaviour;
		private TextTracker textTracker;

	    void Awake()
	    {
	        applicationManager = GameObject.FindObjectOfType<ApplicationManager>();
			//textTracker = GameObject.FindObjectOfType < TextTracker> ();
	    }
	

		void Start () {
	
	        mTextRecoBehaviour = GetComponent<TextRecoBehaviour>();
	        if (mTextRecoBehaviour)
	        {
	            mTextRecoBehaviour.RegisterTextRecoEventHandler(this);
	        }
		}
	
	    public void OnInitialized()
	    {
			//textTracker.SetRegionOfInterest(new Rect(0, 0, Screen.width, Screen.height), new Rect(0, 0, Screen.width, Screen.height), "REGIONOFINTEREST_UP_IS_0_HRS");
			Debug.Log ("Text Reco initialized");
	    }
	    public void OnInitError(TargetFinder.InitState initError)
	    {
	        Debug.Log("Text Reco init error " + initError.ToString());
	    }
	    public void OnUpdateError(TargetFinder.UpdateState updateError)
	    {
	        Debug.Log("Text Reco update error " + updateError.ToString());
	    }
	
		public void OnWordDetected(WordResult wordResult)
		{
	        mTextRecoBehaviour.enabled = false;
	        applicationManager.LogoFound(wordResult.Word.StringValue, false);
	    }

	    public void OnWordLost(Word word)
	    {

	    }
	}
}