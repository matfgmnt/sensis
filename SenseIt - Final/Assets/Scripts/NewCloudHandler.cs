﻿using UnityEngine;
using System.Collections;
using Vuforia;

namespace FGMNT {

	public class NewCloudHandler : MonoBehaviour, ICloudRecoEventHandler {
	
	    private ApplicationManager applicationManager;
	    private CloudRecoBehaviour mCloudRecoBehaviour;
	    private bool mIsScanning = false;
	    private string mTargetMetaData = "";
	
	    void Awake()
	    {
	        applicationManager = GameObject.FindObjectOfType<ApplicationManager>();
	    }
	
		// Use this for initialization
		void Start () {
	
	        mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
	        if (mCloudRecoBehaviour)
	        {
	            mCloudRecoBehaviour.RegisterEventHandler(this);
	        }
		}
	
	    public void OnInitialized()
	    {
			Debug.Log ("Cloud Reco initialized");
	    }
	    public void OnInitError(TargetFinder.InitState initError)
	    {
	        Debug.Log("Cloud Reco init error " + initError.ToString());
	    }
	    public void OnUpdateError(TargetFinder.UpdateState updateError)
	    {
	        Debug.Log("Cloud Reco update error " + updateError.ToString());
	    }
	
	    public void OnStateChanged(bool scanning)
	    {
	        mIsScanning = scanning;
	        if (scanning)
	        {
	            // clear all known trackables
	            ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
	            tracker.TargetFinder.ClearTrackables(false);
	        }
	    }
	
	    // Here we handle a cloud target recognition event
	    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
	    {
	        // do something with the target metadata
	        mTargetMetaData = targetSearchResult.MetaData;
	        // stop the target finder (i.e. stop scanning the cloud)
	        mCloudRecoBehaviour.CloudRecoEnabled = false;
	        applicationManager.LogoFound(mTargetMetaData, true);
	    }
	}
}