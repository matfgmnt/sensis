﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

namespace FGMNT {
	
	public class LocationManager : MonoBehaviour {

		public Vector2 currentPosition = new Vector2(0f,0f);

		IEnumerator Start()
    	{
			while(true)
			{
    	    	// First, check if user has location service enabled
    	    	if (!Input.location.isEnabledByUser)
				{
    	    	    yield break;
				}
				
    	    	// Start service before querying location
				Input.location.Start();
				
    	    	// Wait until service initializes
    	    	int maxWait = 20;
    	    	while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
    	    	{
    	    	    yield return new WaitForSeconds(1);
    	    	    maxWait--;
    	    	}
				
    	    	// Service didn't initialize in 20 seconds
    	    	if (maxWait < 1)
				{
    	    	    yield break;
    	    	}
				
    	    	// Connection has failed
    	    	if (Input.location.status == LocationServiceStatus.Failed)
				{
    	    	    yield break;
    	    	}
    	    	else
				{
					// Access granted and location value could be retrieved
					currentPosition = new Vector2 (Input.location.lastData.latitude, Input.location.lastData.longitude);
    	    	}
				
    	    	// Stop service if there is no need to query location updates continuously
    	    	Input.location.Stop();

				yield return new WaitForSeconds (300);
			}
    	}
	
		public DatabaseEntry FindNearestLocation(List<DatabaseEntry> businesses)
		{
			foreach(DatabaseEntry business in businesses)
			{
				business.distance = Vector2.Distance(currentPosition, new Vector2(business.location.x, business.location.y));
			}

			businesses = businesses.OrderBy(x => x.distance).ToList();

			return businesses[0];
		}
	}
}