﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Collections;
using System.Text;
using System.Net.Security;
using System.ComponentModel;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Globalization;

namespace FGMNT 
{
	public class FeedbackManager : MonoBehaviour {
		
		public bool returned = false;
		public GameObject waitingScreen;
		public Text errorMessage;

		private static bool emailError = false;
		private static bool sentMail = false;
		private bool screenCapture;
		private Texture2D tex;
		private bool source = false;
		private ApplicationManager appManager;
		private LocationManager gps;
		private DatabaseEntry business;
		private String function;
		private MailAddress sender;
		private string recipient;
		private ICredentialsByHost credentials;
		private SmtpClient smtpClient;
		private int port;
		private string path;

		void Start()
		{
			waitingScreen.SetActive (false);
			appManager = FindObjectOfType<ApplicationManager> ();
			gps = FindObjectOfType<LocationManager> ();
			sender = new MailAddress ("wpfigment@gmail.com", "White Pages - Figment");
			recipient = "mathieu@fgmnt.tech, whitepagesvr@sensis.com.au";
			credentials = new System.Net.NetworkCredential ("wpfigment@gmail.com", "trisp1103") as ICredentialsByHost;
			smtpClient = new SmtpClient ("smtp.gmail.com");
			port = 587;
		}

		void Update()
		{
			if (sentMail == true) {
				waitingScreen.SetActive (false);
				appManager.StartSearching ();
				sentMail = false;

				if (emailError == false) {
					errorMessage.color = Color.white;
					errorMessage.text = "Email successfully sent, thank you!";
					DeleteAllFiles ();
				} else 
				{
					errorMessage.color = Color.red;
					errorMessage.text = "Couldn't send Email, please check your connection";
				}

				errorMessage.gameObject.SetActive (true);
			}
		}

		public void PositiveFeedBack(bool reco, DatabaseEntry dbEntry)
		{
			source = reco;
			business = dbEntry;
			SaveInformation (1);
		}

		public void NegativeFeedBack()
		{
			SaveInformation (2);
		}

		public void ActionEmail(string action)
		{
			function = action;
			SaveInformation (0);
		}

		public void TakeScreenshot(ScreenshotManager scManager)
		{
			DirectoryInfo dirInf = new DirectoryInfo(Application.persistentDataPath + "/screenshots");
			if(!dirInf.Exists)
			{
				dirInf.Create(); 
			}

			path = string.Format ("{0}/screenshots/screenshot_{1}.png", 
				Application.persistentDataPath,
				System.DateTime.Now.ToString ("yyyy-MM-dd_HH-mm-ss"));

			scManager.path = path;
			scManager.now = true;
		}

		private void Savecsv(string[] message) 
		{
			string filePath = Application.persistentDataPath + "/" + SystemInfo.deviceUniqueIdentifier + ".csv";  
			string delimiter = ",";  

			if (!File.Exists(filePath))
			{
				string createText = "";
				File.WriteAllText(filePath, createText);
			}

			string readText = File.ReadAllText(filePath);
 
			StringBuilder sb = new StringBuilder();  
			sb.AppendLine(string.Join(delimiter, message)); 

			readText += sb.ToString();

			File.WriteAllText(filePath, readText);                 
		}
			
		private void SaveInformation (int positive)
		{
			ScreenshotManager scManager = FindObjectOfType<ScreenshotManager> ();
			string[] message;

			if(positive == 0) 
			{
				message = new string[]{	
					"3", 
					DateTime.Now.ToString(),
					function, 
					business.contactName,
					String.Format ("{0:F5}, {1:F5}", gps.currentPosition.x, gps.currentPosition.y),
					SystemInfo.deviceUniqueIdentifier
				};
			}
			else 
			{
				TakeScreenshot (scManager);
				
				if (positive == 1) {
					string src = source ? "Logo" : "Text";
					message = new string[] {	
						"1",
						DateTime.Now.ToString(), 
						src,
						business.contactName,
						business.nearestLocation, 
						String.Format ("{0:F5}, {1:F5}", gps.currentPosition.x, gps.currentPosition.y),
						SystemInfo.deviceUniqueIdentifier
					};
				} else {
					message = new string[] {	
						"2",
						DateTime.Now.ToString(),
						String.Format ("{0:F5}, {1:F5}", gps.currentPosition.x, gps.currentPosition.y),
						SystemInfo.deviceUniqueIdentifier
					};
				}
			}


			Savecsv (message);
			returned = true;
		}

		private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
		{;
			if (e.Cancelled || e.Error != null)
			{
				emailError = true;
			} 
			else
			{
				Debug.Log ("Email Sent");
			}

			sentMail = true;
		}
		
		public void SendEmail()
		{
			Debug.Log ("Sending Email");
		
			MailMessage mail = new MailMessage ();
			AlternateView avHtml = AlternateView.CreateAlternateViewFromString("Here is my feedback", null,"text/html");
		
			mail.AlternateViews.Add (avHtml);
			mail.Subject = "New SenseIt report from " + SystemInfo.deviceUniqueIdentifier;

			string[] myFiles = Directory.GetFiles (Application.persistentDataPath + "/screenshots");
			if (File.Exists (Application.persistentDataPath + "/" + SystemInfo.deviceUniqueIdentifier + ".csv")) {
				LinkedResource csvFile = new LinkedResource (Application.persistentDataPath + "/" + SystemInfo.deviceUniqueIdentifier + ".csv", "text/csv");
			
				csvFile.ContentId = Guid.NewGuid ().ToString ();
				csvFile.ContentType.Name = "data.csv";
				avHtml.LinkedResources.Add (csvFile);
				mail.Body = String.Format (@"<img src=""cid:{0}"" />", csvFile.ContentId);
			} else 
			{
				errorMessage.color = Color.red;
				errorMessage.text = "No report found";
				errorMessage.gameObject.SetActive (true);
				appManager.StartSearching ();
				return;
			}

			LinkedResource[] inline = new LinkedResource[myFiles.Length];

			for(int i=0; i<myFiles.Length; i++)
			{
				inline[i] = new LinkedResource(myFiles[i], "image/png");
				inline [i].ContentType.Name = Path.GetFileName(myFiles [i]);
				inline[i].ContentId = Guid.NewGuid ().ToString ();
				avHtml.LinkedResources.Add (inline[i]);
				mail.Body += String.Format (@"<img src=""cid:{0}"" />", inline[i].ContentId);
			}

			mail.IsBodyHtml = true;
			mail.From = sender;
			mail.To.Add(recipient);
		
			waitingScreen.SetActive (true);
			emailError = false;
			sentMail = false;
		
			SmtpClient smtpServer = smtpClient;
			smtpServer.Port = port;
			smtpServer.Credentials = credentials;
			smtpServer.EnableSsl = true;
			ServicePointManager.ServerCertificateValidationCallback = 
				delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors){ return true; };
			smtpServer.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
			smtpServer.SendAsync(mail, "myToken");
		}

		public void DeleteAllFiles()
		{
			System.IO.DirectoryInfo picsDir = new DirectoryInfo (Application.persistentDataPath + "/screenshots");
			foreach (FileInfo file in picsDir.GetFiles())
			{
				file.Delete(); 
			}
			File.Delete (Application.persistentDataPath + "/" + SystemInfo.deviceUniqueIdentifier + ".csv");
		}
	}
}