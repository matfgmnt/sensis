﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace FGMNT {

	public class ContactManager : MonoBehaviour {
	
		public Text name;
		public Image logo;
		public Text callText;
		public Text findText;
		public Text mapText;
		private string number;
		private string website;
		private string address;
		private FeedbackManager fbManager;

		void Start()
		{
			fbManager = FindObjectOfType<FeedbackManager> ();
		}

		public void UpdateContact(DatabaseEntry business)
		{
			name.text = business.contactName;
			logo.sprite = business.contactLogo;
			number = business.contactPhoneNumber1;
			website = business.contactWebsite1;
			address = System.Uri.EscapeUriString(business.contactName + " " + business.nearestLocation);
			callText.text = "Call " + business.contactName;
			findText.text = "Find " + business.contactName;
			mapText.text = "Get Directions";
		}

		public void Call()
		{
			StartCoroutine(ExecuteAction("Call", "tel://" + number));
		}

		public void Website()
		{
			StartCoroutine(ExecuteAction("Contact", "http://" + website));
		}

		public void Nearest()
		{
			StartCoroutine(ExecuteAction("Map", "http://maps.google.com.au/maps?q="+ address));
		}

		IEnumerator ExecuteAction(string function, string target)
		{
			fbManager.returned = false;
			fbManager.ActionEmail (function);

			yield return new WaitUntil (() => fbManager.returned == true);

			Application.OpenURL (target);
		}
	}
}


//function SwitchToEmailMessage() {
//	var emailAddress : String = currentActiveDbEntry.contactEmailAddress1;
//	print ("Creating email to : " + emailAddress);
//	var subject = "Shhhhh -Awesome";
//	var body = "Shhhhh - This is the body text";
//	
//	subject = subject.Replace(" ","%20");
//	body = body.Replace(" ","%20");
//	
////This works 	Application.OpenURL("mailto:adrian%40canopyvfx.com?subject=test%20from%20app&body=emailbody");			
//	Application.OpenURL("mailto:" + emailAddress + "?subject=" +  subject + "&body=" + body);	
//
//}