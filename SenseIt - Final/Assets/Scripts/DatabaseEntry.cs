﻿using UnityEngine;
using System.Collections;

namespace FGMNT {

	public class DatabaseEntry : MonoBehaviour {

		public string SensisID;
		public string contactName;
		public Sprite contactLogo;
		public string contactPhoneNumber1;
		public string contactPhoneNumber2;
		public string contactWebsite1;
		public Sprite contactWebsiteFakeImage;
		public string contactEmailAddress1;
		public string facebookAddress;
		public string nearestLocation;
		public string promotionText;
		public Sprite promotionImage;
		public string twitterAddress;
		public Vector3 location;
		public float distance;
	}
}