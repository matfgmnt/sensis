﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace FGMNT {

	public class ApplicationManager : MonoBehaviour {
		private ApplicationGUIManager appGUIManager;
		private DatabaseManager dbManager;
		private LocationManager locationManager;
		private FeedbackManager fbManager;

		void Awake(){
			appGUIManager = GetComponentInChildren<ApplicationGUIManager>();
			dbManager = GetComponentInChildren<DatabaseManager>();
			locationManager = GetComponent<LocationManager>();
			fbManager = GetComponent<FeedbackManager> ();
			GameObject.DontDestroyOnLoad(gameObject);
		}

		public void StartSearching()
		{
			SceneManager.LoadScene(1);
			appGUIManager.showSearchScreen();

		}
	
		public void LogoFound(string metaData, bool reco) 
		{
			StartCoroutine(ProcessResults(metaData, reco));
		}

		public void SendFeedback()
		{
			Debug.Log ("Send Feedback");
			fbManager.NegativeFeedBack();
		}

		public void InfoFeedback()
		{
			SceneManager.LoadScene(3);
			appGUIManager.ShowFeedbackScreen();
		}

		IEnumerator ProcessResults(string metaData, bool reco)
		{
			var results = dbManager.findBusinessInfo(metaData);

			if(results.Count != 0)
			{
				DatabaseEntry business;

				if(results.Count == 1)
				{
					business = results[0];
				}
				else
				{
					business = locationManager.FindNearestLocation(results);
				}

				fbManager.returned = false;
				fbManager.PositiveFeedBack (reco, business);

				yield return new WaitUntil (() => fbManager.returned == true);

				SceneManager.LoadScene(2);
				appGUIManager.showContactScreen(business);

			}
			else
			{
				StartSearching();
				SceneManager.LoadScene (1);
			}
		}
	}
}