﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace FGMNT {

	public class ApplicationGUIManager : MonoBehaviour {

		public GameObject searchScreen;
		public GameObject contactScreen;
		public GameObject introScreen;
		public GameObject websiteScreen;
		public GameObject waitingScreen;
		public GameObject feedbackScreen;
		public Text feedbackText;
		public ContactManager contactManager;
	
		void Awake() {
			searchScreen.SetActive(false);
			contactScreen.SetActive(false);
			introScreen.SetActive(true);
			websiteScreen.SetActive(false);
			waitingScreen.SetActive (false);
			feedbackScreen.SetActive (false);
			feedbackText.gameObject.SetActive (false);
		}

		public void showSearchScreen() 
		{
			introScreen.SetActive(false);
			searchScreen.SetActive(true);
			contactScreen.SetActive(false);
			waitingScreen.SetActive (false);
			feedbackScreen.SetActive (false);
		}
		
		public void showContactScreen(DatabaseEntry business) 
		{
			searchScreen.SetActive(false);
			contactScreen.SetActive(true);
			contactManager.UpdateContact(business);
			waitingScreen.SetActive (false);
			feedbackScreen.SetActive (false);
		}

		public void ShowFeedbackScreen()
		{
			introScreen.SetActive(false);
			searchScreen.SetActive(false);
			contactScreen.SetActive(false);
			waitingScreen.SetActive (false);
			feedbackScreen.SetActive (true);
		}

		public void EraseFeedbackText()
		{
			feedbackText.text = "";
			feedbackText.gameObject.SetActive (false);
		}
	}
}                                                                                                                                                                                                                                                                                                    