﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Collections;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace FGMNT 
{
	public class FeedbackManagerOld : MonoBehaviour {
	
		private Texture2D tex;
		private bool sendPositiveFeedback = false;
		private bool sendNegativeFeedback = false;
		private bool source = false;

		void OnPostRender () 
     	{
			if(sendPositiveFeedback == true)
			{
				ScreenCapture();
				SendPositiveEmail();
				sendPositiveFeedback = false;
			}

			if(sendNegativeFeedback == true)
			{
				ScreenCapture();
				SendNegativeEmail();
				sendNegativeFeedback = false;
			}
		}

		public void PositiveFeedBack(bool reco)
		{
			source = reco;
			sendPositiveFeedback = true;
		}

		public void NegativeFeedBack()
		{
			sendNegativeFeedback = true;
		}
	
		private void ScreenCapture()
		{
			Application.CaptureScreenshot("screenshot.png");
			Debug.Log("Screen has been captured!");
		}

		private void SendNegativeEmail()
		{
			string htmlBody = "<html><body><h1>Picture</h1><br>A user couldn't scan this logo/text:<br><img src=\"cid:filename\"></body></html>";
 			AlternateView avHtml = AlternateView.CreateAlternateViewFromString(htmlBody, null,"text/html");
 			
			LinkedResource inline = new LinkedResource(Application.persistentDataPath + "/screenshot.png", "image/png");
 			inline.ContentId = Guid.NewGuid().ToString();
 			avHtml.LinkedResources.Add(inline);
 			
 			MailMessage mail = new MailMessage();
 			mail.AlternateViews.Add(avHtml);
 			
			//Attachment att = new Attachment(Application.persistentDataPath + "/screenshot.png");
 			//att.ContentDisposition.Inline = true;

   			mail.From = new MailAddress("mathieu@fgmnt.tech", "Mathieu Reysset");
   			mail.To.Add("mathieu@fgmnt.tech");
			mail.Subject = "Tester Feedback";
			mail.Body = String.Format ("<h3>A user has tried to scan this logo:</h3>" +@"<img src=""cid:{0}"" />", inline.ContentId);
 			
 			mail.IsBodyHtml = true;
 			//mail.Attachments.Add(att);

   			SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
   			smtpServer.Port = 587;
   			smtpServer.Credentials = new System.Net.NetworkCredential("mathieu@fgmnt.tech", "trisi1103") as ICredentialsByHost;
   			smtpServer.EnableSsl = true;
   			ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors){ return true; };
   			smtpServer.SendAsync(mail, "test");
		}

		private void SendPositiveEmail()
		{
			string src;
			if(source)
			{
				src = "Logo";
			} else {
				src = "Text";
			}

			string htmlBody = "<html><body><h1>Picture</h1><br>A user successfully scanned this " + src + " <br><img src=\"cid:filename\"></body></html>";
			AlternateView avHtml = AlternateView.CreateAlternateViewFromString(htmlBody, null,"text/html");

			LinkedResource inline = new LinkedResource(Application.persistentDataPath + "/screenshot.png", "image/png");
			inline.ContentId = Guid.NewGuid().ToString();
			avHtml.LinkedResources.Add(inline);

			MailMessage mail = new MailMessage();
			mail.AlternateViews.Add(avHtml);

			//Attachment att = new Attachment(Application.persistentDataPath + "/screenshot.png");
			//att.ContentDisposition.Inline = true;

			mail.From = new MailAddress("mathieu@fgmnt.tech", "Mathieu Reysset");
			mail.To.Add("mathieu@fgmnt.tech");
			mail.Subject = src + " recognised!";
			mail.Body = String.Format ("<h3>A user has tried to scan this logo:</h3>" +@"<img src=""cid:{0}"" />", inline.ContentId);

			mail.IsBodyHtml = true;
			//mail.Attachments.Add(att);

			SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
			smtpServer.Port = 587;
			smtpServer.Credentials = new System.Net.NetworkCredential("mathieu@fgmnt.tech", "trisi1103") as ICredentialsByHost;
			smtpServer.EnableSsl = true;
			ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors){ return true; };
			smtpServer.SendAsync(mail, "test");
		}
	}
}