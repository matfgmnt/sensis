﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Net.Mail.CCredentialsByHost
struct CCredentialsByHost_t1812000798;
// System.String
struct String_t;
// System.Net.NetworkCredential
struct NetworkCredential_t1714133953;

// System.Void System.Net.Mail.CCredentialsByHost::.ctor(System.String,System.String)
extern "C"  void CCredentialsByHost__ctor_m3332169495 (CCredentialsByHost_t1812000798 * __this, String_t* ___userName0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkCredential System.Net.Mail.CCredentialsByHost::GetCredential(System.String,System.Int32,System.String)
extern "C"  NetworkCredential_t1714133953 * CCredentialsByHost_GetCredential_m2694647239 (CCredentialsByHost_t1812000798 * __this, String_t* ___host0, int32_t ___port1, String_t* ___authenticationType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
