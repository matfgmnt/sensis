﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"

// FGMNT.FeedbackManagerOld
struct FeedbackManagerOld_t108831159;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;

// System.Void FGMNT.FeedbackManagerOld::.ctor()
extern "C"  void FeedbackManagerOld__ctor_m912015608 (FeedbackManagerOld_t108831159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManagerOld::OnPostRender()
extern "C"  void FeedbackManagerOld_OnPostRender_m4203984613 (FeedbackManagerOld_t108831159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManagerOld::PositiveFeedBack(System.Boolean)
extern "C"  void FeedbackManagerOld_PositiveFeedBack_m801128275 (FeedbackManagerOld_t108831159 * __this, bool ___reco0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManagerOld::NegativeFeedBack()
extern "C"  void FeedbackManagerOld_NegativeFeedBack_m439369298 (FeedbackManagerOld_t108831159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManagerOld::ScreenCapture()
extern "C"  void FeedbackManagerOld_ScreenCapture_m3145750846 (FeedbackManagerOld_t108831159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManagerOld::SendNegativeEmail()
extern "C"  void FeedbackManagerOld_SendNegativeEmail_m110902849 (FeedbackManagerOld_t108831159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManagerOld::SendPositiveEmail()
extern "C"  void FeedbackManagerOld_SendPositiveEmail_m3725995949 (FeedbackManagerOld_t108831159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FGMNT.FeedbackManagerOld::<SendNegativeEmail>m__0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool FeedbackManagerOld_U3CSendNegativeEmailU3Em__0_m1195385285 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___s0, X509Certificate_t283079845 * ___certificate1, X509Chain_t777637347 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FGMNT.FeedbackManagerOld::<SendPositiveEmail>m__1(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool FeedbackManagerOld_U3CSendPositiveEmailU3Em__1_m4041484846 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___s0, X509Certificate_t283079845 * ___certificate1, X509Chain_t777637347 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
