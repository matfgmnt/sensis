﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_OpFlags378191910.h"
#include "System_System_Text_RegularExpressions_Position3781184359.h"
#include "System_System_Text_RegularExpressions_BaseMachine4008011478.h"
#include "System_System_Text_RegularExpressions_FactoryCache2051534610.h"
#include "System_System_Text_RegularExpressions_FactoryCache_655155419.h"
#include "System_System_Text_RegularExpressions_MRUList33178162.h"
#include "System_System_Text_RegularExpressions_MRUList_Node1107172180.h"
#include "System_System_Text_RegularExpressions_CaptureColle1671345504.h"
#include "System_System_Text_RegularExpressions_Capture4157900610.h"
#include "System_System_Text_RegularExpressions_CILCompiler1740644799.h"
#include "System_System_Text_RegularExpressions_CILCompiler_F997927490.h"
#include "System_System_Text_RegularExpressions_Category1984577050.h"
#include "System_System_Text_RegularExpressions_CategoryUtil3840220623.h"
#include "System_System_Text_RegularExpressions_LinkRef2090853131.h"
#include "System_System_Text_RegularExpressions_InterpreterFa556462562.h"
#include "System_System_Text_RegularExpressions_PatternCompil637049905.h"
#include "System_System_Text_RegularExpressions_PatternCompi3979537293.h"
#include "System_System_Text_RegularExpressions_PatternCompi3337276394.h"
#include "System_System_Text_RegularExpressions_LinkStack954792760.h"
#include "System_System_Text_RegularExpressions_Mark2724874473.h"
#include "System_System_Text_RegularExpressions_GroupCollecti939014605.h"
#include "System_System_Text_RegularExpressions_Group3761430853.h"
#include "System_System_Text_RegularExpressions_Interpreter3731288230.h"
#include "System_System_Text_RegularExpressions_Interpreter_I273560425.h"
#include "System_System_Text_RegularExpressions_Interpreter_1827616978.h"
#include "System_System_Text_RegularExpressions_Interpreter_2395763083.h"
#include "System_System_Text_RegularExpressions_Interval2354235237.h"
#include "System_System_Text_RegularExpressions_IntervalColl4130821325.h"
#include "System_System_Text_RegularExpressions_IntervalColl1928086041.h"
#include "System_System_Text_RegularExpressions_IntervalColl1824458113.h"
#include "System_System_Text_RegularExpressions_MatchCollect3718216671.h"
#include "System_System_Text_RegularExpressions_MatchCollecti501456973.h"
#include "System_System_Text_RegularExpressions_Match3164245899.h"
#include "System_System_Text_RegularExpressions_Syntax_Parse2756028923.h"
#include "System_System_Text_RegularExpressions_QuickSearch1036078825.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"
#include "System_System_Text_RegularExpressions_RxInterprete2459337652.h"
#include "System_System_Text_RegularExpressions_RxInterprete3288646651.h"
#include "System_System_Text_RegularExpressions_RxInterpreter834810884.h"
#include "System_System_Text_RegularExpressions_RxLinkRef275774985.h"
#include "System_System_Text_RegularExpressions_RxCompiler4215271879.h"
#include "System_System_Text_RegularExpressions_RxInterprete1812879716.h"
#include "System_System_Text_RegularExpressions_RxOp4049298493.h"
#include "System_System_Text_RegularExpressions_Syntax_Expres238836340.h"
#include "System_System_Text_RegularExpressions_Syntax_Expres368137076.h"
#include "System_System_Text_RegularExpressions_Syntax_Compo1921307915.h"
#include "System_System_Text_RegularExpressions_Syntax_Group2558408851.h"
#include "System_System_Text_RegularExpressions_Syntax_Regul3083097024.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu3690174926.h"
#include "System_System_Text_RegularExpressions_Syntax_Balan3168604284.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBac607185170.h"
#include "System_System_Text_RegularExpressions_Syntax_Repet3426306051.h"
#include "System_System_Text_RegularExpressions_Syntax_Asser1490870658.h"
#include "System_System_Text_RegularExpressions_Syntax_Captur196851652.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre3255443744.h"
#include "System_System_Text_RegularExpressions_Syntax_Alter3506694545.h"
#include "System_System_Text_RegularExpressions_Syntax_Liter2896011011.h"
#include "System_System_Text_RegularExpressions_Syntax_Posit2152361535.h"
#include "System_System_Text_RegularExpressions_Syntax_Refer1540574699.h"
#include "System_System_Text_RegularExpressions_Syntax_Backs1461652789.h"
#include "System_System_Text_RegularExpressions_Syntax_Charac655244183.h"
#include "System_System_Text_RegularExpressions_Syntax_Ancho1392970135.h"
#include "System_System_UriBuilder2016461725.h"
#include "System_System_Uri19570940.h"
#include "System_System_Uri_UriScheme1876590943.h"
#include "System_System_UriFormatException3682083048.h"
#include "System_System_UriHostNameType2148127109.h"
#include "System_System_UriKind1128731744.h"
#include "System_System_UriParser1012511323.h"
#include "System_System_UriPartial112107391.h"
#include "System_System_UriTypeConverter3912970448.h"
#include "System_System_ComponentModel_ProgressChangedEventAr711712958.h"
#include "System_System_Runtime_InteropServices_HandleCollec1994147477.h"
#include "System_System_Runtime_InteropServices_StandardOleMa303634704.h"
#include "System_System_ComponentModel_DoWorkEventHandler941110040.h"
#include "System_System_ComponentModel_RunWorkerCompletedEve2492476920.h"
#include "System_System_Net_BindIPEndPoint635820671.h"
#include "System_System_Net_HttpContinueDelegate2713047268.h"
#include "System_System_Net_Mail_SendCompletedEventHandler2151823479.h"
#include "System_System_Net_Security_LocalCertificateSelecti3696771181.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959.h"
#include "System_System_Text_RegularExpressions_EvalDelegate877898325.h"
#include "System_System_ComponentModel_ProgressChangedEventHa839864825.h"
#include "System_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1703410334.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3672778804.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayT116038554.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3672778802.h"
#include "System_Security_U3CModuleU3E3783534214.h"
#include "Mono_Posix_U3CModuleU3E3783534214.h"
#include "System_Core_U3CModuleU3E3783534214.h"
#include "System_Core_System_Runtime_CompilerServices_Extens1840441203.h"
#include "System_Core_Locale4255929014.h"
#include "System_Core_System_MonoTODOAttribute3487514019.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder3965881084.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTr1394030013.h"




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (OpFlags_t378191910)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1900[6] = 
{
	OpFlags_t378191910::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (Position_t3781184359)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1901[11] = 
{
	Position_t3781184359::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (BaseMachine_t4008011478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[1] = 
{
	BaseMachine_t4008011478::get_offset_of_needs_groups_or_captures_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (FactoryCache_t2051534610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[3] = 
{
	FactoryCache_t2051534610::get_offset_of_capacity_0(),
	FactoryCache_t2051534610::get_offset_of_factories_1(),
	FactoryCache_t2051534610::get_offset_of_mru_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (Key_t655155419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[2] = 
{
	Key_t655155419::get_offset_of_pattern_0(),
	Key_t655155419::get_offset_of_options_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (MRUList_t33178162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[2] = 
{
	MRUList_t33178162::get_offset_of_head_0(),
	MRUList_t33178162::get_offset_of_tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (Node_t1107172180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[3] = 
{
	Node_t1107172180::get_offset_of_value_0(),
	Node_t1107172180::get_offset_of_previous_1(),
	Node_t1107172180::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (CaptureCollection_t1671345504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[1] = 
{
	CaptureCollection_t1671345504::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (Capture_t4157900610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[3] = 
{
	Capture_t4157900610::get_offset_of_index_0(),
	Capture_t4157900610::get_offset_of_length_1(),
	Capture_t4157900610::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (CILCompiler_t1740644799), -1, sizeof(CILCompiler_t1740644799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1911[32] = 
{
	CILCompiler_t1740644799::get_offset_of_eval_methods_2(),
	CILCompiler_t1740644799::get_offset_of_eval_methods_defined_3(),
	CILCompiler_t1740644799::get_offset_of_generic_ops_4(),
	CILCompiler_t1740644799::get_offset_of_op_flags_5(),
	CILCompiler_t1740644799::get_offset_of_labels_6(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_str_7(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_string_start_8(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_string_end_9(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_program_10(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_marks_11(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_groups_12(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_deep_13(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_stack_14(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_mark_start_15(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_mark_end_16(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_fi_mark_index_17(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_get_count_18(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_set_count_19(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_push_20(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_stack_pop_21(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_set_start_of_match_22(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_is_word_char_23(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_reset_groups_24(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_checkpoint_25(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_backtrack_26(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_open_27(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_close_28(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_get_last_defined_29(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_mark_get_index_30(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_mi_mark_get_length_31(),
	CILCompiler_t1740644799_StaticFields::get_offset_of_trace_compile_32(),
	CILCompiler_t1740644799::get_offset_of_local_textinfo_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (Frame_t997927490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[2] = 
{
	Frame_t997927490::get_offset_of_label_pass_0(),
	Frame_t997927490::get_offset_of_label_fail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (Category_t1984577050)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1913[146] = 
{
	Category_t1984577050::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (CategoryUtils_t3840220623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (LinkRef_t2090853131), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (InterpreterFactory_t556462562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[4] = 
{
	InterpreterFactory_t556462562::get_offset_of_mapping_0(),
	InterpreterFactory_t556462562::get_offset_of_pattern_1(),
	InterpreterFactory_t556462562::get_offset_of_namesMapping_2(),
	InterpreterFactory_t556462562::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (PatternCompiler_t637049905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[1] = 
{
	PatternCompiler_t637049905::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (PatternLinkStack_t3979537293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[1] = 
{
	PatternLinkStack_t3979537293::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (Link_t3337276394)+ sizeof (Il2CppObject), sizeof(Link_t3337276394 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[2] = 
{
	Link_t3337276394::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t3337276394::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (LinkStack_t954792760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[1] = 
{
	LinkStack_t954792760::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (Mark_t2724874473)+ sizeof (Il2CppObject), sizeof(Mark_t2724874473 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1922[3] = 
{
	Mark_t2724874473::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t2724874473::get_offset_of_End_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t2724874473::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (GroupCollection_t939014605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[2] = 
{
	GroupCollection_t939014605::get_offset_of_list_0(),
	GroupCollection_t939014605::get_offset_of_gap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (Group_t3761430853), -1, sizeof(Group_t3761430853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1924[3] = 
{
	Group_t3761430853_StaticFields::get_offset_of_Fail_3(),
	Group_t3761430853::get_offset_of_success_4(),
	Group_t3761430853::get_offset_of_captures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (Interpreter_t3731288230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[16] = 
{
	Interpreter_t3731288230::get_offset_of_program_1(),
	Interpreter_t3731288230::get_offset_of_program_start_2(),
	Interpreter_t3731288230::get_offset_of_text_3(),
	Interpreter_t3731288230::get_offset_of_text_end_4(),
	Interpreter_t3731288230::get_offset_of_group_count_5(),
	Interpreter_t3731288230::get_offset_of_match_min_6(),
	Interpreter_t3731288230::get_offset_of_qs_7(),
	Interpreter_t3731288230::get_offset_of_scan_ptr_8(),
	Interpreter_t3731288230::get_offset_of_repeat_9(),
	Interpreter_t3731288230::get_offset_of_fast_10(),
	Interpreter_t3731288230::get_offset_of_stack_11(),
	Interpreter_t3731288230::get_offset_of_deep_12(),
	Interpreter_t3731288230::get_offset_of_marks_13(),
	Interpreter_t3731288230::get_offset_of_mark_start_14(),
	Interpreter_t3731288230::get_offset_of_mark_end_15(),
	Interpreter_t3731288230::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (IntStack_t273560425)+ sizeof (Il2CppObject), sizeof(IntStack_t273560425_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1926[2] = 
{
	IntStack_t273560425::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t273560425::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (RepeatContext_t1827616978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[7] = 
{
	RepeatContext_t1827616978::get_offset_of_start_0(),
	RepeatContext_t1827616978::get_offset_of_min_1(),
	RepeatContext_t1827616978::get_offset_of_max_2(),
	RepeatContext_t1827616978::get_offset_of_lazy_3(),
	RepeatContext_t1827616978::get_offset_of_expr_pc_4(),
	RepeatContext_t1827616978::get_offset_of_previous_5(),
	RepeatContext_t1827616978::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (Mode_t2395763083)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1928[4] = 
{
	Mode_t2395763083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (Interval_t2354235237)+ sizeof (Il2CppObject), sizeof(Interval_t2354235237_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1929[3] = 
{
	Interval_t2354235237::get_offset_of_low_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2354235237::get_offset_of_high_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2354235237::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (IntervalCollection_t4130821325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[1] = 
{
	IntervalCollection_t4130821325::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (Enumerator_t1928086041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[2] = 
{
	Enumerator_t1928086041::get_offset_of_list_0(),
	Enumerator_t1928086041::get_offset_of_ptr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (CostDelegate_t1824458113), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (MatchCollection_t3718216671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[2] = 
{
	MatchCollection_t3718216671::get_offset_of_current_0(),
	MatchCollection_t3718216671::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (Enumerator_t501456973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[2] = 
{
	Enumerator_t501456973::get_offset_of_index_0(),
	Enumerator_t501456973::get_offset_of_coll_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (Match_t3164245899), -1, sizeof(Match_t3164245899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1935[5] = 
{
	Match_t3164245899::get_offset_of_regex_6(),
	Match_t3164245899::get_offset_of_machine_7(),
	Match_t3164245899::get_offset_of_text_length_8(),
	Match_t3164245899::get_offset_of_groups_9(),
	Match_t3164245899_StaticFields::get_offset_of_empty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (Parser_t2756028923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[6] = 
{
	Parser_t2756028923::get_offset_of_pattern_0(),
	Parser_t2756028923::get_offset_of_ptr_1(),
	Parser_t2756028923::get_offset_of_caps_2(),
	Parser_t2756028923::get_offset_of_refs_3(),
	Parser_t2756028923::get_offset_of_num_groups_4(),
	Parser_t2756028923::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (QuickSearch_t1036078825), -1, sizeof(QuickSearch_t1036078825_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1937[7] = 
{
	QuickSearch_t1036078825::get_offset_of_str_0(),
	QuickSearch_t1036078825::get_offset_of_len_1(),
	QuickSearch_t1036078825::get_offset_of_ignore_2(),
	QuickSearch_t1036078825::get_offset_of_reverse_3(),
	QuickSearch_t1036078825::get_offset_of_shift_4(),
	QuickSearch_t1036078825::get_offset_of_shiftExtended_5(),
	QuickSearch_t1036078825_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (Regex_t1803876613), -1, sizeof(Regex_t1803876613_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1938[10] = 
{
	Regex_t1803876613_StaticFields::get_offset_of_cache_0(),
	Regex_t1803876613_StaticFields::get_offset_of_old_rx_1(),
	Regex_t1803876613::get_offset_of_machineFactory_2(),
	Regex_t1803876613::get_offset_of_mapping_3(),
	Regex_t1803876613::get_offset_of_group_count_4(),
	Regex_t1803876613::get_offset_of_gap_5(),
	Regex_t1803876613::get_offset_of_group_names_6(),
	Regex_t1803876613::get_offset_of_group_numbers_7(),
	Regex_t1803876613::get_offset_of_pattern_8(),
	Regex_t1803876613::get_offset_of_roptions_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (RegexOptions_t2418259727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1939[11] = 
{
	RegexOptions_t2418259727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (RxInterpreter_t2459337652), -1, sizeof(RxInterpreter_t2459337652_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1940[14] = 
{
	RxInterpreter_t2459337652::get_offset_of_program_1(),
	RxInterpreter_t2459337652::get_offset_of_str_2(),
	RxInterpreter_t2459337652::get_offset_of_string_start_3(),
	RxInterpreter_t2459337652::get_offset_of_string_end_4(),
	RxInterpreter_t2459337652::get_offset_of_group_count_5(),
	RxInterpreter_t2459337652::get_offset_of_groups_6(),
	RxInterpreter_t2459337652::get_offset_of_eval_del_7(),
	RxInterpreter_t2459337652::get_offset_of_marks_8(),
	RxInterpreter_t2459337652::get_offset_of_mark_start_9(),
	RxInterpreter_t2459337652::get_offset_of_mark_end_10(),
	RxInterpreter_t2459337652::get_offset_of_stack_11(),
	RxInterpreter_t2459337652::get_offset_of_repeat_12(),
	RxInterpreter_t2459337652::get_offset_of_deep_13(),
	RxInterpreter_t2459337652_StaticFields::get_offset_of_trace_rx_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (IntStack_t3288646651)+ sizeof (Il2CppObject), sizeof(IntStack_t3288646651_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1941[2] = 
{
	IntStack_t3288646651::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t3288646651::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (RepeatContext_t834810884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[7] = 
{
	RepeatContext_t834810884::get_offset_of_start_0(),
	RepeatContext_t834810884::get_offset_of_min_1(),
	RepeatContext_t834810884::get_offset_of_max_2(),
	RepeatContext_t834810884::get_offset_of_lazy_3(),
	RepeatContext_t834810884::get_offset_of_expr_pc_4(),
	RepeatContext_t834810884::get_offset_of_previous_5(),
	RepeatContext_t834810884::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (RxLinkRef_t275774985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[2] = 
{
	RxLinkRef_t275774985::get_offset_of_offsets_0(),
	RxLinkRef_t275774985::get_offset_of_current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (RxCompiler_t4215271879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[2] = 
{
	RxCompiler_t4215271879::get_offset_of_program_0(),
	RxCompiler_t4215271879::get_offset_of_curpos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (RxInterpreterFactory_t1812879716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[5] = 
{
	RxInterpreterFactory_t1812879716::get_offset_of_mapping_0(),
	RxInterpreterFactory_t1812879716::get_offset_of_program_1(),
	RxInterpreterFactory_t1812879716::get_offset_of_eval_del_2(),
	RxInterpreterFactory_t1812879716::get_offset_of_namesMapping_3(),
	RxInterpreterFactory_t1812879716::get_offset_of_gap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (RxOp_t4049298493)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1946[160] = 
{
	RxOp_t4049298493::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (ExpressionCollection_t238836340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (Expression_t368137076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (CompositeExpression_t1921307915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1949[1] = 
{
	CompositeExpression_t1921307915::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (Group_t2558408851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (RegularExpression_t3083097024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[1] = 
{
	RegularExpression_t3083097024::get_offset_of_group_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (CapturingGroup_t3690174926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[2] = 
{
	CapturingGroup_t3690174926::get_offset_of_gid_1(),
	CapturingGroup_t3690174926::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (BalancingGroup_t3168604284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[1] = 
{
	BalancingGroup_t3168604284::get_offset_of_balance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (NonBacktrackingGroup_t607185170), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (Repetition_t3426306051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[3] = 
{
	Repetition_t3426306051::get_offset_of_min_1(),
	Repetition_t3426306051::get_offset_of_max_2(),
	Repetition_t3426306051::get_offset_of_lazy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (Assertion_t1490870658), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (CaptureAssertion_t196851652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[3] = 
{
	CaptureAssertion_t196851652::get_offset_of_alternate_1(),
	CaptureAssertion_t196851652::get_offset_of_group_2(),
	CaptureAssertion_t196851652::get_offset_of_literal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (ExpressionAssertion_t3255443744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[2] = 
{
	ExpressionAssertion_t3255443744::get_offset_of_reverse_1(),
	ExpressionAssertion_t3255443744::get_offset_of_negate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (Alternation_t3506694545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (Literal_t2896011011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[2] = 
{
	Literal_t2896011011::get_offset_of_str_0(),
	Literal_t2896011011::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (PositionAssertion_t2152361535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[1] = 
{
	PositionAssertion_t2152361535::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (Reference_t1540574699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[2] = 
{
	Reference_t1540574699::get_offset_of_group_0(),
	Reference_t1540574699::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (BackslashNumber_t1461652789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[2] = 
{
	BackslashNumber_t1461652789::get_offset_of_literal_2(),
	BackslashNumber_t1461652789::get_offset_of_ecma_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (CharacterClass_t655244183), -1, sizeof(CharacterClass_t655244183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1964[6] = 
{
	CharacterClass_t655244183_StaticFields::get_offset_of_upper_case_characters_0(),
	CharacterClass_t655244183::get_offset_of_negate_1(),
	CharacterClass_t655244183::get_offset_of_ignore_2(),
	CharacterClass_t655244183::get_offset_of_pos_cats_3(),
	CharacterClass_t655244183::get_offset_of_neg_cats_4(),
	CharacterClass_t655244183::get_offset_of_intervals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (AnchorInfo_t1392970135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[6] = 
{
	AnchorInfo_t1392970135::get_offset_of_expr_0(),
	AnchorInfo_t1392970135::get_offset_of_pos_1(),
	AnchorInfo_t1392970135::get_offset_of_offset_2(),
	AnchorInfo_t1392970135::get_offset_of_str_3(),
	AnchorInfo_t1392970135::get_offset_of_width_4(),
	AnchorInfo_t1392970135::get_offset_of_ignore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (UriBuilder_t2016461725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[10] = 
{
	UriBuilder_t2016461725::get_offset_of_scheme_0(),
	UriBuilder_t2016461725::get_offset_of_host_1(),
	UriBuilder_t2016461725::get_offset_of_port_2(),
	UriBuilder_t2016461725::get_offset_of_path_3(),
	UriBuilder_t2016461725::get_offset_of_query_4(),
	UriBuilder_t2016461725::get_offset_of_fragment_5(),
	UriBuilder_t2016461725::get_offset_of_username_6(),
	UriBuilder_t2016461725::get_offset_of_password_7(),
	UriBuilder_t2016461725::get_offset_of_uri_8(),
	UriBuilder_t2016461725::get_offset_of_modified_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (Uri_t19570940), -1, sizeof(Uri_t19570940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1967[36] = 
{
	Uri_t19570940::get_offset_of_isUnixFilePath_0(),
	Uri_t19570940::get_offset_of_source_1(),
	Uri_t19570940::get_offset_of_scheme_2(),
	Uri_t19570940::get_offset_of_host_3(),
	Uri_t19570940::get_offset_of_port_4(),
	Uri_t19570940::get_offset_of_path_5(),
	Uri_t19570940::get_offset_of_query_6(),
	Uri_t19570940::get_offset_of_fragment_7(),
	Uri_t19570940::get_offset_of_userinfo_8(),
	Uri_t19570940::get_offset_of_isUnc_9(),
	Uri_t19570940::get_offset_of_isOpaquePart_10(),
	Uri_t19570940::get_offset_of_isAbsoluteUri_11(),
	Uri_t19570940::get_offset_of_userEscaped_12(),
	Uri_t19570940::get_offset_of_cachedAbsoluteUri_13(),
	Uri_t19570940::get_offset_of_cachedToString_14(),
	Uri_t19570940::get_offset_of_cachedLocalPath_15(),
	Uri_t19570940::get_offset_of_cachedHashCode_16(),
	Uri_t19570940_StaticFields::get_offset_of_hexUpperChars_17(),
	Uri_t19570940_StaticFields::get_offset_of_SchemeDelimiter_18(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFile_19(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFtp_20(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeGopher_21(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttp_22(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttps_23(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeMailto_24(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNews_25(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNntp_26(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetPipe_27(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetTcp_28(),
	Uri_t19570940_StaticFields::get_offset_of_schemes_29(),
	Uri_t19570940::get_offset_of_parser_30(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map1C_31(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map1D_32(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map1E_33(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map1F_34(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map20_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (UriScheme_t1876590943)+ sizeof (Il2CppObject), sizeof(UriScheme_t1876590943_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1968[3] = 
{
	UriScheme_t1876590943::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1876590943::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1876590943::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (UriFormatException_t3682083048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (UriHostNameType_t2148127109)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1970[6] = 
{
	UriHostNameType_t2148127109::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (UriKind_t1128731744)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1971[4] = 
{
	UriKind_t1128731744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (UriParser_t1012511323), -1, sizeof(UriParser_t1012511323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1972[6] = 
{
	UriParser_t1012511323_StaticFields::get_offset_of_lock_object_0(),
	UriParser_t1012511323_StaticFields::get_offset_of_table_1(),
	UriParser_t1012511323::get_offset_of_scheme_name_2(),
	UriParser_t1012511323::get_offset_of_default_port_3(),
	UriParser_t1012511323_StaticFields::get_offset_of_uri_regex_4(),
	UriParser_t1012511323_StaticFields::get_offset_of_auth_regex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (UriPartial_t112107391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1973[5] = 
{
	UriPartial_t112107391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (UriTypeConverter_t3912970448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (ProgressChangedEventArgs_t711712958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (HandleCollector_t1994147477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[5] = 
{
	HandleCollector_t1994147477::get_offset_of_count_0(),
	HandleCollector_t1994147477::get_offset_of_init_1(),
	HandleCollector_t1994147477::get_offset_of_max_2(),
	HandleCollector_t1994147477::get_offset_of_name_3(),
	HandleCollector_t1994147477::get_offset_of_previous_collection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (StandardOleMarshalObject_t303634704), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (DoWorkEventHandler_t941110040), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (RunWorkerCompletedEventHandler_t2492476920), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (BindIPEndPoint_t635820671), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (HttpContinueDelegate_t2713047268), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (SendCompletedEventHandler_t2151823479), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (LocalCertificateSelectionCallback_t3696771181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (RemoteCertificateValidationCallback_t2756269959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (EvalDelegate_t877898325), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (ProgressChangedEventHandler_t839864825), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305140), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1987[5] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D2_1(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D3_2(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D4_3(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D5_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (U24ArrayTypeU2416_t1703410336)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410336 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (U24ArrayTypeU2432_t3672778807)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3672778807 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (U24ArrayTypeU24128_t116038555)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t116038555 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (U24ArrayTypeU2412_t3672778808)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778808 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (ExtensionAttribute_t1840441203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (Locale_t4255929017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (MonoTODOAttribute_t3487514023), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (KeyBuilder_t3965881086), -1, sizeof(KeyBuilder_t3965881086_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1998[1] = 
{
	KeyBuilder_t3965881086_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (SymmetricTransform_t1394030014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[12] = 
{
	SymmetricTransform_t1394030014::get_offset_of_algo_0(),
	SymmetricTransform_t1394030014::get_offset_of_encrypt_1(),
	SymmetricTransform_t1394030014::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t1394030014::get_offset_of_temp_3(),
	SymmetricTransform_t1394030014::get_offset_of_temp2_4(),
	SymmetricTransform_t1394030014::get_offset_of_workBuff_5(),
	SymmetricTransform_t1394030014::get_offset_of_workout_6(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t1394030014::get_offset_of_m_disposed_9(),
	SymmetricTransform_t1394030014::get_offset_of_lastBlock_10(),
	SymmetricTransform_t1394030014::get_offset_of__rng_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
