﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitUntil
struct WaitUntil_t722209015;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;

// System.Void UnityEngine.WaitUntil::.ctor(System.Func`1<System.Boolean>)
extern "C"  void WaitUntil__ctor_m3031229173 (WaitUntil_t722209015 * __this, Func_1_t1485000104 * ___predicate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WaitUntil::get_keepWaiting()
extern "C"  bool WaitUntil_get_keepWaiting_m3100747609 (WaitUntil_t722209015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
