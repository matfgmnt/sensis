﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoBackgroundManager
struct VideoBackgroundManager_t3515346924;

// System.Void Vuforia.VideoBackgroundManager::.ctor()
extern "C"  void VideoBackgroundManager__ctor_m1028119993 (VideoBackgroundManager_t3515346924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VideoBackgroundManager Vuforia.VideoBackgroundManager::get_Instance()
extern "C"  VideoBackgroundManager_t3515346924 * VideoBackgroundManager_get_Instance_m232925854 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManager::.cctor()
extern "C"  void VideoBackgroundManager__cctor_m2298157356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
