﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Runtime.InteropServices.DefaultParameterValueAttribute
struct DefaultParameterValueAttribute_t589541489;
// System.Object
struct Il2CppObject;

// System.Void System.Runtime.InteropServices.DefaultParameterValueAttribute::.ctor(System.Object)
extern "C"  void DefaultParameterValueAttribute__ctor_m458420735 (DefaultParameterValueAttribute_t589541489 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.DefaultParameterValueAttribute::get_Value()
extern "C"  Il2CppObject * DefaultParameterValueAttribute_get_Value_m1687085018 (DefaultParameterValueAttribute_t589541489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
