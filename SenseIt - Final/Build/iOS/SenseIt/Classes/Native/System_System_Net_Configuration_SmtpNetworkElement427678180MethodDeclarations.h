﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Net.Configuration.SmtpNetworkElement
struct SmtpNetworkElement_t427678180;
// System.String
struct String_t;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;

// System.Void System.Net.Configuration.SmtpNetworkElement::.ctor()
extern "C"  void SmtpNetworkElement__ctor_m834398544 (SmtpNetworkElement_t427678180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Configuration.SmtpNetworkElement::get_DefaultCredentials()
extern "C"  bool SmtpNetworkElement_get_DefaultCredentials_m2969276090 (SmtpNetworkElement_t427678180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Configuration.SmtpNetworkElement::set_DefaultCredentials(System.Boolean)
extern "C"  void SmtpNetworkElement_set_DefaultCredentials_m2441327757 (SmtpNetworkElement_t427678180 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.Configuration.SmtpNetworkElement::get_Host()
extern "C"  String_t* SmtpNetworkElement_get_Host_m1926231498 (SmtpNetworkElement_t427678180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Configuration.SmtpNetworkElement::set_Host(System.String)
extern "C"  void SmtpNetworkElement_set_Host_m2701036795 (SmtpNetworkElement_t427678180 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.Configuration.SmtpNetworkElement::get_Password()
extern "C"  String_t* SmtpNetworkElement_get_Password_m1245516251 (SmtpNetworkElement_t427678180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Configuration.SmtpNetworkElement::set_Password(System.String)
extern "C"  void SmtpNetworkElement_set_Password_m1831887340 (SmtpNetworkElement_t427678180 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Configuration.SmtpNetworkElement::get_Port()
extern "C"  int32_t SmtpNetworkElement_get_Port_m714670004 (SmtpNetworkElement_t427678180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Configuration.SmtpNetworkElement::set_Port(System.Int32)
extern "C"  void SmtpNetworkElement_set_Port_m1367099165 (SmtpNetworkElement_t427678180 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.Configuration.SmtpNetworkElement::get_UserName()
extern "C"  String_t* SmtpNetworkElement_get_UserName_m1694805796 (SmtpNetworkElement_t427678180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Configuration.SmtpNetworkElement::set_UserName(System.String)
extern "C"  void SmtpNetworkElement_set_UserName_m378608677 (SmtpNetworkElement_t427678180 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.SmtpNetworkElement::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * SmtpNetworkElement_get_Properties_m3128233203 (SmtpNetworkElement_t427678180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Configuration.SmtpNetworkElement::PostDeserialize()
extern "C"  void SmtpNetworkElement_PostDeserialize_m3596056215 (SmtpNetworkElement_t427678180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
