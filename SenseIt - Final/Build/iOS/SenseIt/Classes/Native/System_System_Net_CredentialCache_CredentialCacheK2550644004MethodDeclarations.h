﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Net.CredentialCache/CredentialCacheKey
struct CredentialCacheKey_t2550644004;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// System.Object
struct Il2CppObject;

// System.Int32 System.Net.CredentialCache/CredentialCacheKey::get_Length()
extern "C"  int32_t CredentialCacheKey_get_Length_m3490995627 (CredentialCacheKey_t2550644004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheKey::get_AbsPath()
extern "C"  String_t* CredentialCacheKey_get_AbsPath_m2639583073 (CredentialCacheKey_t2550644004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.CredentialCache/CredentialCacheKey::get_UriPrefix()
extern "C"  Uri_t19570940 * CredentialCacheKey_get_UriPrefix_m2626934337 (CredentialCacheKey_t2550644004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheKey::get_AuthType()
extern "C"  String_t* CredentialCacheKey_get_AuthType_m2714579914 (CredentialCacheKey_t2550644004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.CredentialCache/CredentialCacheKey::GetHashCode()
extern "C"  int32_t CredentialCacheKey_GetHashCode_m3939510439 (CredentialCacheKey_t2550644004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.CredentialCache/CredentialCacheKey::Equals(System.Object)
extern "C"  bool CredentialCacheKey_Equals_m3750081417 (CredentialCacheKey_t2550644004 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheKey::ToString()
extern "C"  String_t* CredentialCacheKey_ToString_m1905859591 (CredentialCacheKey_t2550644004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
