﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// ScreenshotManager
struct ScreenshotManager_t2800354649;

// System.Void ScreenshotManager::.ctor()
extern "C"  void ScreenshotManager__ctor_m3529795582 (ScreenshotManager_t2800354649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenshotManager::OnPostRender()
extern "C"  void ScreenshotManager_OnPostRender_m529110371 (ScreenshotManager_t2800354649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
