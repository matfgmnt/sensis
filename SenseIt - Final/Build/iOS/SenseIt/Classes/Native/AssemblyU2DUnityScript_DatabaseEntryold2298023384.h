﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DatabaseEntryold
struct  DatabaseEntryold_t2298023384  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 DatabaseEntryold::SensisID
	int32_t ___SensisID_2;
	// System.String DatabaseEntryold::contactName
	String_t* ___contactName_3;
	// UnityEngine.Sprite DatabaseEntryold::contactLogo
	Sprite_t309593783 * ___contactLogo_4;
	// System.String DatabaseEntryold::contactPhoneNumber1
	String_t* ___contactPhoneNumber1_5;
	// System.String DatabaseEntryold::contactPhoneNumber2
	String_t* ___contactPhoneNumber2_6;
	// System.String DatabaseEntryold::contactWebsite1
	String_t* ___contactWebsite1_7;
	// UnityEngine.Sprite DatabaseEntryold::contactWebsiteFakeImage
	Sprite_t309593783 * ___contactWebsiteFakeImage_8;
	// System.String DatabaseEntryold::contactEmailAddress1
	String_t* ___contactEmailAddress1_9;
	// System.String DatabaseEntryold::facebookAddress
	String_t* ___facebookAddress_10;
	// System.String DatabaseEntryold::nearestLocation
	String_t* ___nearestLocation_11;
	// System.String DatabaseEntryold::promotionText
	String_t* ___promotionText_12;
	// UnityEngine.Sprite DatabaseEntryold::promotionImage
	Sprite_t309593783 * ___promotionImage_13;
	// System.String DatabaseEntryold::twitterAddress
	String_t* ___twitterAddress_14;

public:
	inline static int32_t get_offset_of_SensisID_2() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___SensisID_2)); }
	inline int32_t get_SensisID_2() const { return ___SensisID_2; }
	inline int32_t* get_address_of_SensisID_2() { return &___SensisID_2; }
	inline void set_SensisID_2(int32_t value)
	{
		___SensisID_2 = value;
	}

	inline static int32_t get_offset_of_contactName_3() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___contactName_3)); }
	inline String_t* get_contactName_3() const { return ___contactName_3; }
	inline String_t** get_address_of_contactName_3() { return &___contactName_3; }
	inline void set_contactName_3(String_t* value)
	{
		___contactName_3 = value;
		Il2CppCodeGenWriteBarrier(&___contactName_3, value);
	}

	inline static int32_t get_offset_of_contactLogo_4() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___contactLogo_4)); }
	inline Sprite_t309593783 * get_contactLogo_4() const { return ___contactLogo_4; }
	inline Sprite_t309593783 ** get_address_of_contactLogo_4() { return &___contactLogo_4; }
	inline void set_contactLogo_4(Sprite_t309593783 * value)
	{
		___contactLogo_4 = value;
		Il2CppCodeGenWriteBarrier(&___contactLogo_4, value);
	}

	inline static int32_t get_offset_of_contactPhoneNumber1_5() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___contactPhoneNumber1_5)); }
	inline String_t* get_contactPhoneNumber1_5() const { return ___contactPhoneNumber1_5; }
	inline String_t** get_address_of_contactPhoneNumber1_5() { return &___contactPhoneNumber1_5; }
	inline void set_contactPhoneNumber1_5(String_t* value)
	{
		___contactPhoneNumber1_5 = value;
		Il2CppCodeGenWriteBarrier(&___contactPhoneNumber1_5, value);
	}

	inline static int32_t get_offset_of_contactPhoneNumber2_6() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___contactPhoneNumber2_6)); }
	inline String_t* get_contactPhoneNumber2_6() const { return ___contactPhoneNumber2_6; }
	inline String_t** get_address_of_contactPhoneNumber2_6() { return &___contactPhoneNumber2_6; }
	inline void set_contactPhoneNumber2_6(String_t* value)
	{
		___contactPhoneNumber2_6 = value;
		Il2CppCodeGenWriteBarrier(&___contactPhoneNumber2_6, value);
	}

	inline static int32_t get_offset_of_contactWebsite1_7() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___contactWebsite1_7)); }
	inline String_t* get_contactWebsite1_7() const { return ___contactWebsite1_7; }
	inline String_t** get_address_of_contactWebsite1_7() { return &___contactWebsite1_7; }
	inline void set_contactWebsite1_7(String_t* value)
	{
		___contactWebsite1_7 = value;
		Il2CppCodeGenWriteBarrier(&___contactWebsite1_7, value);
	}

	inline static int32_t get_offset_of_contactWebsiteFakeImage_8() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___contactWebsiteFakeImage_8)); }
	inline Sprite_t309593783 * get_contactWebsiteFakeImage_8() const { return ___contactWebsiteFakeImage_8; }
	inline Sprite_t309593783 ** get_address_of_contactWebsiteFakeImage_8() { return &___contactWebsiteFakeImage_8; }
	inline void set_contactWebsiteFakeImage_8(Sprite_t309593783 * value)
	{
		___contactWebsiteFakeImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___contactWebsiteFakeImage_8, value);
	}

	inline static int32_t get_offset_of_contactEmailAddress1_9() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___contactEmailAddress1_9)); }
	inline String_t* get_contactEmailAddress1_9() const { return ___contactEmailAddress1_9; }
	inline String_t** get_address_of_contactEmailAddress1_9() { return &___contactEmailAddress1_9; }
	inline void set_contactEmailAddress1_9(String_t* value)
	{
		___contactEmailAddress1_9 = value;
		Il2CppCodeGenWriteBarrier(&___contactEmailAddress1_9, value);
	}

	inline static int32_t get_offset_of_facebookAddress_10() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___facebookAddress_10)); }
	inline String_t* get_facebookAddress_10() const { return ___facebookAddress_10; }
	inline String_t** get_address_of_facebookAddress_10() { return &___facebookAddress_10; }
	inline void set_facebookAddress_10(String_t* value)
	{
		___facebookAddress_10 = value;
		Il2CppCodeGenWriteBarrier(&___facebookAddress_10, value);
	}

	inline static int32_t get_offset_of_nearestLocation_11() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___nearestLocation_11)); }
	inline String_t* get_nearestLocation_11() const { return ___nearestLocation_11; }
	inline String_t** get_address_of_nearestLocation_11() { return &___nearestLocation_11; }
	inline void set_nearestLocation_11(String_t* value)
	{
		___nearestLocation_11 = value;
		Il2CppCodeGenWriteBarrier(&___nearestLocation_11, value);
	}

	inline static int32_t get_offset_of_promotionText_12() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___promotionText_12)); }
	inline String_t* get_promotionText_12() const { return ___promotionText_12; }
	inline String_t** get_address_of_promotionText_12() { return &___promotionText_12; }
	inline void set_promotionText_12(String_t* value)
	{
		___promotionText_12 = value;
		Il2CppCodeGenWriteBarrier(&___promotionText_12, value);
	}

	inline static int32_t get_offset_of_promotionImage_13() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___promotionImage_13)); }
	inline Sprite_t309593783 * get_promotionImage_13() const { return ___promotionImage_13; }
	inline Sprite_t309593783 ** get_address_of_promotionImage_13() { return &___promotionImage_13; }
	inline void set_promotionImage_13(Sprite_t309593783 * value)
	{
		___promotionImage_13 = value;
		Il2CppCodeGenWriteBarrier(&___promotionImage_13, value);
	}

	inline static int32_t get_offset_of_twitterAddress_14() { return static_cast<int32_t>(offsetof(DatabaseEntryold_t2298023384, ___twitterAddress_14)); }
	inline String_t* get_twitterAddress_14() const { return ___twitterAddress_14; }
	inline String_t** get_address_of_twitterAddress_14() { return &___twitterAddress_14; }
	inline void set_twitterAddress_14(String_t* value)
	{
		___twitterAddress_14 = value;
		Il2CppCodeGenWriteBarrier(&___twitterAddress_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
