﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoBackgroundBehaviour
struct VideoBackgroundBehaviour_t3161817952;

// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern "C"  void VideoBackgroundBehaviour__ctor_m2133855431 (VideoBackgroundBehaviour_t3161817952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
