﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2231194049MethodDeclarations.h"


// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::.ctor()
#define Collection_1__ctor_m691017847(__this, method) ((  void (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1__ctor_m3383758099_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1786747892(__this, method) ((  bool (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2832435102_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m424488821(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2151674177 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2795445359_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2654218756(__this, method) ((  Il2CppObject * (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m539985258_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m3102534933(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2151674177 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m916188271_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m965152853(__this, ___value0, method) ((  bool (*) (Collection_1_t2151674177 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3240760119_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m31579711(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2151674177 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3460849589_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m3233625606(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2151674177 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3482199744_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m1662524404(__this, ___value0, method) ((  void (*) (Collection_1_t2151674177 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1739078822_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1631362233(__this, method) ((  bool (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1442644511_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1903233241(__this, method) ((  Il2CppObject * (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1422512927_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1312948702(__this, method) ((  bool (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2968235316_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1636977341(__this, method) ((  bool (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1990189611_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m4100967100(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2151674177 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m75082808_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m4280320103(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2151674177 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m507853765_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Add(T)
#define Collection_1_Add_m1649713474(__this, ___item0, method) ((  void (*) (Collection_1_t2151674177 *, Attachment_t2609929423 *, const MethodInfo*))Collection_1_Add_m2987402052_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Clear()
#define Collection_1_Clear_m454037182(__this, method) ((  void (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_Clear_m1596645192_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::ClearItems()
#define Collection_1_ClearItems_m1764251162(__this, method) ((  void (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_ClearItems_m1175603758_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Contains(T)
#define Collection_1_Contains_m3885909084(__this, ___item0, method) ((  bool (*) (Collection_1_t2151674177 *, Attachment_t2609929423 *, const MethodInfo*))Collection_1_Contains_m2116635914_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m3796602362(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2151674177 *, AttachmentU5BU5D_t3042992918*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1578267616_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::GetEnumerator()
#define Collection_1_GetEnumerator_m3089730326(__this, method) ((  Il2CppObject* (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_GetEnumerator_m2963411583_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::IndexOf(T)
#define Collection_1_IndexOf_m1796250892(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2151674177 *, Attachment_t2609929423 *, const MethodInfo*))Collection_1_IndexOf_m3885709710_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Insert(System.Int32,T)
#define Collection_1_Insert_m1303583410(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2151674177 *, int32_t, Attachment_t2609929423 *, const MethodInfo*))Collection_1_Insert_m2334889193_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m3395791865(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2151674177 *, int32_t, Attachment_t2609929423 *, const MethodInfo*))Collection_1_InsertItem_m3611385334_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::Remove(T)
#define Collection_1_Remove_m2007128926(__this, ___item0, method) ((  bool (*) (Collection_1_t2151674177 *, Attachment_t2609929423 *, const MethodInfo*))Collection_1_Remove_m452558737_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m2831139979(__this, ___index0, method) ((  void (*) (Collection_1_t2151674177 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1632496813_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m3580883505(__this, ___index0, method) ((  void (*) (Collection_1_t2151674177 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4104600353_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::get_Count()
#define Collection_1_get_Count_m2148678635(__this, method) ((  int32_t (*) (Collection_1_t2151674177 *, const MethodInfo*))Collection_1_get_Count_m2250721247_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::get_Item(System.Int32)
#define Collection_1_get_Item_m1252441444(__this, ___index0, method) ((  Attachment_t2609929423 * (*) (Collection_1_t2151674177 *, int32_t, const MethodInfo*))Collection_1_get_Item_m266052953_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m3792555376(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2151674177 *, int32_t, Attachment_t2609929423 *, const MethodInfo*))Collection_1_set_Item_m3489932746_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m758282304(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2151674177 *, int32_t, Attachment_t2609929423 *, const MethodInfo*))Collection_1_SetItem_m1075410277_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m2736794454(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3443424420_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m2200254974(__this /* static, unused */, ___item0, method) ((  Attachment_t2609929423 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1521356246_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m2473990758(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m215419136_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m2312797812(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m328767958_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.Attachment>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m4192595119(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3594284193_gshared)(__this /* static, unused */, ___list0, method)
