﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.FeedbackManagerOld
struct  FeedbackManagerOld_t108831159  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D FGMNT.FeedbackManagerOld::tex
	Texture2D_t3542995729 * ___tex_2;
	// System.Boolean FGMNT.FeedbackManagerOld::sendPositiveFeedback
	bool ___sendPositiveFeedback_3;
	// System.Boolean FGMNT.FeedbackManagerOld::sendNegativeFeedback
	bool ___sendNegativeFeedback_4;
	// System.Boolean FGMNT.FeedbackManagerOld::source
	bool ___source_5;

public:
	inline static int32_t get_offset_of_tex_2() { return static_cast<int32_t>(offsetof(FeedbackManagerOld_t108831159, ___tex_2)); }
	inline Texture2D_t3542995729 * get_tex_2() const { return ___tex_2; }
	inline Texture2D_t3542995729 ** get_address_of_tex_2() { return &___tex_2; }
	inline void set_tex_2(Texture2D_t3542995729 * value)
	{
		___tex_2 = value;
		Il2CppCodeGenWriteBarrier(&___tex_2, value);
	}

	inline static int32_t get_offset_of_sendPositiveFeedback_3() { return static_cast<int32_t>(offsetof(FeedbackManagerOld_t108831159, ___sendPositiveFeedback_3)); }
	inline bool get_sendPositiveFeedback_3() const { return ___sendPositiveFeedback_3; }
	inline bool* get_address_of_sendPositiveFeedback_3() { return &___sendPositiveFeedback_3; }
	inline void set_sendPositiveFeedback_3(bool value)
	{
		___sendPositiveFeedback_3 = value;
	}

	inline static int32_t get_offset_of_sendNegativeFeedback_4() { return static_cast<int32_t>(offsetof(FeedbackManagerOld_t108831159, ___sendNegativeFeedback_4)); }
	inline bool get_sendNegativeFeedback_4() const { return ___sendNegativeFeedback_4; }
	inline bool* get_address_of_sendNegativeFeedback_4() { return &___sendNegativeFeedback_4; }
	inline void set_sendNegativeFeedback_4(bool value)
	{
		___sendNegativeFeedback_4 = value;
	}

	inline static int32_t get_offset_of_source_5() { return static_cast<int32_t>(offsetof(FeedbackManagerOld_t108831159, ___source_5)); }
	inline bool get_source_5() const { return ___source_5; }
	inline bool* get_address_of_source_5() { return &___source_5; }
	inline void set_source_5(bool value)
	{
		___source_5 = value;
	}
};

struct FeedbackManagerOld_t108831159_StaticFields
{
public:
	// System.Net.Security.RemoteCertificateValidationCallback FGMNT.FeedbackManagerOld::<>f__am$cache0
	RemoteCertificateValidationCallback_t2756269959 * ___U3CU3Ef__amU24cache0_6;
	// System.Net.Security.RemoteCertificateValidationCallback FGMNT.FeedbackManagerOld::<>f__am$cache1
	RemoteCertificateValidationCallback_t2756269959 * ___U3CU3Ef__amU24cache1_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(FeedbackManagerOld_t108831159_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(FeedbackManagerOld_t108831159_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
