﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerBehaviour
struct SmartTerrainTrackerBehaviour_t3844158157;

// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern "C"  void SmartTerrainTrackerBehaviour__ctor_m1862741924 (SmartTerrainTrackerBehaviour_t3844158157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
