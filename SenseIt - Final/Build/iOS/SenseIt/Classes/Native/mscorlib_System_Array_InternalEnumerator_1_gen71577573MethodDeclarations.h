﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"


// System.Void System.Array/InternalEnumerator`1<System.ValueType>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m51465471(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t71577573 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.ValueType>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4273518383(__this, method) ((  void (*) (InternalEnumerator_1_t71577573 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.ValueType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2097852475(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t71577573 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ValueType>::Dispose()
#define InternalEnumerator_1_Dispose_m1979923166(__this, method) ((  void (*) (InternalEnumerator_1_t71577573 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.ValueType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3036221931(__this, method) ((  bool (*) (InternalEnumerator_1_t71577573 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.ValueType>::get_Current()
#define InternalEnumerator_1_get_Current_m3514749120(__this, method) ((  ValueType_t3507792607 * (*) (InternalEnumerator_1_t71577573 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
