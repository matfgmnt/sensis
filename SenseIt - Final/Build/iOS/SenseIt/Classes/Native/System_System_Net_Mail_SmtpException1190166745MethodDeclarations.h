﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Mail_SmtpStatusCode887155417.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Net.Mail.SmtpException
struct SmtpException_t1190166745;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Exception
struct Exception_t1927440687;

// System.Void System.Net.Mail.SmtpException::.ctor()
extern "C"  void SmtpException__ctor_m3732418010 (SmtpException_t1190166745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpException::.ctor(System.Net.Mail.SmtpStatusCode)
extern "C"  void SmtpException__ctor_m3505831492 (SmtpException_t1190166745 * __this, int32_t ___statusCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpException::.ctor(System.String)
extern "C"  void SmtpException__ctor_m4014025072 (SmtpException_t1190166745 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SmtpException__ctor_m459136857 (SmtpException_t1190166745 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpException::.ctor(System.Net.Mail.SmtpStatusCode,System.String)
extern "C"  void SmtpException__ctor_m1779043996 (SmtpException_t1190166745 * __this, int32_t ___statusCode0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpException::.ctor(System.String,System.Exception)
extern "C"  void SmtpException__ctor_m1380789360 (SmtpException_t1190166745 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SmtpException_System_Runtime_Serialization_ISerializable_GetObjectData_m3176476085 (SmtpException_t1190166745 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SmtpException_GetObjectData_m4025964018 (SmtpException_t1190166745 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
