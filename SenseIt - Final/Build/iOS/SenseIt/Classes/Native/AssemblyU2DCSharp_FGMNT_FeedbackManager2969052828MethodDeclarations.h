﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FGMNT_DatabaseEntry2505461191.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ScreenshotManager2800354649.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_ComponentModel_AsyncCompletedEventArgs83270938.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"

// FGMNT.FeedbackManager
struct FeedbackManager_t2969052828;
// FGMNT.DatabaseEntry
struct DatabaseEntry_t2505461191;
// System.String
struct String_t;
// ScreenshotManager
struct ScreenshotManager_t2800354649;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object
struct Il2CppObject;
// System.ComponentModel.AsyncCompletedEventArgs
struct AsyncCompletedEventArgs_t83270938;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;

// System.Void FGMNT.FeedbackManager::.ctor()
extern "C"  void FeedbackManager__ctor_m4126828041 (FeedbackManager_t2969052828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::Start()
extern "C"  void FeedbackManager_Start_m3319102585 (FeedbackManager_t2969052828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::Update()
extern "C"  void FeedbackManager_Update_m2807601726 (FeedbackManager_t2969052828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::PositiveFeedBack(System.Boolean,FGMNT.DatabaseEntry)
extern "C"  void FeedbackManager_PositiveFeedBack_m2364828345 (FeedbackManager_t2969052828 * __this, bool ___reco0, DatabaseEntry_t2505461191 * ___dbEntry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::NegativeFeedBack()
extern "C"  void FeedbackManager_NegativeFeedBack_m337206255 (FeedbackManager_t2969052828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::ActionEmail(System.String)
extern "C"  void FeedbackManager_ActionEmail_m268566173 (FeedbackManager_t2969052828 * __this, String_t* ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::TakeScreenshot(ScreenshotManager)
extern "C"  void FeedbackManager_TakeScreenshot_m261426609 (FeedbackManager_t2969052828 * __this, ScreenshotManager_t2800354649 * ___scManager0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::Savecsv(System.String[])
extern "C"  void FeedbackManager_Savecsv_m1914520974 (FeedbackManager_t2969052828 * __this, StringU5BU5D_t1642385972* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::SaveInformation(System.Int32)
extern "C"  void FeedbackManager_SaveInformation_m4132262201 (FeedbackManager_t2969052828 * __this, int32_t ___positive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::SendCompletedCallback(System.Object,System.ComponentModel.AsyncCompletedEventArgs)
extern "C"  void FeedbackManager_SendCompletedCallback_m4003766862 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, AsyncCompletedEventArgs_t83270938 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::SendEmail()
extern "C"  void FeedbackManager_SendEmail_m1720600505 (FeedbackManager_t2969052828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::DeleteAllFiles()
extern "C"  void FeedbackManager_DeleteAllFiles_m2361518242 (FeedbackManager_t2969052828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.FeedbackManager::.cctor()
extern "C"  void FeedbackManager__cctor_m3328653342 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FGMNT.FeedbackManager::<SendEmail>m__0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool FeedbackManager_U3CSendEmailU3Em__0_m129161213 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___s0, X509Certificate_t283079845 * ___certificate1, X509Chain_t777637347 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
