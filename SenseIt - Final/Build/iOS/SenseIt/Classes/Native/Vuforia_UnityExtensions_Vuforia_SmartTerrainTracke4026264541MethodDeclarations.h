﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// Vuforia.SmartTerrainTrackerAbstractBehaviour
struct SmartTerrainTrackerAbstractBehaviour_t4026264541;
// System.Action
struct Action_t3226471752;

// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Awake()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_Awake_m195975514 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnEnable()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnEnable_m1919743413 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDisable()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnDisable_m462525450 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDestroy()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnDestroy_m1313639928 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
extern "C"  void SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m4123369325 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
extern "C"  void SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m1722967068 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerAbstractBehaviour::get_AutomaticStart()
extern "C"  bool SmartTerrainTrackerAbstractBehaviour_get_AutomaticStart_m909945801 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StartSmartTerrainTracker()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m3956375703 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StopSmartTerrainTracker()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m556875331 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::InitSmartTerrainTracker()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m1655903997 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnVuforiaInitialized()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnVuforiaInitialized_m1688904364 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnVuforiaStarted()
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnVuforiaStarted_m3618061557 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnPause(System.Boolean)
extern "C"  void SmartTerrainTrackerAbstractBehaviour_OnPause_m788481953 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::.ctor()
extern "C"  void SmartTerrainTrackerAbstractBehaviour__ctor_m2604970325 (SmartTerrainTrackerAbstractBehaviour_t4026264541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
