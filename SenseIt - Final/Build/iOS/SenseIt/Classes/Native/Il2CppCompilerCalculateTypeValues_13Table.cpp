﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet3019654938.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaForm1143227640.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFractionDigi2708215647.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroup4189650927.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupBase3811767860.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaGroupRef3082205844.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaIdentityCons1058613623.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaImport250324363.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInclude2752556710.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo2864028808.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKey1946917723.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaKeyref1894386400.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaLengthFacet430394943.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxExclusive2593226405.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxInclusiveF663510947.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMaxLengthFac3225833099.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinExclusive3225721695.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinInclusiveF708563817.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaMinLengthFac1871324785.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaNotation346281646.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaNumericFacet3887766756.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject2050913741.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectCollect395083109.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectEnumer2354997415.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable3364835593.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectTable_2908487920.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3365045970.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle_Empt446815059.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaPatternFacet2024909611.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaRedefine3478619248.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet313318308.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSequence728414063.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten2303138587.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten3718357176.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleConten2728776481.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType248156492.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1606103299.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLi2170323082.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe1099506232.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUnio91327365.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaTotalDigitsF2939281405.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType1795078578.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUnique403169513.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUse3553149267.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity995929432.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationEx2387044366.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaWhiteSpaceFa2007056012.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaXPath604820427.h"
#include "System_Xml_System_Xml_Schema_XmlSeverityType3547578624.h"
#include "System_Xml_System_Xml_Schema_ValidationHandler3342756761.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil3576230726.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaReader3786681597.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidationFla910489930.h"
#include "System_Xml_System_Xml_Schema_XmlTypeCode58293802.h"
#include "System_Xml_System_Xml_Serialization_CodeIdentifier3245527920.h"
#include "System_Xml_System_Xml_Serialization_SchemaTypes3045759914.h"
#include "System_Xml_System_Xml_Serialization_TypeData3979356678.h"
#include "System_Xml_System_Xml_Serialization_TypeTranslator1077722680.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyAttribute713947513.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyElementA2502375235.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAtt850813783.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2182839281.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut919400678.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri2333915871.h"
#include "System_Xml_System_Xml_Serialization_XmlNamespaceDe2069522403.h"
#include "System_Xml_System_Xml_Serialization_XmlRootAttribu3527426713.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerN3063656491.h"
#include "System_Xml_System_Xml_Serialization_XmlTextAttribu3321178844.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler2964483403.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle1580700381.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3672778802.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1957337327.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2038352954.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar628910058.h"
#include "Mono_Security_U3CModuleU3E3783534214.h"
#include "Mono_Security_Locale4255929014.h"
#include "Mono_Security_Mono_Math_BigInteger925946152.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign874893935.h"
#include "Mono_Security_Mono_Math_BigInteger_ModulusRing80355991.h"
#include "Mono_Security_Mono_Math_BigInteger_Kernel1353186455.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor1997037801.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTests3283102398.h"
#include "Mono_Security_Mono_Math_Prime_Generator_PrimeGener1053438167.h"
#include "Mono_Security_Mono_Math_Prime_Generator_SequentialS463670656.h"
#include "Mono_Security_Mono_Security_ASN1924533535.h"
#include "Mono_Security_Mono_Security_ASN1Convert3301846396.h"
#include "Mono_Security_Mono_Security_BitConverterLE2825370260.h"
#include "Mono_Security_Mono_Security_PKCS73223261922.h"
#include "Mono_Security_Mono_Security_PKCS7_ContentInfo1443605387.h"
#include "Mono_Security_Mono_Security_PKCS7_EncryptedData2656813772.h"
#include "Mono_Security_Mono_Security_Cryptography_ARC4Manag3379271383.h"
#include "Mono_Security_Mono_Security_Cryptography_CryptoCon4146607874.h"
#include "Mono_Security_Mono_Security_Cryptography_KeyBuilde3965881084.h"
#include "Mono_Security_Mono_Security_Cryptography_MD2726199179.h"
#include "Mono_Security_Mono_Security_Cryptography_MD2Manage1421454332.h"
#include "Mono_Security_Mono_Security_Cryptography_MD41888998593.h"
#include "Mono_Security_Mono_Security_Cryptography_MD4Manage2176273562.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS13312870480.h"




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (Facet_t3019654938)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1300[14] = 
{
	Facet_t3019654938::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (XmlSchemaForm_t1143227640)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1301[4] = 
{
	XmlSchemaForm_t1143227640::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (XmlSchemaFractionDigitsFacet_t2708215647), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (XmlSchemaGroup_t4189650927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1303[4] = 
{
	XmlSchemaGroup_t4189650927::get_offset_of_name_16(),
	XmlSchemaGroup_t4189650927::get_offset_of_particle_17(),
	XmlSchemaGroup_t4189650927::get_offset_of_qualifiedName_18(),
	XmlSchemaGroup_t4189650927::get_offset_of_isCircularDefinition_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (XmlSchemaGroupBase_t3811767860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1304[1] = 
{
	XmlSchemaGroupBase_t3811767860::get_offset_of_compiledItems_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (XmlSchemaGroupRef_t3082205844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1305[4] = 
{
	XmlSchemaGroupRef_t3082205844::get_offset_of_schema_27(),
	XmlSchemaGroupRef_t3082205844::get_offset_of_refName_28(),
	XmlSchemaGroupRef_t3082205844::get_offset_of_referencedGroup_29(),
	XmlSchemaGroupRef_t3082205844::get_offset_of_busy_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (XmlSchemaIdentityConstraint_t1058613623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1306[5] = 
{
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_fields_16(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_name_17(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_qName_18(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_selector_19(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_compiledSelector_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (XmlSchemaImport_t250324363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1307[2] = 
{
	XmlSchemaImport_t250324363::get_offset_of_annotation_16(),
	XmlSchemaImport_t250324363::get_offset_of_nameSpace_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (XmlSchemaInclude_t2752556710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1308[1] = 
{
	XmlSchemaInclude_t2752556710::get_offset_of_annotation_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (XmlSchemaInfo_t2864028808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1309[7] = 
{
	XmlSchemaInfo_t2864028808::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t2864028808::get_offset_of_isNil_1(),
	XmlSchemaInfo_t2864028808::get_offset_of_memberType_2(),
	XmlSchemaInfo_t2864028808::get_offset_of_attr_3(),
	XmlSchemaInfo_t2864028808::get_offset_of_elem_4(),
	XmlSchemaInfo_t2864028808::get_offset_of_type_5(),
	XmlSchemaInfo_t2864028808::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (XmlSchemaKey_t1946917723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (XmlSchemaKeyref_t1894386400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1311[2] = 
{
	XmlSchemaKeyref_t1894386400::get_offset_of_refer_21(),
	XmlSchemaKeyref_t1894386400::get_offset_of_target_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (XmlSchemaLengthFacet_t430394943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (XmlSchemaMaxExclusiveFacet_t2593226405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (XmlSchemaMaxInclusiveFacet_t663510947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (XmlSchemaMaxLengthFacet_t3225833099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (XmlSchemaMinExclusiveFacet_t3225721695), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (XmlSchemaMinInclusiveFacet_t708563817), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (XmlSchemaMinLengthFacet_t1871324785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (XmlSchemaNotation_t346281646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1319[4] = 
{
	XmlSchemaNotation_t346281646::get_offset_of_name_16(),
	XmlSchemaNotation_t346281646::get_offset_of_pub_17(),
	XmlSchemaNotation_t346281646::get_offset_of_system_18(),
	XmlSchemaNotation_t346281646::get_offset_of_qualifiedName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (XmlSchemaNumericFacet_t3887766756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (XmlSchemaObject_t2050913741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1321[13] = 
{
	XmlSchemaObject_t2050913741::get_offset_of_lineNumber_0(),
	XmlSchemaObject_t2050913741::get_offset_of_linePosition_1(),
	XmlSchemaObject_t2050913741::get_offset_of_sourceUri_2(),
	XmlSchemaObject_t2050913741::get_offset_of_namespaces_3(),
	XmlSchemaObject_t2050913741::get_offset_of_unhandledAttributeList_4(),
	XmlSchemaObject_t2050913741::get_offset_of_isCompiled_5(),
	XmlSchemaObject_t2050913741::get_offset_of_errorCount_6(),
	XmlSchemaObject_t2050913741::get_offset_of_CompilationId_7(),
	XmlSchemaObject_t2050913741::get_offset_of_ValidationId_8(),
	XmlSchemaObject_t2050913741::get_offset_of_isRedefineChild_9(),
	XmlSchemaObject_t2050913741::get_offset_of_isRedefinedComponent_10(),
	XmlSchemaObject_t2050913741::get_offset_of_redefinedObject_11(),
	XmlSchemaObject_t2050913741::get_offset_of_parent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (XmlSchemaObjectCollection_t395083109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (XmlSchemaObjectEnumerator_t2354997415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1323[1] = 
{
	XmlSchemaObjectEnumerator_t2354997415::get_offset_of_ienum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (XmlSchemaObjectTable_t3364835593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1324[1] = 
{
	XmlSchemaObjectTable_t3364835593::get_offset_of_table_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (XmlSchemaObjectTableEnumerator_t2908487920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1325[2] = 
{
	XmlSchemaObjectTableEnumerator_t2908487920::get_offset_of_xenum_0(),
	XmlSchemaObjectTableEnumerator_t2908487920::get_offset_of_tmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (XmlSchemaParticle_t3365045970), -1, sizeof(XmlSchemaParticle_t3365045970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1326[11] = 
{
	XmlSchemaParticle_t3365045970::get_offset_of_minOccurs_16(),
	XmlSchemaParticle_t3365045970::get_offset_of_maxOccurs_17(),
	XmlSchemaParticle_t3365045970::get_offset_of_minstr_18(),
	XmlSchemaParticle_t3365045970::get_offset_of_maxstr_19(),
	XmlSchemaParticle_t3365045970_StaticFields::get_offset_of_empty_20(),
	XmlSchemaParticle_t3365045970::get_offset_of_validatedMinOccurs_21(),
	XmlSchemaParticle_t3365045970::get_offset_of_validatedMaxOccurs_22(),
	XmlSchemaParticle_t3365045970::get_offset_of_recursionDepth_23(),
	XmlSchemaParticle_t3365045970::get_offset_of_minEffectiveTotalRange_24(),
	XmlSchemaParticle_t3365045970::get_offset_of_parentIsGroupDefinition_25(),
	XmlSchemaParticle_t3365045970::get_offset_of_OptimizedParticle_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (EmptyParticle_t446815059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (XmlSchemaPatternFacet_t2024909611), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (XmlSchemaRedefine_t3478619248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1329[4] = 
{
	XmlSchemaRedefine_t3478619248::get_offset_of_attributeGroups_16(),
	XmlSchemaRedefine_t3478619248::get_offset_of_groups_17(),
	XmlSchemaRedefine_t3478619248::get_offset_of_items_18(),
	XmlSchemaRedefine_t3478619248::get_offset_of_schemaTypes_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (XmlSchemaSet_t313318308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1330[12] = 
{
	XmlSchemaSet_t313318308::get_offset_of_nameTable_0(),
	XmlSchemaSet_t313318308::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t313318308::get_offset_of_schemas_2(),
	XmlSchemaSet_t313318308::get_offset_of_attributes_3(),
	XmlSchemaSet_t313318308::get_offset_of_elements_4(),
	XmlSchemaSet_t313318308::get_offset_of_types_5(),
	XmlSchemaSet_t313318308::get_offset_of_idCollection_6(),
	XmlSchemaSet_t313318308::get_offset_of_namedIdentities_7(),
	XmlSchemaSet_t313318308::get_offset_of_settings_8(),
	XmlSchemaSet_t313318308::get_offset_of_isCompiled_9(),
	XmlSchemaSet_t313318308::get_offset_of_CompilationId_10(),
	XmlSchemaSet_t313318308::get_offset_of_ValidationEventHandler_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (XmlSchemaSequence_t728414063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1331[1] = 
{
	XmlSchemaSequence_t728414063::get_offset_of_items_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (XmlSchemaSimpleContent_t2303138587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1332[1] = 
{
	XmlSchemaSimpleContent_t2303138587::get_offset_of_content_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (XmlSchemaSimpleContentExtension_t3718357176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1333[3] = 
{
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_any_17(),
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_baseTypeName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (XmlSchemaSimpleContentRestriction_t2728776481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1334[5] = 
{
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_any_17(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_baseType_19(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_baseTypeName_20(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_facets_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (XmlSchemaSimpleType_t248156492), -1, sizeof(XmlSchemaSimpleType_t248156492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1335[54] = 
{
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_schemaLocationType_28(),
	XmlSchemaSimpleType_t248156492::get_offset_of_content_29(),
	XmlSchemaSimpleType_t248156492::get_offset_of_islocal_30(),
	XmlSchemaSimpleType_t248156492::get_offset_of_recursed_31(),
	XmlSchemaSimpleType_t248156492::get_offset_of_variety_32(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnySimpleType_33(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsString_34(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBoolean_35(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDecimal_36(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsFloat_37(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDouble_38(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDuration_39(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDateTime_40(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsTime_41(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDate_42(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYearMonth_43(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYear_44(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonthDay_45(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGDay_46(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonth_47(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsHexBinary_48(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBase64Binary_49(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnyUri_50(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsQName_51(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNotation_52(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNormalizedString_53(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsToken_54(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLanguage_55(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMToken_56(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMTokens_57(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsName_58(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNCName_59(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsID_60(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRef_61(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRefs_62(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntity_63(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntities_64(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInteger_65(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonPositiveInteger_66(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNegativeInteger_67(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLong_68(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInt_69(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsShort_70(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsByte_71(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonNegativeInteger_72(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedLong_73(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedInt_74(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedShort_75(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedByte_76(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsPositiveInteger_77(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtUntypedAtomic_78(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtAnyAtomicType_79(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtYearMonthDuration_80(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtDayTimeDuration_81(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (XmlSchemaSimpleTypeContent_t1606103299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1336[1] = 
{
	XmlSchemaSimpleTypeContent_t1606103299::get_offset_of_OwnerType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (XmlSchemaSimpleTypeList_t2170323082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1337[4] = 
{
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemType_17(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemTypeName_18(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_validatedListItemType_19(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_validatedListItemSchemaType_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (XmlSchemaSimpleTypeRestriction_t1099506232), -1, sizeof(XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1338[18] = 
{
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_baseType_17(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_baseTypeName_18(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_facets_19(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_enumarationFacetValues_20(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_patternFacetValues_21(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_rexPatterns_22(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_lengthFacet_23(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_maxLengthFacet_24(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_minLengthFacet_25(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_fractionDigitsFacet_26(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_totalDigitsFacet_27(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_maxInclusiveFacet_28(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_maxExclusiveFacet_29(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_minInclusiveFacet_30(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_minExclusiveFacet_31(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_fixedFacets_32(),
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_lengthStyle_33(),
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_listFacets_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (XmlSchemaSimpleTypeUnion_t91327365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1339[4] = 
{
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_baseTypes_17(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_memberTypes_18(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_validatedTypes_19(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_validatedSchemaTypes_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (XmlSchemaTotalDigitsFacet_t2939281405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (XmlSchemaType_t1795078578), -1, sizeof(XmlSchemaType_t1795078578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1341[12] = 
{
	XmlSchemaType_t1795078578::get_offset_of_final_16(),
	XmlSchemaType_t1795078578::get_offset_of_isMixed_17(),
	XmlSchemaType_t1795078578::get_offset_of_name_18(),
	XmlSchemaType_t1795078578::get_offset_of_recursed_19(),
	XmlSchemaType_t1795078578::get_offset_of_BaseSchemaTypeName_20(),
	XmlSchemaType_t1795078578::get_offset_of_BaseXmlSchemaTypeInternal_21(),
	XmlSchemaType_t1795078578::get_offset_of_DatatypeInternal_22(),
	XmlSchemaType_t1795078578::get_offset_of_resolvedDerivedBy_23(),
	XmlSchemaType_t1795078578::get_offset_of_finalResolved_24(),
	XmlSchemaType_t1795078578::get_offset_of_QNameInternal_25(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map42_26(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map43_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (XmlSchemaUnique_t403169513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (XmlSchemaUse_t3553149267)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1343[5] = 
{
	XmlSchemaUse_t3553149267::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (XmlSchemaValidity_t995929432)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1344[4] = 
{
	XmlSchemaValidity_t995929432::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (XmlSchemaValidationException_t2387044366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (XmlSchemaWhiteSpaceFacet_t2007056012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (XmlSchemaXPath_t604820427), -1, sizeof(XmlSchemaXPath_t604820427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1347[6] = 
{
	XmlSchemaXPath_t604820427::get_offset_of_xpath_16(),
	XmlSchemaXPath_t604820427::get_offset_of_nsmgr_17(),
	XmlSchemaXPath_t604820427::get_offset_of_isSelector_18(),
	XmlSchemaXPath_t604820427::get_offset_of_compiledExpression_19(),
	XmlSchemaXPath_t604820427::get_offset_of_currentPath_20(),
	XmlSchemaXPath_t604820427_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (XmlSeverityType_t3547578624)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1348[3] = 
{
	XmlSeverityType_t3547578624::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (ValidationHandler_t3342756761), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (XmlSchemaUtil_t3576230726), -1, sizeof(XmlSchemaUtil_t3576230726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1350[10] = 
{
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_StrictMsCompliant_3(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map4B_4(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map4C_5(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map4D_6(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map4E_7(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map4F_8(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map50_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (XmlSchemaReader_t3786681597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1351[3] = 
{
	XmlSchemaReader_t3786681597::get_offset_of_reader_2(),
	XmlSchemaReader_t3786681597::get_offset_of_handler_3(),
	XmlSchemaReader_t3786681597::get_offset_of_hasLineInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (XmlSchemaValidationFlags_t910489930)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1352[7] = 
{
	XmlSchemaValidationFlags_t910489930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (XmlTypeCode_t58293802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1353[56] = 
{
	XmlTypeCode_t58293802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (CodeIdentifier_t3245527920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (SchemaTypes_t3045759914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1356[9] = 
{
	SchemaTypes_t3045759914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (TypeData_t3979356678), -1, sizeof(TypeData_t3979356678_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1357[12] = 
{
	TypeData_t3979356678::get_offset_of_type_0(),
	TypeData_t3979356678::get_offset_of_elementName_1(),
	TypeData_t3979356678::get_offset_of_sType_2(),
	TypeData_t3979356678::get_offset_of_listItemType_3(),
	TypeData_t3979356678::get_offset_of_typeName_4(),
	TypeData_t3979356678::get_offset_of_fullTypeName_5(),
	TypeData_t3979356678::get_offset_of_listItemTypeData_6(),
	TypeData_t3979356678::get_offset_of_mappedType_7(),
	TypeData_t3979356678::get_offset_of_facet_8(),
	TypeData_t3979356678::get_offset_of_hasPublicConstructor_9(),
	TypeData_t3979356678::get_offset_of_nullableOverride_10(),
	TypeData_t3979356678_StaticFields::get_offset_of_keywords_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (TypeTranslator_t1077722680), -1, sizeof(TypeTranslator_t1077722680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1358[4] = 
{
	TypeTranslator_t1077722680_StaticFields::get_offset_of_nameCache_0(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_primitiveTypes_1(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_primitiveArrayTypes_2(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_nullableTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (XmlAnyAttributeAttribute_t713947513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (XmlAnyElementAttribute_t2502375235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1360[1] = 
{
	XmlAnyElementAttribute_t2502375235::get_offset_of_order_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (XmlAttributeAttribute_t850813783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1361[2] = 
{
	XmlAttributeAttribute_t850813783::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t850813783::get_offset_of_dataType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (XmlElementAttribute_t2182839281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1362[3] = 
{
	XmlElementAttribute_t2182839281::get_offset_of_elementName_0(),
	XmlElementAttribute_t2182839281::get_offset_of_type_1(),
	XmlElementAttribute_t2182839281::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (XmlEnumAttribute_t919400678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1363[1] = 
{
	XmlEnumAttribute_t919400678::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (XmlIgnoreAttribute_t2333915871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (XmlNamespaceDeclarationsAttribute_t2069522403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (XmlRootAttribute_t3527426713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1366[3] = 
{
	XmlRootAttribute_t3527426713::get_offset_of_elementName_0(),
	XmlRootAttribute_t3527426713::get_offset_of_isNullable_1(),
	XmlRootAttribute_t3527426713::get_offset_of_ns_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (XmlSerializerNamespaces_t3063656491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1367[1] = 
{
	XmlSerializerNamespaces_t3063656491::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (XmlTextAttribute_t3321178844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (XmlNodeChangedEventHandler_t2964483403), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (ValidationEventHandler_t1580700381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305138), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1371[8] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D36_0(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D37_1(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D38_2(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D39_3(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D40_4(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D41_5(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D43_6(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D44_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (U24ArrayTypeU2412_t3672778803)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778803 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (U24ArrayTypeU248_t1957337328)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337328 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (U24ArrayTypeU24256_t2038352955)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352955 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (U24ArrayTypeU241280_t628910058)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t628910058 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (U3CModuleU3E_t3783534216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (Locale_t4255929015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (BigInteger_t925946153), -1, sizeof(BigInteger_t925946153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1378[4] = 
{
	BigInteger_t925946153::get_offset_of_length_0(),
	BigInteger_t925946153::get_offset_of_data_1(),
	BigInteger_t925946153_StaticFields::get_offset_of_smallPrimes_2(),
	BigInteger_t925946153_StaticFields::get_offset_of_rng_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (Sign_t874893936)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1379[4] = 
{
	Sign_t874893936::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (ModulusRing_t80355992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1380[2] = 
{
	ModulusRing_t80355992::get_offset_of_mod_0(),
	ModulusRing_t80355992::get_offset_of_constant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (Kernel_t1353186456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (ConfidenceFactor_t1997037802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1382[7] = 
{
	ConfidenceFactor_t1997037802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (PrimalityTests_t3283102399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (PrimeGeneratorBase_t1053438168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (SequentialSearchPrimeGeneratorBase_t463670657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (ASN1_t924533536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1386[3] = 
{
	ASN1_t924533536::get_offset_of_m_nTag_0(),
	ASN1_t924533536::get_offset_of_m_aValue_1(),
	ASN1_t924533536::get_offset_of_elist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (ASN1Convert_t3301846397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (BitConverterLE_t2825370261), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (PKCS7_t3223261923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (ContentInfo_t1443605388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1390[2] = 
{
	ContentInfo_t1443605388::get_offset_of_contentType_0(),
	ContentInfo_t1443605388::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (EncryptedData_t2656813773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1391[4] = 
{
	EncryptedData_t2656813773::get_offset_of__version_0(),
	EncryptedData_t2656813773::get_offset_of__content_1(),
	EncryptedData_t2656813773::get_offset_of__encryptionAlgorithm_2(),
	EncryptedData_t2656813773::get_offset_of__encrypted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (ARC4Managed_t3379271383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1392[5] = 
{
	ARC4Managed_t3379271383::get_offset_of_key_12(),
	ARC4Managed_t3379271383::get_offset_of_state_13(),
	ARC4Managed_t3379271383::get_offset_of_x_14(),
	ARC4Managed_t3379271383::get_offset_of_y_15(),
	ARC4Managed_t3379271383::get_offset_of_m_disposed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (CryptoConvert_t4146607875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (KeyBuilder_t3965881085), -1, sizeof(KeyBuilder_t3965881085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1394[1] = 
{
	KeyBuilder_t3965881085_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (MD2_t726199179), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (MD2Managed_t1421454332), -1, sizeof(MD2Managed_t1421454332_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1396[6] = 
{
	MD2Managed_t1421454332::get_offset_of_state_4(),
	MD2Managed_t1421454332::get_offset_of_checksum_5(),
	MD2Managed_t1421454332::get_offset_of_buffer_6(),
	MD2Managed_t1421454332::get_offset_of_count_7(),
	MD2Managed_t1421454332::get_offset_of_x_8(),
	MD2Managed_t1421454332_StaticFields::get_offset_of_PI_SUBST_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (MD4_t1888998593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (MD4Managed_t2176273562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1398[5] = 
{
	MD4Managed_t2176273562::get_offset_of_state_4(),
	MD4Managed_t2176273562::get_offset_of_buffer_5(),
	MD4Managed_t2176273562::get_offset_of_count_6(),
	MD4Managed_t2176273562::get_offset_of_x_7(),
	MD4Managed_t2176273562::get_offset_of_digest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (PKCS1_t3312870481), -1, sizeof(PKCS1_t3312870481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1399[4] = 
{
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA512_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
