﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"

// System.Net.Mail.SmtpFailedRecipientsException
struct SmtpFailedRecipientsException_t1979361300;
// System.String
struct String_t;
// System.Net.Mail.SmtpFailedRecipientException[]
struct SmtpFailedRecipientExceptionU5BU5D_t1612260502;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;

// System.Void System.Net.Mail.SmtpFailedRecipientsException::.ctor()
extern "C"  void SmtpFailedRecipientsException__ctor_m2483360253 (SmtpFailedRecipientsException_t1979361300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpFailedRecipientsException::.ctor(System.String,System.Net.Mail.SmtpFailedRecipientException[])
extern "C"  void SmtpFailedRecipientsException__ctor_m3732671897 (SmtpFailedRecipientsException_t1979361300 * __this, String_t* ___message0, SmtpFailedRecipientExceptionU5BU5D_t1612260502* ___innerExceptions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpFailedRecipientsException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SmtpFailedRecipientsException__ctor_m2034884812 (SmtpFailedRecipientsException_t1979361300 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpFailedRecipientsException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SmtpFailedRecipientsException_System_Runtime_Serialization_ISerializable_GetObjectData_m1149798550 (SmtpFailedRecipientsException_t1979361300 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpFailedRecipientsException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SmtpFailedRecipientsException_GetObjectData_m2350998177 (SmtpFailedRecipientsException_t1979361300 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
