﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<FGMNT.DatabaseEntry>
struct List_1_t1874582323;
// FGMNT.DatabaseEntry
struct DatabaseEntry_t2505461191;
// FGMNT.ApplicationManager
struct ApplicationManager_t2067665347;
// System.Object
struct Il2CppObject;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.ApplicationManager/<ProcessResults>c__Iterator0
struct  U3CProcessResultsU3Ec__Iterator0_t2708174358  : public Il2CppObject
{
public:
	// System.String FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::metaData
	String_t* ___metaData_0;
	// System.Collections.Generic.List`1<FGMNT.DatabaseEntry> FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::<results>__1
	List_1_t1874582323 * ___U3CresultsU3E__1_1;
	// FGMNT.DatabaseEntry FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::<business>__2
	DatabaseEntry_t2505461191 * ___U3CbusinessU3E__2_2;
	// System.Boolean FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::reco
	bool ___reco_3;
	// FGMNT.ApplicationManager FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::$this
	ApplicationManager_t2067665347 * ___U24this_4;
	// System.Object FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_metaData_0() { return static_cast<int32_t>(offsetof(U3CProcessResultsU3Ec__Iterator0_t2708174358, ___metaData_0)); }
	inline String_t* get_metaData_0() const { return ___metaData_0; }
	inline String_t** get_address_of_metaData_0() { return &___metaData_0; }
	inline void set_metaData_0(String_t* value)
	{
		___metaData_0 = value;
		Il2CppCodeGenWriteBarrier(&___metaData_0, value);
	}

	inline static int32_t get_offset_of_U3CresultsU3E__1_1() { return static_cast<int32_t>(offsetof(U3CProcessResultsU3Ec__Iterator0_t2708174358, ___U3CresultsU3E__1_1)); }
	inline List_1_t1874582323 * get_U3CresultsU3E__1_1() const { return ___U3CresultsU3E__1_1; }
	inline List_1_t1874582323 ** get_address_of_U3CresultsU3E__1_1() { return &___U3CresultsU3E__1_1; }
	inline void set_U3CresultsU3E__1_1(List_1_t1874582323 * value)
	{
		___U3CresultsU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresultsU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CbusinessU3E__2_2() { return static_cast<int32_t>(offsetof(U3CProcessResultsU3Ec__Iterator0_t2708174358, ___U3CbusinessU3E__2_2)); }
	inline DatabaseEntry_t2505461191 * get_U3CbusinessU3E__2_2() const { return ___U3CbusinessU3E__2_2; }
	inline DatabaseEntry_t2505461191 ** get_address_of_U3CbusinessU3E__2_2() { return &___U3CbusinessU3E__2_2; }
	inline void set_U3CbusinessU3E__2_2(DatabaseEntry_t2505461191 * value)
	{
		___U3CbusinessU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbusinessU3E__2_2, value);
	}

	inline static int32_t get_offset_of_reco_3() { return static_cast<int32_t>(offsetof(U3CProcessResultsU3Ec__Iterator0_t2708174358, ___reco_3)); }
	inline bool get_reco_3() const { return ___reco_3; }
	inline bool* get_address_of_reco_3() { return &___reco_3; }
	inline void set_reco_3(bool value)
	{
		___reco_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CProcessResultsU3Ec__Iterator0_t2708174358, ___U24this_4)); }
	inline ApplicationManager_t2067665347 * get_U24this_4() const { return ___U24this_4; }
	inline ApplicationManager_t2067665347 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(ApplicationManager_t2067665347 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CProcessResultsU3Ec__Iterator0_t2708174358, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CProcessResultsU3Ec__Iterator0_t2708174358, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CProcessResultsU3Ec__Iterator0_t2708174358, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
