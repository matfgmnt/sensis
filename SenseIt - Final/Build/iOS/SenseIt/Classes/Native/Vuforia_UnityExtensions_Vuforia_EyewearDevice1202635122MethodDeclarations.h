﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearDevice
struct EyewearDevice_t1202635122;

// System.Void Vuforia.EyewearDevice::.ctor()
extern "C"  void EyewearDevice__ctor_m924025370 (EyewearDevice_t1202635122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
