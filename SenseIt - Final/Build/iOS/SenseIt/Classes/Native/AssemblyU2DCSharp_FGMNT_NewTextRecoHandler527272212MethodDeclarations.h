﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_InitStat4409649.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Updat1473252352.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197.h"

// FGMNT.NewTextRecoHandler
struct NewTextRecoHandler_t527272212;
// Vuforia.WordResult
struct WordResult_t1915507197;
// Vuforia.Word
struct Word_t3872119486;

// System.Void FGMNT.NewTextRecoHandler::.ctor()
extern "C"  void NewTextRecoHandler__ctor_m3611108873 (NewTextRecoHandler_t527272212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewTextRecoHandler::Awake()
extern "C"  void NewTextRecoHandler_Awake_m4240499132 (NewTextRecoHandler_t527272212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewTextRecoHandler::Start()
extern "C"  void NewTextRecoHandler_Start_m3651053881 (NewTextRecoHandler_t527272212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewTextRecoHandler::OnInitialized()
extern "C"  void NewTextRecoHandler_OnInitialized_m2031921766 (NewTextRecoHandler_t527272212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewTextRecoHandler::OnInitError(Vuforia.TargetFinder/InitState)
extern "C"  void NewTextRecoHandler_OnInitError_m3792273205 (NewTextRecoHandler_t527272212 * __this, int32_t ___initError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewTextRecoHandler::OnUpdateError(Vuforia.TargetFinder/UpdateState)
extern "C"  void NewTextRecoHandler_OnUpdateError_m640992533 (NewTextRecoHandler_t527272212 * __this, int32_t ___updateError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewTextRecoHandler::OnWordDetected(Vuforia.WordResult)
extern "C"  void NewTextRecoHandler_OnWordDetected_m3927385037 (NewTextRecoHandler_t527272212 * __this, WordResult_t1915507197 * ___wordResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewTextRecoHandler::OnWordLost(Vuforia.Word)
extern "C"  void NewTextRecoHandler_OnWordLost_m2351571512 (NewTextRecoHandler_t527272212 * __this, Il2CppObject * ___word0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
