﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_ComponentModel_DoWorkEventArgs62745097.h"
#include "System_System_ComponentModel_RunWorkerCompletedEve3512304465.h"

// System.Net.Mail.SmtpClient/<SendAsync>c__AnonStorey5
struct U3CSendAsyncU3Ec__AnonStorey5_t3553274745;
// System.Object
struct Il2CppObject;
// System.ComponentModel.DoWorkEventArgs
struct DoWorkEventArgs_t62745097;
// System.ComponentModel.RunWorkerCompletedEventArgs
struct RunWorkerCompletedEventArgs_t3512304465;

// System.Void System.Net.Mail.SmtpClient/<SendAsync>c__AnonStorey5::.ctor()
extern "C"  void U3CSendAsyncU3Ec__AnonStorey5__ctor_m1534443337 (U3CSendAsyncU3Ec__AnonStorey5_t3553274745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpClient/<SendAsync>c__AnonStorey5::<>m__5(System.Object,System.ComponentModel.DoWorkEventArgs)
extern "C"  void U3CSendAsyncU3Ec__AnonStorey5_U3CU3Em__5_m2113639285 (U3CSendAsyncU3Ec__AnonStorey5_t3553274745 * __this, Il2CppObject * ___o0, DoWorkEventArgs_t62745097 * ___ea1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.SmtpClient/<SendAsync>c__AnonStorey5::<>m__6(System.Object,System.ComponentModel.RunWorkerCompletedEventArgs)
extern "C"  void U3CSendAsyncU3Ec__AnonStorey5_U3CU3Em__6_m2456723580 (U3CSendAsyncU3Ec__AnonStorey5_t3553274745 * __this, Il2CppObject * ___o0, RunWorkerCompletedEventArgs_t3512304465 * ___ea1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
