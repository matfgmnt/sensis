﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Ren2572170852.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// UnityEngine.Experimental.Rendering.RenderLoop/PrepareRenderLoop
struct PrepareRenderLoop_t473970113;
// System.Object
struct Il2CppObject;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

// System.Void UnityEngine.Experimental.Rendering.RenderLoop/PrepareRenderLoop::.ctor(System.Object,System.IntPtr)
extern "C"  void PrepareRenderLoop__ctor_m3767967369 (PrepareRenderLoop_t473970113 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Rendering.RenderLoop/PrepareRenderLoop::Invoke(UnityEngine.Camera[],UnityEngine.Experimental.Rendering.RenderLoop)
extern "C"  bool PrepareRenderLoop_Invoke_m3948996340 (PrepareRenderLoop_t473970113 * __this, CameraU5BU5D_t3079764780* ___cameras0, RenderLoop_t2572170852  ___outputLoop1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UnityEngine.Experimental.Rendering.RenderLoop/PrepareRenderLoop::BeginInvoke(UnityEngine.Camera[],UnityEngine.Experimental.Rendering.RenderLoop,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PrepareRenderLoop_BeginInvoke_m2331553633 (PrepareRenderLoop_t473970113 * __this, CameraU5BU5D_t3079764780* ___cameras0, RenderLoop_t2572170852  ___outputLoop1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Rendering.RenderLoop/PrepareRenderLoop::EndInvoke(System.IAsyncResult)
extern "C"  bool PrepareRenderLoop_EndInvoke_m1000409351 (PrepareRenderLoop_t473970113 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
