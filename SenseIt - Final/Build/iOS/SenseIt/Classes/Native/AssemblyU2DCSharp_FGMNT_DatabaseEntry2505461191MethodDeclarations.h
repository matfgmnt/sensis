﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// FGMNT.DatabaseEntry
struct DatabaseEntry_t2505461191;

// System.Void FGMNT.DatabaseEntry::.ctor()
extern "C"  void DatabaseEntry__ctor_m2546785644 (DatabaseEntry_t2505461191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
