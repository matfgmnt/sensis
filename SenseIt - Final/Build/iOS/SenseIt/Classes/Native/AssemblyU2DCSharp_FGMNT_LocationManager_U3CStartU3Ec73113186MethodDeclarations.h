﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// FGMNT.LocationManager/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t73113186;
// System.Object
struct Il2CppObject;

// System.Void FGMNT.LocationManager/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m263574089 (U3CStartU3Ec__Iterator0_t73113186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FGMNT.LocationManager/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1090372199 (U3CStartU3Ec__Iterator0_t73113186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FGMNT.LocationManager/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1384653843 (U3CStartU3Ec__Iterator0_t73113186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FGMNT.LocationManager/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1763688747 (U3CStartU3Ec__Iterator0_t73113186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.LocationManager/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m1348692492 (U3CStartU3Ec__Iterator0_t73113186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.LocationManager/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m790310926 (U3CStartU3Ec__Iterator0_t73113186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
