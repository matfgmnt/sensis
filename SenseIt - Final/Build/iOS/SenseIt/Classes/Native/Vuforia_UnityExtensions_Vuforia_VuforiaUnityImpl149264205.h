﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"


#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnityImpl
struct  VuforiaUnityImpl_t149264205  : public Il2CppObject
{
public:

public:
};

struct VuforiaUnityImpl_t149264205_StaticFields
{
public:
	// System.Boolean Vuforia.VuforiaUnityImpl::mRendererDirty
	bool ___mRendererDirty_0;

public:
	inline static int32_t get_offset_of_mRendererDirty_0() { return static_cast<int32_t>(offsetof(VuforiaUnityImpl_t149264205_StaticFields, ___mRendererDirty_0)); }
	inline bool get_mRendererDirty_0() const { return ___mRendererDirty_0; }
	inline bool* get_address_of_mRendererDirty_0() { return &___mRendererDirty_0; }
	inline void set_mRendererDirty_0(bool value)
	{
		___mRendererDirty_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
