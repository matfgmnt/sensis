﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_ChunkStream91719323.h"
#include "System_System_Net_ChunkStream_State4001596355.h"
#include "System_System_Net_ChunkStream_Chunk3860501603.h"
#include "System_System_Net_Configuration_AuthenticationModu1084300802.h"
#include "System_System_Net_Configuration_AuthenticationModu1426459758.h"
#include "System_System_Net_Configuration_AuthenticationModu1750570770.h"
#include "System_System_Net_Configuration_BypassElementColle3411512674.h"
#include "System_System_Net_Configuration_BypassElement4253212366.h"
#include "System_System_Net_Configuration_ConnectionManageme3244012643.h"
#include "System_System_Net_Configuration_ConnectionManageme1734801665.h"
#include "System_System_Net_Configuration_ConnectionManageme2657447783.h"
#include "System_System_Net_Configuration_HandlersUtil1360800625.h"
#include "System_System_Net_Configuration_ConnectionManageme1533889992.h"
#include "System_System_Net_Configuration_DefaultProxySectio2916409848.h"
#include "System_System_Net_Configuration_FtpCachePolicyEleme919314008.h"
#include "System_System_Net_Configuration_HttpCachePolicyElem129882946.h"
#include "System_System_Net_Configuration_HttpWebRequestElem2106051069.h"
#include "System_System_Net_Configuration_Ipv6Element977943121.h"
#include "System_System_Net_Configuration_MailSettingsSectionG25838306.h"
#include "System_System_Net_Configuration_ModuleElement3031348726.h"
#include "System_System_Net_Configuration_NetConfigurationHan415947373.h"
#include "System_System_Net_Configuration_NetSectionGroup2546413291.h"
#include "System_System_Net_Configuration_PerformanceCounter1606215027.h"
#include "System_System_Net_Configuration_ProxyElement1414493002.h"
#include "System_System_Net_Configuration_ProxyElement_Bypas3271087145.h"
#include "System_System_Net_Configuration_ProxyElement_UseSy1457033350.h"
#include "System_System_Net_Configuration_ProxyElement_AutoDe735769531.h"
#include "System_System_Net_Configuration_RequestCachingSect1642835391.h"
#include "System_System_Net_Configuration_ServicePointManage1132364388.h"
#include "System_System_Net_Configuration_SettingsSection2300716058.h"
#include "System_System_Net_Configuration_SmtpNetworkElement427678180.h"
#include "System_System_Net_Configuration_SmtpSection3258969749.h"
#include "System_System_Net_Configuration_SmtpSpecifiedPicku3442976541.h"
#include "System_System_Net_Configuration_SocketElement792962077.h"
#include "System_System_Net_Configuration_WebProxyScriptElem1017943775.h"
#include "System_System_Net_Configuration_WebRequestModuleEl2218695785.h"
#include "System_System_Net_Configuration_WebRequestModuleEl4070853259.h"
#include "System_System_Net_Configuration_WebRequestModuleHa3598459681.h"
#include "System_System_Net_Configuration_WebRequestModulesS3717257007.h"
#include "System_System_Net_CookieCollection521422364.h"
#include "System_System_Net_CookieCollection_CookieCollectio3570802680.h"
#include "System_System_Net_CookieContainer2808809223.h"
#include "System_System_Net_Cookie3154017544.h"
#include "System_System_Net_CookieException1505724635.h"
#include "System_System_Net_CredentialCache1992799279.h"
#include "System_System_Net_CredentialCache_CredentialCacheK2550644004.h"
#include "System_System_Net_CredentialCache_CredentialCacheF1860630919.h"
#include "System_System_Net_DecompressionMethods2530166567.h"
#include "System_System_Net_DefaultCertificatePolicy2545332216.h"
#include "System_System_Net_Dns1335526197.h"
#include "System_System_Net_EndPoint4156119363.h"
#include "System_System_Net_FileWebRequestCreator1109072211.h"
#include "System_System_Net_FileWebRequest1571840111.h"
#include "System_System_Net_FileWebRequest_FileWebStream1952319648.h"
#include "System_System_Net_FileWebRequest_GetResponseCallba3725471744.h"
#include "System_System_Net_FileWebResponse1934981865.h"
#include "System_System_Net_FtpAsyncResult770082413.h"
#include "System_System_Net_FtpDataStream3588258764.h"
#include "System_System_Net_FtpDataStream_WriteDelegate888270799.h"
#include "System_System_Net_FtpDataStream_ReadDelegate1559754630.h"
#include "System_System_Net_FtpRequestCreator3711983251.h"
#include "System_System_Net_FtpStatusCode1448112771.h"
#include "System_System_Net_FtpWebRequest3120721823.h"
#include "System_System_Net_FtpWebRequest_RequestState4256633122.h"
#include "System_System_Net_FtpStatus3714482970.h"
#include "System_System_Net_FtpWebResponse2609078769.h"
#include "System_System_Net_GlobalProxySelection2251180943.h"
#include "System_System_Net_HttpRequestCreator1416559589.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "System_System_Net_HttpVersion1276659706.h"
#include "System_System_Net_HttpWebRequest1951404513.h"
#include "System_System_Net_HttpWebResponse2828383075.h"
#include "System_System_Net_CookieParser1405985527.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "System_System_Net_IPHostEntry994738509.h"
#include "System_System_Net_IPv6Address2596635879.h"
#include "System_System_Net_Mail_AlternateViewCollection2752355131.h"
#include "System_System_Net_Mail_AlternateView1668264691.h"
#include "System_System_Net_Mail_AttachmentBase3226558610.h"
#include "System_System_Net_Mail_AttachmentCollection537487287.h"
#include "System_System_Net_Mail_Attachment2609929423.h"
#include "System_System_Net_Mail_DeliveryNotificationOptions653946403.h"
#include "System_System_Net_Mail_LinkedResourceCollection3361889291.h"
#include "System_System_Net_Mail_LinkedResource830466835.h"
#include "System_System_Net_Mail_MailAddressCollection3502002665.h"
#include "System_System_Net_Mail_MailAddress1980919589.h"
#include "System_System_Net_Mail_MailMessage460962712.h"
#include "System_System_Net_Mail_MailPriority2524119153.h"
#include "System_System_Net_Mail_SmtpClient2719616745.h"
#include "System_System_Net_Mail_SmtpClient_AuthMechs3916064079.h"
#include "System_System_Net_Mail_SmtpClient_CancellationExcep544132923.h"
#include "System_System_Net_Mail_SmtpClient_SmtpResponse616735068.h"




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (ChunkStream_t91719323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[9] = 
{
	ChunkStream_t91719323::get_offset_of_headers_0(),
	ChunkStream_t91719323::get_offset_of_chunkSize_1(),
	ChunkStream_t91719323::get_offset_of_chunkRead_2(),
	ChunkStream_t91719323::get_offset_of_state_3(),
	ChunkStream_t91719323::get_offset_of_saved_4(),
	ChunkStream_t91719323::get_offset_of_sawCR_5(),
	ChunkStream_t91719323::get_offset_of_gotit_6(),
	ChunkStream_t91719323::get_offset_of_trailerState_7(),
	ChunkStream_t91719323::get_offset_of_chunks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (State_t4001596355)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1701[5] = 
{
	State_t4001596355::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (Chunk_t3860501603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[2] = 
{
	Chunk_t3860501603::get_offset_of_Bytes_0(),
	Chunk_t3860501603::get_offset_of_Offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (AuthenticationModuleElementCollection_t1084300802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (AuthenticationModuleElement_t1426459758), -1, sizeof(AuthenticationModuleElement_t1426459758_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1704[2] = 
{
	AuthenticationModuleElement_t1426459758_StaticFields::get_offset_of_properties_13(),
	AuthenticationModuleElement_t1426459758_StaticFields::get_offset_of_typeProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (AuthenticationModulesSection_t1750570770), -1, sizeof(AuthenticationModulesSection_t1750570770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1705[2] = 
{
	AuthenticationModulesSection_t1750570770_StaticFields::get_offset_of_properties_17(),
	AuthenticationModulesSection_t1750570770_StaticFields::get_offset_of_authenticationModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (BypassElementCollection_t3411512674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (BypassElement_t4253212366), -1, sizeof(BypassElement_t4253212366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1707[2] = 
{
	BypassElement_t4253212366_StaticFields::get_offset_of_properties_13(),
	BypassElement_t4253212366_StaticFields::get_offset_of_addressProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (ConnectionManagementElementCollection_t3244012643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (ConnectionManagementElement_t1734801665), -1, sizeof(ConnectionManagementElement_t1734801665_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1709[3] = 
{
	ConnectionManagementElement_t1734801665_StaticFields::get_offset_of_properties_13(),
	ConnectionManagementElement_t1734801665_StaticFields::get_offset_of_addressProp_14(),
	ConnectionManagementElement_t1734801665_StaticFields::get_offset_of_maxConnectionProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (ConnectionManagementData_t2657447783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	ConnectionManagementData_t2657447783::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (HandlersUtil_t1360800625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (ConnectionManagementSection_t1533889992), -1, sizeof(ConnectionManagementSection_t1533889992_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1712[2] = 
{
	ConnectionManagementSection_t1533889992_StaticFields::get_offset_of_connectionManagementProp_17(),
	ConnectionManagementSection_t1533889992_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (DefaultProxySection_t2916409848), -1, sizeof(DefaultProxySection_t2916409848_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1713[6] = 
{
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_properties_17(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_bypassListProp_18(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_enabledProp_19(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_moduleProp_20(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_proxyProp_21(),
	DefaultProxySection_t2916409848_StaticFields::get_offset_of_useDefaultCredentialsProp_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (FtpCachePolicyElement_t919314008), -1, sizeof(FtpCachePolicyElement_t919314008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1714[2] = 
{
	FtpCachePolicyElement_t919314008_StaticFields::get_offset_of_policyLevelProp_13(),
	FtpCachePolicyElement_t919314008_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (HttpCachePolicyElement_t129882946), -1, sizeof(HttpCachePolicyElement_t129882946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1715[5] = 
{
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_maximumAgeProp_13(),
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_maximumStaleProp_14(),
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_minimumFreshProp_15(),
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_policyLevelProp_16(),
	HttpCachePolicyElement_t129882946_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (HttpWebRequestElement_t2106051069), -1, sizeof(HttpWebRequestElement_t2106051069_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1716[5] = 
{
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_maximumErrorResponseLengthProp_13(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_maximumResponseHeadersLengthProp_14(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_maximumUnauthorizedUploadLengthProp_15(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_useUnsafeHeaderParsingProp_16(),
	HttpWebRequestElement_t2106051069_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (Ipv6Element_t977943121), -1, sizeof(Ipv6Element_t977943121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1717[2] = 
{
	Ipv6Element_t977943121_StaticFields::get_offset_of_properties_13(),
	Ipv6Element_t977943121_StaticFields::get_offset_of_enabledProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (MailSettingsSectionGroup_t25838306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (ModuleElement_t3031348726), -1, sizeof(ModuleElement_t3031348726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1719[2] = 
{
	ModuleElement_t3031348726_StaticFields::get_offset_of_properties_13(),
	ModuleElement_t3031348726_StaticFields::get_offset_of_typeProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (NetConfigurationHandler_t415947373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (NetSectionGroup_t2546413291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (PerformanceCountersElement_t1606215027), -1, sizeof(PerformanceCountersElement_t1606215027_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1722[2] = 
{
	PerformanceCountersElement_t1606215027_StaticFields::get_offset_of_enabledProp_13(),
	PerformanceCountersElement_t1606215027_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (ProxyElement_t1414493002), -1, sizeof(ProxyElement_t1414493002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1723[6] = 
{
	ProxyElement_t1414493002_StaticFields::get_offset_of_properties_13(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_autoDetectProp_14(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_bypassOnLocalProp_15(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_proxyAddressProp_16(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_scriptLocationProp_17(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_useSystemDefaultProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (BypassOnLocalValues_t3271087145)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1724[4] = 
{
	BypassOnLocalValues_t3271087145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (UseSystemDefaultValues_t1457033350)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1725[4] = 
{
	UseSystemDefaultValues_t1457033350::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (AutoDetectValues_t735769531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1726[4] = 
{
	AutoDetectValues_t735769531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (RequestCachingSection_t1642835391), -1, sizeof(RequestCachingSection_t1642835391_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1727[7] = 
{
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_properties_17(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_defaultFtpCachePolicyProp_18(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_defaultHttpCachePolicyProp_19(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_defaultPolicyLevelProp_20(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_disableAllCachingProp_21(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_isPrivateCacheProp_22(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_unspecifiedMaximumAgeProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (ServicePointManagerElement_t1132364388), -1, sizeof(ServicePointManagerElement_t1132364388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1728[7] = 
{
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_properties_13(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_checkCertificateNameProp_14(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_checkCertificateRevocationListProp_15(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_dnsRefreshTimeoutProp_16(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_enableDnsRoundRobinProp_17(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_expect100ContinueProp_18(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_useNagleAlgorithmProp_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (SettingsSection_t2300716058), -1, sizeof(SettingsSection_t2300716058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1729[7] = 
{
	SettingsSection_t2300716058_StaticFields::get_offset_of_properties_17(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_httpWebRequestProp_18(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_ipv6Prop_19(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_performanceCountersProp_20(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_servicePointManagerProp_21(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_webProxyScriptProp_22(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_socketProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (SmtpNetworkElement_t427678180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (SmtpSection_t3258969749), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (SmtpSpecifiedPickupDirectoryElement_t3442976541), -1, sizeof(SmtpSpecifiedPickupDirectoryElement_t3442976541_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1732[2] = 
{
	SmtpSpecifiedPickupDirectoryElement_t3442976541_StaticFields::get_offset_of_pickupDirectoryLocationProp_13(),
	SmtpSpecifiedPickupDirectoryElement_t3442976541_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (SocketElement_t792962077), -1, sizeof(SocketElement_t792962077_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1733[3] = 
{
	SocketElement_t792962077_StaticFields::get_offset_of_properties_13(),
	SocketElement_t792962077_StaticFields::get_offset_of_alwaysUseCompletionPortsForAcceptProp_14(),
	SocketElement_t792962077_StaticFields::get_offset_of_alwaysUseCompletionPortsForConnectProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (WebProxyScriptElement_t1017943775), -1, sizeof(WebProxyScriptElement_t1017943775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1734[2] = 
{
	WebProxyScriptElement_t1017943775_StaticFields::get_offset_of_downloadTimeoutProp_13(),
	WebProxyScriptElement_t1017943775_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (WebRequestModuleElementCollection_t2218695785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (WebRequestModuleElement_t4070853259), -1, sizeof(WebRequestModuleElement_t4070853259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1736[3] = 
{
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_properties_13(),
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_prefixProp_14(),
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_typeProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (WebRequestModuleHandler_t3598459681), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (WebRequestModulesSection_t3717257007), -1, sizeof(WebRequestModulesSection_t3717257007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1738[2] = 
{
	WebRequestModulesSection_t3717257007_StaticFields::get_offset_of_properties_17(),
	WebRequestModulesSection_t3717257007_StaticFields::get_offset_of_webRequestModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (CookieCollection_t521422364), -1, sizeof(CookieCollection_t521422364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1739[2] = 
{
	CookieCollection_t521422364::get_offset_of_list_0(),
	CookieCollection_t521422364_StaticFields::get_offset_of_Comparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (CookieCollectionComparer_t3570802680), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (CookieContainer_t2808809223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[4] = 
{
	CookieContainer_t2808809223::get_offset_of_capacity_0(),
	CookieContainer_t2808809223::get_offset_of_perDomainCapacity_1(),
	CookieContainer_t2808809223::get_offset_of_maxCookieSize_2(),
	CookieContainer_t2808809223::get_offset_of_cookies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (Cookie_t3154017544), -1, sizeof(Cookie_t3154017544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1742[18] = 
{
	Cookie_t3154017544::get_offset_of_comment_0(),
	Cookie_t3154017544::get_offset_of_commentUri_1(),
	Cookie_t3154017544::get_offset_of_discard_2(),
	Cookie_t3154017544::get_offset_of_domain_3(),
	Cookie_t3154017544::get_offset_of_expires_4(),
	Cookie_t3154017544::get_offset_of_httpOnly_5(),
	Cookie_t3154017544::get_offset_of_name_6(),
	Cookie_t3154017544::get_offset_of_path_7(),
	Cookie_t3154017544::get_offset_of_port_8(),
	Cookie_t3154017544::get_offset_of_ports_9(),
	Cookie_t3154017544::get_offset_of_secure_10(),
	Cookie_t3154017544::get_offset_of_timestamp_11(),
	Cookie_t3154017544::get_offset_of_val_12(),
	Cookie_t3154017544::get_offset_of_version_13(),
	Cookie_t3154017544_StaticFields::get_offset_of_reservedCharsName_14(),
	Cookie_t3154017544_StaticFields::get_offset_of_portSeparators_15(),
	Cookie_t3154017544_StaticFields::get_offset_of_tspecials_16(),
	Cookie_t3154017544::get_offset_of_exact_domain_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (CookieException_t1505724635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (CredentialCache_t1992799279), -1, sizeof(CredentialCache_t1992799279_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1744[3] = 
{
	CredentialCache_t1992799279_StaticFields::get_offset_of_empty_0(),
	CredentialCache_t1992799279::get_offset_of_cache_1(),
	CredentialCache_t1992799279::get_offset_of_cacheForHost_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (CredentialCacheKey_t2550644004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1745[5] = 
{
	CredentialCacheKey_t2550644004::get_offset_of_uriPrefix_0(),
	CredentialCacheKey_t2550644004::get_offset_of_authType_1(),
	CredentialCacheKey_t2550644004::get_offset_of_absPath_2(),
	CredentialCacheKey_t2550644004::get_offset_of_len_3(),
	CredentialCacheKey_t2550644004::get_offset_of_hash_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (CredentialCacheForHostKey_t1860630919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[4] = 
{
	CredentialCacheForHostKey_t1860630919::get_offset_of_host_0(),
	CredentialCacheForHostKey_t1860630919::get_offset_of_port_1(),
	CredentialCacheForHostKey_t1860630919::get_offset_of_authType_2(),
	CredentialCacheForHostKey_t1860630919::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (DecompressionMethods_t2530166567)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1747[4] = 
{
	DecompressionMethods_t2530166567::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (DefaultCertificatePolicy_t2545332216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (Dns_t1335526197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (EndPoint_t4156119363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (FileWebRequestCreator_t1109072211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (FileWebRequest_t1571840111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[14] = 
{
	FileWebRequest_t1571840111::get_offset_of_uri_6(),
	FileWebRequest_t1571840111::get_offset_of_webHeaders_7(),
	FileWebRequest_t1571840111::get_offset_of_credentials_8(),
	FileWebRequest_t1571840111::get_offset_of_connectionGroup_9(),
	FileWebRequest_t1571840111::get_offset_of_contentLength_10(),
	FileWebRequest_t1571840111::get_offset_of_fileAccess_11(),
	FileWebRequest_t1571840111::get_offset_of_method_12(),
	FileWebRequest_t1571840111::get_offset_of_proxy_13(),
	FileWebRequest_t1571840111::get_offset_of_preAuthenticate_14(),
	FileWebRequest_t1571840111::get_offset_of_timeout_15(),
	FileWebRequest_t1571840111::get_offset_of_webResponse_16(),
	FileWebRequest_t1571840111::get_offset_of_requestEndEvent_17(),
	FileWebRequest_t1571840111::get_offset_of_requesting_18(),
	FileWebRequest_t1571840111::get_offset_of_asyncResponding_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (FileWebStream_t1952319648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1753[1] = 
{
	FileWebStream_t1952319648::get_offset_of_webRequest_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (GetResponseCallback_t3725471744), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (FileWebResponse_t1934981865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[5] = 
{
	FileWebResponse_t1934981865::get_offset_of_responseUri_1(),
	FileWebResponse_t1934981865::get_offset_of_fileStream_2(),
	FileWebResponse_t1934981865::get_offset_of_contentLength_3(),
	FileWebResponse_t1934981865::get_offset_of_webHeaders_4(),
	FileWebResponse_t1934981865::get_offset_of_disposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (FtpAsyncResult_t770082413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1756[9] = 
{
	FtpAsyncResult_t770082413::get_offset_of_response_0(),
	FtpAsyncResult_t770082413::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t770082413::get_offset_of_exception_2(),
	FtpAsyncResult_t770082413::get_offset_of_callback_3(),
	FtpAsyncResult_t770082413::get_offset_of_stream_4(),
	FtpAsyncResult_t770082413::get_offset_of_state_5(),
	FtpAsyncResult_t770082413::get_offset_of_completed_6(),
	FtpAsyncResult_t770082413::get_offset_of_synch_7(),
	FtpAsyncResult_t770082413::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (FtpDataStream_t3588258764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[5] = 
{
	FtpDataStream_t3588258764::get_offset_of_request_2(),
	FtpDataStream_t3588258764::get_offset_of_networkStream_3(),
	FtpDataStream_t3588258764::get_offset_of_disposed_4(),
	FtpDataStream_t3588258764::get_offset_of_isRead_5(),
	FtpDataStream_t3588258764::get_offset_of_totalRead_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (WriteDelegate_t888270799), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (ReadDelegate_t1559754630), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (FtpRequestCreator_t3711983251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (FtpStatusCode_t1448112771)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1761[38] = 
{
	FtpStatusCode_t1448112771::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (FtpWebRequest_t3120721823), -1, sizeof(FtpWebRequest_t3120721823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1762[31] = 
{
	FtpWebRequest_t3120721823::get_offset_of_requestUri_6(),
	FtpWebRequest_t3120721823::get_offset_of_file_name_7(),
	FtpWebRequest_t3120721823::get_offset_of_servicePoint_8(),
	FtpWebRequest_t3120721823::get_offset_of_origDataStream_9(),
	FtpWebRequest_t3120721823::get_offset_of_dataStream_10(),
	FtpWebRequest_t3120721823::get_offset_of_controlStream_11(),
	FtpWebRequest_t3120721823::get_offset_of_controlReader_12(),
	FtpWebRequest_t3120721823::get_offset_of_credentials_13(),
	FtpWebRequest_t3120721823::get_offset_of_hostEntry_14(),
	FtpWebRequest_t3120721823::get_offset_of_localEndPoint_15(),
	FtpWebRequest_t3120721823::get_offset_of_proxy_16(),
	FtpWebRequest_t3120721823::get_offset_of_timeout_17(),
	FtpWebRequest_t3120721823::get_offset_of_rwTimeout_18(),
	FtpWebRequest_t3120721823::get_offset_of_offset_19(),
	FtpWebRequest_t3120721823::get_offset_of_binary_20(),
	FtpWebRequest_t3120721823::get_offset_of_enableSsl_21(),
	FtpWebRequest_t3120721823::get_offset_of_usePassive_22(),
	FtpWebRequest_t3120721823::get_offset_of_keepAlive_23(),
	FtpWebRequest_t3120721823::get_offset_of_method_24(),
	FtpWebRequest_t3120721823::get_offset_of_renameTo_25(),
	FtpWebRequest_t3120721823::get_offset_of_locker_26(),
	FtpWebRequest_t3120721823::get_offset_of_requestState_27(),
	FtpWebRequest_t3120721823::get_offset_of_asyncResult_28(),
	FtpWebRequest_t3120721823::get_offset_of_ftpResponse_29(),
	FtpWebRequest_t3120721823::get_offset_of_requestStream_30(),
	FtpWebRequest_t3120721823::get_offset_of_initial_path_31(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_supportedCommands_32(),
	FtpWebRequest_t3120721823::get_offset_of_callback_33(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_34(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_35(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (RequestState_t4256633122)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1763[10] = 
{
	RequestState_t4256633122::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (FtpStatus_t3714482970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[2] = 
{
	FtpStatus_t3714482970::get_offset_of_statusCode_0(),
	FtpStatus_t3714482970::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (FtpWebResponse_t2609078769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[12] = 
{
	FtpWebResponse_t2609078769::get_offset_of_stream_1(),
	FtpWebResponse_t2609078769::get_offset_of_uri_2(),
	FtpWebResponse_t2609078769::get_offset_of_statusCode_3(),
	FtpWebResponse_t2609078769::get_offset_of_lastModified_4(),
	FtpWebResponse_t2609078769::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t2609078769::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t2609078769::get_offset_of_exitMessage_7(),
	FtpWebResponse_t2609078769::get_offset_of_statusDescription_8(),
	FtpWebResponse_t2609078769::get_offset_of_method_9(),
	FtpWebResponse_t2609078769::get_offset_of_disposed_10(),
	FtpWebResponse_t2609078769::get_offset_of_request_11(),
	FtpWebResponse_t2609078769::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (GlobalProxySelection_t2251180943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (HttpRequestCreator_t1416559589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (HttpStatusCode_t1898409641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1768[47] = 
{
	HttpStatusCode_t1898409641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (HttpVersion_t1276659706), -1, sizeof(HttpVersion_t1276659706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1769[2] = 
{
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (HttpWebRequest_t1951404513), -1, sizeof(HttpWebRequest_t1951404513_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1770[50] = 
{
	HttpWebRequest_t1951404513::get_offset_of_requestUri_6(),
	HttpWebRequest_t1951404513::get_offset_of_actualUri_7(),
	HttpWebRequest_t1951404513::get_offset_of_hostChanged_8(),
	HttpWebRequest_t1951404513::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t1951404513::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t1951404513::get_offset_of_certificates_11(),
	HttpWebRequest_t1951404513::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t1951404513::get_offset_of_contentLength_13(),
	HttpWebRequest_t1951404513::get_offset_of_continueDelegate_14(),
	HttpWebRequest_t1951404513::get_offset_of_cookieContainer_15(),
	HttpWebRequest_t1951404513::get_offset_of_credentials_16(),
	HttpWebRequest_t1951404513::get_offset_of_haveResponse_17(),
	HttpWebRequest_t1951404513::get_offset_of_haveRequest_18(),
	HttpWebRequest_t1951404513::get_offset_of_requestSent_19(),
	HttpWebRequest_t1951404513::get_offset_of_webHeaders_20(),
	HttpWebRequest_t1951404513::get_offset_of_keepAlive_21(),
	HttpWebRequest_t1951404513::get_offset_of_maxAutoRedirect_22(),
	HttpWebRequest_t1951404513::get_offset_of_mediaType_23(),
	HttpWebRequest_t1951404513::get_offset_of_method_24(),
	HttpWebRequest_t1951404513::get_offset_of_initialMethod_25(),
	HttpWebRequest_t1951404513::get_offset_of_pipelined_26(),
	HttpWebRequest_t1951404513::get_offset_of_preAuthenticate_27(),
	HttpWebRequest_t1951404513::get_offset_of_usedPreAuth_28(),
	HttpWebRequest_t1951404513::get_offset_of_version_29(),
	HttpWebRequest_t1951404513::get_offset_of_actualVersion_30(),
	HttpWebRequest_t1951404513::get_offset_of_proxy_31(),
	HttpWebRequest_t1951404513::get_offset_of_sendChunked_32(),
	HttpWebRequest_t1951404513::get_offset_of_servicePoint_33(),
	HttpWebRequest_t1951404513::get_offset_of_timeout_34(),
	HttpWebRequest_t1951404513::get_offset_of_writeStream_35(),
	HttpWebRequest_t1951404513::get_offset_of_webResponse_36(),
	HttpWebRequest_t1951404513::get_offset_of_asyncWrite_37(),
	HttpWebRequest_t1951404513::get_offset_of_asyncRead_38(),
	HttpWebRequest_t1951404513::get_offset_of_abortHandler_39(),
	HttpWebRequest_t1951404513::get_offset_of_aborted_40(),
	HttpWebRequest_t1951404513::get_offset_of_redirects_41(),
	HttpWebRequest_t1951404513::get_offset_of_expectContinue_42(),
	HttpWebRequest_t1951404513::get_offset_of_authCompleted_43(),
	HttpWebRequest_t1951404513::get_offset_of_bodyBuffer_44(),
	HttpWebRequest_t1951404513::get_offset_of_bodyBufferLength_45(),
	HttpWebRequest_t1951404513::get_offset_of_getResponseCalled_46(),
	HttpWebRequest_t1951404513::get_offset_of_saved_exc_47(),
	HttpWebRequest_t1951404513::get_offset_of_locker_48(),
	HttpWebRequest_t1951404513::get_offset_of_is_ntlm_auth_49(),
	HttpWebRequest_t1951404513::get_offset_of_finished_reading_50(),
	HttpWebRequest_t1951404513::get_offset_of_WebConnection_51(),
	HttpWebRequest_t1951404513::get_offset_of_auto_decomp_52(),
	HttpWebRequest_t1951404513_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_53(),
	HttpWebRequest_t1951404513::get_offset_of_readWriteTimeout_54(),
	HttpWebRequest_t1951404513::get_offset_of_unsafe_auth_blah_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (HttpWebResponse_t2828383075), -1, sizeof(HttpWebResponse_t2828383075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1771[14] = 
{
	HttpWebResponse_t2828383075::get_offset_of_uri_1(),
	HttpWebResponse_t2828383075::get_offset_of_webHeaders_2(),
	HttpWebResponse_t2828383075::get_offset_of_cookieCollection_3(),
	HttpWebResponse_t2828383075::get_offset_of_method_4(),
	HttpWebResponse_t2828383075::get_offset_of_version_5(),
	HttpWebResponse_t2828383075::get_offset_of_statusCode_6(),
	HttpWebResponse_t2828383075::get_offset_of_statusDescription_7(),
	HttpWebResponse_t2828383075::get_offset_of_contentLength_8(),
	HttpWebResponse_t2828383075::get_offset_of_contentType_9(),
	HttpWebResponse_t2828383075::get_offset_of_cookie_container_10(),
	HttpWebResponse_t2828383075::get_offset_of_disposed_11(),
	HttpWebResponse_t2828383075::get_offset_of_stream_12(),
	HttpWebResponse_t2828383075::get_offset_of_cookieExpiresFormats_13(),
	HttpWebResponse_t2828383075_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (CookieParser_t1405985527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[3] = 
{
	CookieParser_t1405985527::get_offset_of_header_0(),
	CookieParser_t1405985527::get_offset_of_pos_1(),
	CookieParser_t1405985527::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (IPAddress_t1399971723), -1, sizeof(IPAddress_t1399971723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1778[11] = 
{
	IPAddress_t1399971723::get_offset_of_m_Address_0(),
	IPAddress_t1399971723::get_offset_of_m_Family_1(),
	IPAddress_t1399971723::get_offset_of_m_Numbers_2(),
	IPAddress_t1399971723::get_offset_of_m_ScopeId_3(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Any_4(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t1399971723_StaticFields::get_offset_of_None_7(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6None_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (IPEndPoint_t2615413766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[2] = 
{
	IPEndPoint_t2615413766::get_offset_of_address_0(),
	IPEndPoint_t2615413766::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (IPHostEntry_t994738509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[3] = 
{
	IPHostEntry_t994738509::get_offset_of_addressList_0(),
	IPHostEntry_t994738509::get_offset_of_aliases_1(),
	IPHostEntry_t994738509::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (IPv6Address_t2596635879), -1, sizeof(IPv6Address_t2596635879_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1781[5] = 
{
	IPv6Address_t2596635879::get_offset_of_address_0(),
	IPv6Address_t2596635879::get_offset_of_prefixLength_1(),
	IPv6Address_t2596635879::get_offset_of_scopeId_2(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (AlternateViewCollection_t2752355131), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (AlternateView_t1668264691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[1] = 
{
	AlternateView_t1668264691::get_offset_of_linkedResources_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (AttachmentBase_t3226558610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[4] = 
{
	AttachmentBase_t3226558610::get_offset_of_id_0(),
	AttachmentBase_t3226558610::get_offset_of_contentType_1(),
	AttachmentBase_t3226558610::get_offset_of_contentStream_2(),
	AttachmentBase_t3226558610::get_offset_of_transferEncoding_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (AttachmentCollection_t537487287), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (Attachment_t2609929423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1788[2] = 
{
	Attachment_t2609929423::get_offset_of_contentDisposition_4(),
	Attachment_t2609929423::get_offset_of_nameEncoding_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (DeliveryNotificationOptions_t653946403)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1789[6] = 
{
	DeliveryNotificationOptions_t653946403::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (LinkedResourceCollection_t3361889291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (LinkedResource_t830466835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (MailAddressCollection_t3502002665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (MailAddress_t1980919589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[2] = 
{
	MailAddress_t1980919589::get_offset_of_address_0(),
	MailAddress_t1980919589::get_offset_of_displayName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (MailMessage_t460962712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[17] = 
{
	MailMessage_t460962712::get_offset_of_alternateViews_0(),
	MailMessage_t460962712::get_offset_of_attachments_1(),
	MailMessage_t460962712::get_offset_of_bcc_2(),
	MailMessage_t460962712::get_offset_of_replyTo_3(),
	MailMessage_t460962712::get_offset_of_body_4(),
	MailMessage_t460962712::get_offset_of_priority_5(),
	MailMessage_t460962712::get_offset_of_sender_6(),
	MailMessage_t460962712::get_offset_of_deliveryNotificationOptions_7(),
	MailMessage_t460962712::get_offset_of_cc_8(),
	MailMessage_t460962712::get_offset_of_from_9(),
	MailMessage_t460962712::get_offset_of_headers_10(),
	MailMessage_t460962712::get_offset_of_to_11(),
	MailMessage_t460962712::get_offset_of_subject_12(),
	MailMessage_t460962712::get_offset_of_subjectEncoding_13(),
	MailMessage_t460962712::get_offset_of_bodyEncoding_14(),
	MailMessage_t460962712::get_offset_of_headersEncoding_15(),
	MailMessage_t460962712::get_offset_of_isHtml_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (MailPriority_t2524119153)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1795[4] = 
{
	MailPriority_t2524119153::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (SmtpClient_t2719616745), -1, sizeof(SmtpClient_t2719616745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1796[24] = 
{
	SmtpClient_t2719616745::get_offset_of_host_0(),
	SmtpClient_t2719616745::get_offset_of_port_1(),
	SmtpClient_t2719616745::get_offset_of_timeout_2(),
	SmtpClient_t2719616745::get_offset_of_credentials_3(),
	SmtpClient_t2719616745::get_offset_of_pickupDirectoryLocation_4(),
	SmtpClient_t2719616745::get_offset_of_deliveryMethod_5(),
	SmtpClient_t2719616745::get_offset_of_enableSsl_6(),
	SmtpClient_t2719616745::get_offset_of_clientCertificates_7(),
	SmtpClient_t2719616745::get_offset_of_client_8(),
	SmtpClient_t2719616745::get_offset_of_stream_9(),
	SmtpClient_t2719616745::get_offset_of_writer_10(),
	SmtpClient_t2719616745::get_offset_of_reader_11(),
	SmtpClient_t2719616745::get_offset_of_boundaryIndex_12(),
	SmtpClient_t2719616745::get_offset_of_defaultFrom_13(),
	SmtpClient_t2719616745::get_offset_of_messageInProcess_14(),
	SmtpClient_t2719616745::get_offset_of_worker_15(),
	SmtpClient_t2719616745::get_offset_of_user_async_state_16(),
	SmtpClient_t2719616745::get_offset_of_authMechs_17(),
	SmtpClient_t2719616745::get_offset_of_mutex_18(),
	SmtpClient_t2719616745::get_offset_of_callback_19(),
	SmtpClient_t2719616745::get_offset_of_SendCompleted_20(),
	SmtpClient_t2719616745::get_offset_of_U3CTargetNameU3Ek__BackingField_21(),
	SmtpClient_t2719616745_StaticFields::get_offset_of_U3CU3Ef__amU24cache16_22(),
	SmtpClient_t2719616745_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (AuthMechs_t3916064079)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1797[8] = 
{
	AuthMechs_t3916064079::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (CancellationException_t544132923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (SmtpResponse_t616735068)+ sizeof (Il2CppObject), sizeof(SmtpResponse_t616735068_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1799[2] = 
{
	SmtpResponse_t616735068::get_offset_of_StatusCode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SmtpResponse_t616735068::get_offset_of_Description_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
