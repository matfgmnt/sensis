﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlWhitespace2557770518.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_XmlTextWriter2527250655.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo3709371029.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil2068578019.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState3530111136.h"
#include "System_Xml_System_Xml_XmlStreamReader2725532304.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader3963211903.h"
#include "System_Xml_System_Xml_XmlInputStream2650744719.h"
#include "System_Xml_System_Xml_XmlParserInput2366782760.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInputS25800784.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet2758097827.h"
#include "System_Xml_Mono_Xml_Schema_XsdOrdering3741887935.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType1096449895.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType3359210273.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic2904699188.h"
#include "System_Xml_Mono_Xml_Schema_XsdString263933896.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString2132216291.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2902215462.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage3897851897.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken547135877.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens1753890866.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1409945702.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName414304927.h"
#include "System_Xml_Mono_Xml_Schema_XsdID1067193160.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef1377311049.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs763119546.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity2664305022.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities1103053540.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation720379093.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal2932251550.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger1330502157.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong4179235589.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt1488443894.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort1778530041.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte1120972221.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger3587933853.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong137890294.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt2956447959.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort3693774826.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte3216355454.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger1896481288.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger409343009.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger399444136.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat386143221.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble2510112208.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary1094704629.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary3496718151.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName930779123.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean4126353587.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI2527983239.h"
#include "System_Xml_Mono_Xml_Schema_XmlSchemaUri1295878664.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration1605638443.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration2797717973.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration1764328599.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime1344468684.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate919459387.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime4165680512.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth1363357165.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay3859978294.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear3810382607.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth4076673358.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay1914244270.h"
#include "System_Xml_System_Xml_Schema_QNameValueType2109511131.h"
#include "System_Xml_System_Xml_Schema_StringValueType652312500.h"
#include "System_Xml_System_Xml_Schema_UriValueType1626089757.h"
#include "System_Xml_System_Xml_Schema_StringArrayValueType1731700877.h"
#include "System_Xml_System_Xml_Schema_ValidationEventArgs1577905814.h"
#include "System_Xml_System_Xml_Schema_XmlSchema880472818.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAll1805755215.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated2082486936.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotation2400301303.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAny3277730824.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnyAttribute530453212.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAppInfo2033489551.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute4015859774.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttributeGrou491156493.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttributeGrou825996660.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaChoice654568461.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollection3518500204.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCollectionEn1538181312.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationS2971213394.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConte2065934415.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConten655218998.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexConte1722137421.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaComplexType4086789226.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContent3733871217.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentModel907989596.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentProcess74226324.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaContentType2874429441.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype1195946242.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMe3165007540.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDocumentatio3832803992.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement2433337156.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaEnumerationF3989738874.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaException4082200141.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaExternal3943748629.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet614309579.h"




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (XmlWhitespace_t2557770518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (XmlWriter_t1048088568), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (XmlTextWriter_t2527250655), -1, sizeof(XmlTextWriter_t2527250655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1202[35] = 
{
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_unmarked_utf8encoding_0(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_text_chars_1(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_attr_chars_2(),
	XmlTextWriter_t2527250655::get_offset_of_base_stream_3(),
	XmlTextWriter_t2527250655::get_offset_of_source_4(),
	XmlTextWriter_t2527250655::get_offset_of_writer_5(),
	XmlTextWriter_t2527250655::get_offset_of_preserver_6(),
	XmlTextWriter_t2527250655::get_offset_of_preserved_name_7(),
	XmlTextWriter_t2527250655::get_offset_of_is_preserved_xmlns_8(),
	XmlTextWriter_t2527250655::get_offset_of_allow_doc_fragment_9(),
	XmlTextWriter_t2527250655::get_offset_of_close_output_stream_10(),
	XmlTextWriter_t2527250655::get_offset_of_ignore_encoding_11(),
	XmlTextWriter_t2527250655::get_offset_of_namespaces_12(),
	XmlTextWriter_t2527250655::get_offset_of_xmldecl_state_13(),
	XmlTextWriter_t2527250655::get_offset_of_check_character_validity_14(),
	XmlTextWriter_t2527250655::get_offset_of_newline_handling_15(),
	XmlTextWriter_t2527250655::get_offset_of_is_document_entity_16(),
	XmlTextWriter_t2527250655::get_offset_of_state_17(),
	XmlTextWriter_t2527250655::get_offset_of_node_state_18(),
	XmlTextWriter_t2527250655::get_offset_of_nsmanager_19(),
	XmlTextWriter_t2527250655::get_offset_of_open_count_20(),
	XmlTextWriter_t2527250655::get_offset_of_elements_21(),
	XmlTextWriter_t2527250655::get_offset_of_new_local_namespaces_22(),
	XmlTextWriter_t2527250655::get_offset_of_explicit_nsdecls_23(),
	XmlTextWriter_t2527250655::get_offset_of_namespace_handling_24(),
	XmlTextWriter_t2527250655::get_offset_of_indent_25(),
	XmlTextWriter_t2527250655::get_offset_of_indent_count_26(),
	XmlTextWriter_t2527250655::get_offset_of_indent_char_27(),
	XmlTextWriter_t2527250655::get_offset_of_indent_string_28(),
	XmlTextWriter_t2527250655::get_offset_of_newline_29(),
	XmlTextWriter_t2527250655::get_offset_of_indent_attributes_30(),
	XmlTextWriter_t2527250655::get_offset_of_quote_char_31(),
	XmlTextWriter_t2527250655::get_offset_of_v2_32(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map3A_33(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map3B_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (XmlNodeInfo_t3709371029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1203[7] = 
{
	XmlNodeInfo_t3709371029::get_offset_of_Prefix_0(),
	XmlNodeInfo_t3709371029::get_offset_of_LocalName_1(),
	XmlNodeInfo_t3709371029::get_offset_of_NS_2(),
	XmlNodeInfo_t3709371029::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t3709371029::get_offset_of_HasElements_4(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (StringUtil_t2068578019), -1, sizeof(StringUtil_t2068578019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1204[2] = 
{
	StringUtil_t2068578019_StaticFields::get_offset_of_cul_0(),
	StringUtil_t2068578019_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (XmlDeclState_t3530111136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1205[5] = 
{
	XmlDeclState_t3530111136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (XmlStreamReader_t2725532304), -1, sizeof(XmlStreamReader_t2725532304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1206[2] = 
{
	XmlStreamReader_t2725532304::get_offset_of_input_13(),
	XmlStreamReader_t2725532304_StaticFields::get_offset_of_invalidDataException_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (NonBlockingStreamReader_t3963211903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1207[11] = 
{
	NonBlockingStreamReader_t3963211903::get_offset_of_input_buffer_2(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_buffer_3(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_count_4(),
	NonBlockingStreamReader_t3963211903::get_offset_of_pos_5(),
	NonBlockingStreamReader_t3963211903::get_offset_of_buffer_size_6(),
	NonBlockingStreamReader_t3963211903::get_offset_of_encoding_7(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoder_8(),
	NonBlockingStreamReader_t3963211903::get_offset_of_base_stream_9(),
	NonBlockingStreamReader_t3963211903::get_offset_of_mayBlock_10(),
	NonBlockingStreamReader_t3963211903::get_offset_of_line_builder_11(),
	NonBlockingStreamReader_t3963211903::get_offset_of_foundCR_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { sizeof (XmlInputStream_t2650744719), -1, sizeof(XmlInputStream_t2650744719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1208[7] = 
{
	XmlInputStream_t2650744719_StaticFields::get_offset_of_StrictUTF8_2(),
	XmlInputStream_t2650744719::get_offset_of_enc_3(),
	XmlInputStream_t2650744719::get_offset_of_stream_4(),
	XmlInputStream_t2650744719::get_offset_of_buffer_5(),
	XmlInputStream_t2650744719::get_offset_of_bufLength_6(),
	XmlInputStream_t2650744719::get_offset_of_bufPos_7(),
	XmlInputStream_t2650744719_StaticFields::get_offset_of_encodingException_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (XmlParserInput_t2366782760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1209[5] = 
{
	XmlParserInput_t2366782760::get_offset_of_sourceStack_0(),
	XmlParserInput_t2366782760::get_offset_of_source_1(),
	XmlParserInput_t2366782760::get_offset_of_has_peek_2(),
	XmlParserInput_t2366782760::get_offset_of_peek_char_3(),
	XmlParserInput_t2366782760::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (XmlParserInputSource_t25800784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1210[6] = 
{
	XmlParserInputSource_t25800784::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t25800784::get_offset_of_reader_1(),
	XmlParserInputSource_t25800784::get_offset_of_state_2(),
	XmlParserInputSource_t25800784::get_offset_of_isPE_3(),
	XmlParserInputSource_t25800784::get_offset_of_line_4(),
	XmlParserInputSource_t25800784::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (XsdWhitespaceFacet_t2758097827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1212[4] = 
{
	XsdWhitespaceFacet_t2758097827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (XsdOrdering_t3741887935)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1213[5] = 
{
	XsdOrdering_t3741887935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (XsdAnySimpleType_t1096449895), -1, sizeof(XsdAnySimpleType_t1096449895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1214[6] = 
{
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (XdtAnyAtomicType_t3359210273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (XdtUntypedAtomic_t2904699188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (XsdString_t263933896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (XsdNormalizedString_t2132216291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (XsdToken_t2902215462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (XsdLanguage_t3897851897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (XsdNMToken_t547135877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (XsdNMTokens_t1753890866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (XsdName_t1409945702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (XsdNCName_t414304927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (XsdID_t1067193160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (XsdIDRef_t1377311049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (XsdIDRefs_t763119546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (XsdEntity_t2664305022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (XsdEntities_t1103053540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (XsdNotation_t720379093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (XsdDecimal_t2932251550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (XsdInteger_t1330502157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (XsdLong_t4179235589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (XsdInt_t1488443894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (XsdShort_t1778530041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (XsdByte_t1120972221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (XsdNonNegativeInteger_t3587933853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (XsdUnsignedLong_t137890294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (XsdUnsignedInt_t2956447959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (XsdUnsignedShort_t3693774826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (XsdUnsignedByte_t3216355454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (XsdPositiveInteger_t1896481288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (XsdNonPositiveInteger_t409343009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (XsdNegativeInteger_t399444136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (XsdFloat_t386143221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (XsdDouble_t2510112208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (XsdBase64Binary_t1094704629), -1, sizeof(XsdBase64Binary_t1094704629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1247[2] = 
{
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (XsdHexBinary_t3496718151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (XsdQName_t930779123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (XsdBoolean_t4126353587), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (XsdAnyURI_t2527983239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (XmlSchemaUri_t1295878664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1252[1] = 
{
	XmlSchemaUri_t1295878664::get_offset_of_value_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (XsdDuration_t1605638443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (XdtDayTimeDuration_t2797717973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (XdtYearMonthDuration_t1764328599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (XsdDateTime_t1344468684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (XsdDate_t919459387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (XsdTime_t4165680512), -1, sizeof(XsdTime_t4165680512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1258[1] = 
{
	XsdTime_t4165680512_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (XsdGYearMonth_t1363357165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (XsdGMonthDay_t3859978294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (XsdGYear_t3810382607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (XsdGMonth_t4076673358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { sizeof (XsdGDay_t1914244270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (QNameValueType_t2109511131)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1265[1] = 
{
	QNameValueType_t2109511131::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (StringValueType_t652312500)+ sizeof (Il2CppObject), sizeof(StringValueType_t652312500_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1266[1] = 
{
	StringValueType_t652312500::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (UriValueType_t1626089757)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1267[1] = 
{
	UriValueType_t1626089757::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (StringArrayValueType_t1731700877)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1268[1] = 
{
	StringArrayValueType_t1731700877::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (ValidationEventArgs_t1577905814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1269[3] = 
{
	ValidationEventArgs_t1577905814::get_offset_of_exception_1(),
	ValidationEventArgs_t1577905814::get_offset_of_message_2(),
	ValidationEventArgs_t1577905814::get_offset_of_severity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (XmlSchema_t880472818), -1, sizeof(XmlSchema_t880472818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1270[20] = 
{
	XmlSchema_t880472818::get_offset_of_attributeFormDefault_13(),
	XmlSchema_t880472818::get_offset_of_attributeGroups_14(),
	XmlSchema_t880472818::get_offset_of_attributes_15(),
	XmlSchema_t880472818::get_offset_of_blockDefault_16(),
	XmlSchema_t880472818::get_offset_of_elementFormDefault_17(),
	XmlSchema_t880472818::get_offset_of_elements_18(),
	XmlSchema_t880472818::get_offset_of_finalDefault_19(),
	XmlSchema_t880472818::get_offset_of_groups_20(),
	XmlSchema_t880472818::get_offset_of_id_21(),
	XmlSchema_t880472818::get_offset_of_includes_22(),
	XmlSchema_t880472818::get_offset_of_items_23(),
	XmlSchema_t880472818::get_offset_of_notations_24(),
	XmlSchema_t880472818::get_offset_of_schemaTypes_25(),
	XmlSchema_t880472818::get_offset_of_targetNamespace_26(),
	XmlSchema_t880472818::get_offset_of_version_27(),
	XmlSchema_t880472818::get_offset_of_schemas_28(),
	XmlSchema_t880472818::get_offset_of_nameTable_29(),
	XmlSchema_t880472818::get_offset_of_missedSubComponents_30(),
	XmlSchema_t880472818::get_offset_of_compilationItems_31(),
	XmlSchema_t880472818_StaticFields::get_offset_of_U3CU3Ef__switchU24map41_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (XmlSchemaAll_t1805755215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1271[3] = 
{
	XmlSchemaAll_t1805755215::get_offset_of_schema_28(),
	XmlSchemaAll_t1805755215::get_offset_of_items_29(),
	XmlSchemaAll_t1805755215::get_offset_of_emptiable_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (XmlSchemaAnnotated_t2082486936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1272[3] = 
{
	XmlSchemaAnnotated_t2082486936::get_offset_of_annotation_13(),
	XmlSchemaAnnotated_t2082486936::get_offset_of_id_14(),
	XmlSchemaAnnotated_t2082486936::get_offset_of_unhandledAttributes_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (XmlSchemaAnnotation_t2400301303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1273[2] = 
{
	XmlSchemaAnnotation_t2400301303::get_offset_of_id_13(),
	XmlSchemaAnnotation_t2400301303::get_offset_of_items_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { sizeof (XmlSchemaAny_t3277730824), -1, sizeof(XmlSchemaAny_t3277730824_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1274[4] = 
{
	XmlSchemaAny_t3277730824_StaticFields::get_offset_of_anyTypeContent_27(),
	XmlSchemaAny_t3277730824::get_offset_of_nameSpace_28(),
	XmlSchemaAny_t3277730824::get_offset_of_processing_29(),
	XmlSchemaAny_t3277730824::get_offset_of_wildcard_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (XmlSchemaAnyAttribute_t530453212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1275[3] = 
{
	XmlSchemaAnyAttribute_t530453212::get_offset_of_nameSpace_16(),
	XmlSchemaAnyAttribute_t530453212::get_offset_of_processing_17(),
	XmlSchemaAnyAttribute_t530453212::get_offset_of_wildcard_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (XmlSchemaAppInfo_t2033489551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1276[2] = 
{
	XmlSchemaAppInfo_t2033489551::get_offset_of_markup_13(),
	XmlSchemaAppInfo_t2033489551::get_offset_of_source_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { sizeof (XmlSchemaAttribute_t4015859774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1277[17] = 
{
	XmlSchemaAttribute_t4015859774::get_offset_of_attributeType_16(),
	XmlSchemaAttribute_t4015859774::get_offset_of_attributeSchemaType_17(),
	XmlSchemaAttribute_t4015859774::get_offset_of_defaultValue_18(),
	XmlSchemaAttribute_t4015859774::get_offset_of_fixedValue_19(),
	XmlSchemaAttribute_t4015859774::get_offset_of_validatedDefaultValue_20(),
	XmlSchemaAttribute_t4015859774::get_offset_of_validatedFixedValue_21(),
	XmlSchemaAttribute_t4015859774::get_offset_of_validatedFixedTypedValue_22(),
	XmlSchemaAttribute_t4015859774::get_offset_of_form_23(),
	XmlSchemaAttribute_t4015859774::get_offset_of_name_24(),
	XmlSchemaAttribute_t4015859774::get_offset_of_targetNamespace_25(),
	XmlSchemaAttribute_t4015859774::get_offset_of_qualifiedName_26(),
	XmlSchemaAttribute_t4015859774::get_offset_of_refName_27(),
	XmlSchemaAttribute_t4015859774::get_offset_of_schemaType_28(),
	XmlSchemaAttribute_t4015859774::get_offset_of_schemaTypeName_29(),
	XmlSchemaAttribute_t4015859774::get_offset_of_use_30(),
	XmlSchemaAttribute_t4015859774::get_offset_of_validatedUse_31(),
	XmlSchemaAttribute_t4015859774::get_offset_of_referencedAttribute_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (XmlSchemaAttributeGroup_t491156493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1278[8] = 
{
	XmlSchemaAttributeGroup_t491156493::get_offset_of_anyAttribute_16(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_attributes_17(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_name_18(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_redefined_19(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_qualifiedName_20(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_attributeUses_21(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_anyAttributeUse_22(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_AttributeGroupRecursionCheck_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (XmlSchemaAttributeGroupRef_t825996660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1279[1] = 
{
	XmlSchemaAttributeGroupRef_t825996660::get_offset_of_refName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (XmlSchemaChoice_t654568461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1280[2] = 
{
	XmlSchemaChoice_t654568461::get_offset_of_items_28(),
	XmlSchemaChoice_t654568461::get_offset_of_minEffectiveTotalRange_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (XmlSchemaCollection_t3518500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1281[2] = 
{
	XmlSchemaCollection_t3518500204::get_offset_of_schemaSet_0(),
	XmlSchemaCollection_t3518500204::get_offset_of_ValidationEventHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (XmlSchemaCollectionEnumerator_t1538181312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1282[1] = 
{
	XmlSchemaCollectionEnumerator_t1538181312::get_offset_of_xenum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (XmlSchemaCompilationSettings_t2971213394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1283[1] = 
{
	XmlSchemaCompilationSettings_t2971213394::get_offset_of_enable_upa_check_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (XmlSchemaComplexContent_t2065934415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1284[2] = 
{
	XmlSchemaComplexContent_t2065934415::get_offset_of_content_16(),
	XmlSchemaComplexContent_t2065934415::get_offset_of_isMixed_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (XmlSchemaComplexContentExtension_t655218998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1285[4] = 
{
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_any_17(),
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_attributes_18(),
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (XmlSchemaComplexContentRestriction_t1722137421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1286[4] = 
{
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_any_17(),
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_attributes_18(),
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (XmlSchemaComplexType_t4086789226), -1, sizeof(XmlSchemaComplexType_t4086789226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1287[17] = 
{
	XmlSchemaComplexType_t4086789226::get_offset_of_anyAttribute_28(),
	XmlSchemaComplexType_t4086789226::get_offset_of_attributes_29(),
	XmlSchemaComplexType_t4086789226::get_offset_of_attributeUses_30(),
	XmlSchemaComplexType_t4086789226::get_offset_of_attributeWildcard_31(),
	XmlSchemaComplexType_t4086789226::get_offset_of_block_32(),
	XmlSchemaComplexType_t4086789226::get_offset_of_blockResolved_33(),
	XmlSchemaComplexType_t4086789226::get_offset_of_contentModel_34(),
	XmlSchemaComplexType_t4086789226::get_offset_of_validatableParticle_35(),
	XmlSchemaComplexType_t4086789226::get_offset_of_contentTypeParticle_36(),
	XmlSchemaComplexType_t4086789226::get_offset_of_isAbstract_37(),
	XmlSchemaComplexType_t4086789226::get_offset_of_isMixed_38(),
	XmlSchemaComplexType_t4086789226::get_offset_of_particle_39(),
	XmlSchemaComplexType_t4086789226::get_offset_of_resolvedContentType_40(),
	XmlSchemaComplexType_t4086789226::get_offset_of_ValidatedIsAbstract_41(),
	XmlSchemaComplexType_t4086789226_StaticFields::get_offset_of_anyType_42(),
	XmlSchemaComplexType_t4086789226_StaticFields::get_offset_of_AnyTypeName_43(),
	XmlSchemaComplexType_t4086789226::get_offset_of_CollectProcessId_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (XmlSchemaContent_t3733871217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1288[1] = 
{
	XmlSchemaContent_t3733871217::get_offset_of_actualBaseSchemaType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (XmlSchemaContentModel_t907989596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (XmlSchemaContentProcessing_t74226324)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1290[5] = 
{
	XmlSchemaContentProcessing_t74226324::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (XmlSchemaContentType_t2874429441)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1291[5] = 
{
	XmlSchemaContentType_t2874429441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (XmlSchemaDatatype_t1195946242), -1, sizeof(XmlSchemaDatatype_t1195946242_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1292[55] = 
{
	XmlSchemaDatatype_t1195946242::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t1195946242::get_offset_of_sb_2(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map3E_52(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map3F_53(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map40_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (XmlSchemaDerivationMethod_t3165007540)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1293[9] = 
{
	XmlSchemaDerivationMethod_t3165007540::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (XmlSchemaDocumentation_t3832803992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1294[3] = 
{
	XmlSchemaDocumentation_t3832803992::get_offset_of_language_13(),
	XmlSchemaDocumentation_t3832803992::get_offset_of_markup_14(),
	XmlSchemaDocumentation_t3832803992::get_offset_of_source_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (XmlSchemaElement_t2433337156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1295[27] = 
{
	XmlSchemaElement_t2433337156::get_offset_of_block_27(),
	XmlSchemaElement_t2433337156::get_offset_of_constraints_28(),
	XmlSchemaElement_t2433337156::get_offset_of_defaultValue_29(),
	XmlSchemaElement_t2433337156::get_offset_of_elementType_30(),
	XmlSchemaElement_t2433337156::get_offset_of_elementSchemaType_31(),
	XmlSchemaElement_t2433337156::get_offset_of_final_32(),
	XmlSchemaElement_t2433337156::get_offset_of_fixedValue_33(),
	XmlSchemaElement_t2433337156::get_offset_of_form_34(),
	XmlSchemaElement_t2433337156::get_offset_of_isAbstract_35(),
	XmlSchemaElement_t2433337156::get_offset_of_isNillable_36(),
	XmlSchemaElement_t2433337156::get_offset_of_name_37(),
	XmlSchemaElement_t2433337156::get_offset_of_refName_38(),
	XmlSchemaElement_t2433337156::get_offset_of_schemaType_39(),
	XmlSchemaElement_t2433337156::get_offset_of_schemaTypeName_40(),
	XmlSchemaElement_t2433337156::get_offset_of_substitutionGroup_41(),
	XmlSchemaElement_t2433337156::get_offset_of_schema_42(),
	XmlSchemaElement_t2433337156::get_offset_of_parentIsSchema_43(),
	XmlSchemaElement_t2433337156::get_offset_of_qName_44(),
	XmlSchemaElement_t2433337156::get_offset_of_blockResolved_45(),
	XmlSchemaElement_t2433337156::get_offset_of_finalResolved_46(),
	XmlSchemaElement_t2433337156::get_offset_of_referencedElement_47(),
	XmlSchemaElement_t2433337156::get_offset_of_substitutingElements_48(),
	XmlSchemaElement_t2433337156::get_offset_of_substitutionGroupElement_49(),
	XmlSchemaElement_t2433337156::get_offset_of_actualIsAbstract_50(),
	XmlSchemaElement_t2433337156::get_offset_of_actualIsNillable_51(),
	XmlSchemaElement_t2433337156::get_offset_of_validatedDefaultValue_52(),
	XmlSchemaElement_t2433337156::get_offset_of_validatedFixedValue_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (XmlSchemaEnumerationFacet_t3989738874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (XmlSchemaException_t4082200141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1297[5] = 
{
	XmlSchemaException_t4082200141::get_offset_of_hasLineInfo_11(),
	XmlSchemaException_t4082200141::get_offset_of_lineNumber_12(),
	XmlSchemaException_t4082200141::get_offset_of_linePosition_13(),
	XmlSchemaException_t4082200141::get_offset_of_sourceObj_14(),
	XmlSchemaException_t4082200141::get_offset_of_sourceUri_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (XmlSchemaExternal_t3943748629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1298[3] = 
{
	XmlSchemaExternal_t3943748629::get_offset_of_id_13(),
	XmlSchemaExternal_t3943748629::get_offset_of_schema_14(),
	XmlSchemaExternal_t3943748629::get_offset_of_location_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (XmlSchemaFacet_t614309579), -1, sizeof(XmlSchemaFacet_t614309579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1299[3] = 
{
	XmlSchemaFacet_t614309579_StaticFields::get_offset_of_AllFacets_16(),
	XmlSchemaFacet_t614309579::get_offset_of_isFixed_17(),
	XmlSchemaFacet_t614309579::get_offset_of_val_18(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
