﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_ObjectModel_Collection1210009445.h"


#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mail.AlternateViewCollection
struct  AlternateViewCollection_t2752355131  : public Collection_1_t1210009445
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
