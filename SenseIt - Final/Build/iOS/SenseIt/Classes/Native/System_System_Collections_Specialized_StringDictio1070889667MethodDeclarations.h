﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Collections.Specialized.StringDictionary
struct StringDictionary_t1070889667;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

// System.Void System.Collections.Specialized.StringDictionary::.ctor()
extern "C"  void StringDictionary__ctor_m270184480 (StringDictionary_t1070889667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.StringDictionary::get_Count()
extern "C"  int32_t StringDictionary_get_Count_m2592141200 (StringDictionary_t1070889667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Specialized.StringDictionary::get_Item(System.String)
extern "C"  String_t* StringDictionary_get_Item_m695063801 (StringDictionary_t1070889667 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.StringDictionary::set_Item(System.String,System.String)
extern "C"  void StringDictionary_set_Item_m3594495450 (StringDictionary_t1070889667 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.StringDictionary::Add(System.String,System.String)
extern "C"  void StringDictionary_Add_m4083181659 (StringDictionary_t1070889667 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.StringDictionary::GetEnumerator()
extern "C"  Il2CppObject * StringDictionary_GetEnumerator_m2202077700 (StringDictionary_t1070889667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.StringDictionary::Remove(System.String)
extern "C"  void StringDictionary_Remove_m959773668 (StringDictionary_t1070889667 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
