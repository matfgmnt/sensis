﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Diagnostics.DefaultTraceListener
struct DefaultTraceListener_t1568159610;
// System.String
struct String_t;

// System.Void System.Diagnostics.DefaultTraceListener::.ctor()
extern "C"  void DefaultTraceListener__ctor_m1814291763 (DefaultTraceListener_t1568159610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DefaultTraceListener::.cctor()
extern "C"  void DefaultTraceListener__cctor_m1196250802 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DefaultTraceListener::GetPrefix(System.String,System.String)
extern "C"  String_t* DefaultTraceListener_GetPrefix_m2663364656 (Il2CppObject * __this /* static, unused */, String_t* ___var0, String_t* ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DefaultTraceListener::set_AssertUiEnabled(System.Boolean)
extern "C"  void DefaultTraceListener_set_AssertUiEnabled_m3845605984 (DefaultTraceListener_t1568159610 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DefaultTraceListener::set_LogFileName(System.String)
extern "C"  void DefaultTraceListener_set_LogFileName_m3441897069 (DefaultTraceListener_t1568159610 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
