﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"


// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTarget>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m667827559(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2949329744 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTarget>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2858141687(__this, method) ((  void (*) (InternalEnumerator_1_t2949329744 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ImageTarget>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m210353767(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2949329744 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTarget>::Dispose()
#define InternalEnumerator_1_Dispose_m2229369004(__this, method) ((  void (*) (InternalEnumerator_1_t2949329744 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ImageTarget>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1100943523(__this, method) ((  bool (*) (InternalEnumerator_1_t2949329744 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.ImageTarget>::get_Current()
#define InternalEnumerator_1_get_Current_m3080392912(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2949329744 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
