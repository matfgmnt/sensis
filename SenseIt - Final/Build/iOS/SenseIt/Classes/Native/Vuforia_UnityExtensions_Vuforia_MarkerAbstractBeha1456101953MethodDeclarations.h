﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t1456101953;
// Vuforia.Marker
struct Marker_t1563082680;

// Vuforia.Marker Vuforia.MarkerAbstractBehaviour::get_Marker()
extern "C"  Il2CppObject * MarkerAbstractBehaviour_get_Marker_m1448736941 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::.ctor()
extern "C"  void MarkerAbstractBehaviour__ctor_m2689139661 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void MarkerAbstractBehaviour_InternalUnregisterTrackable_m1113807269 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::CorrectScaleImpl()
extern "C"  bool MarkerAbstractBehaviour_CorrectScaleImpl_m1400180015 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MarkerAbstractBehaviour::get_MarkerID()
extern "C"  int32_t MarkerAbstractBehaviour_get_MarkerID_m2385494369 (MarkerAbstractBehaviour_t1456101953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::InitializeMarker(Vuforia.Marker)
extern "C"  void MarkerAbstractBehaviour_InitializeMarker_m195364939 (MarkerAbstractBehaviour_t1456101953 * __this, Il2CppObject * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
