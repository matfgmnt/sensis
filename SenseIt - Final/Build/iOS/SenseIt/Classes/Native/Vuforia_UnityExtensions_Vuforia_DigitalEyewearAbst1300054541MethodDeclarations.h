﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.DigitalEyewearAbstractBehaviour/SerializableViewerParameters
struct SerializableViewerParameters_t1300054541;

// System.Void Vuforia.DigitalEyewearAbstractBehaviour/SerializableViewerParameters::.ctor()
extern "C"  void SerializableViewerParameters__ctor_m2941564211 (SerializableViewerParameters_t1300054541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
