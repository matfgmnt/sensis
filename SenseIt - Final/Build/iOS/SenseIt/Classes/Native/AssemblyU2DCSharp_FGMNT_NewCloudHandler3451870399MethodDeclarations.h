﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_InitStat4409649.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Updat1473252352.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe1958726506.h"

// FGMNT.NewCloudHandler
struct NewCloudHandler_t3451870399;

// System.Void FGMNT.NewCloudHandler::.ctor()
extern "C"  void NewCloudHandler__ctor_m3910659858 (NewCloudHandler_t3451870399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewCloudHandler::Awake()
extern "C"  void NewCloudHandler_Awake_m769392995 (NewCloudHandler_t3451870399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewCloudHandler::Start()
extern "C"  void NewCloudHandler_Start_m914698782 (NewCloudHandler_t3451870399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewCloudHandler::OnInitialized()
extern "C"  void NewCloudHandler_OnInitialized_m2537922067 (NewCloudHandler_t3451870399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewCloudHandler::OnInitError(Vuforia.TargetFinder/InitState)
extern "C"  void NewCloudHandler_OnInitError_m1240728066 (NewCloudHandler_t3451870399 * __this, int32_t ___initError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewCloudHandler::OnUpdateError(Vuforia.TargetFinder/UpdateState)
extern "C"  void NewCloudHandler_OnUpdateError_m2302571296 (NewCloudHandler_t3451870399 * __this, int32_t ___updateError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewCloudHandler::OnStateChanged(System.Boolean)
extern "C"  void NewCloudHandler_OnStateChanged_m2133395345 (NewCloudHandler_t3451870399 * __this, bool ___scanning0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.NewCloudHandler::OnNewSearchResult(Vuforia.TargetFinder/TargetSearchResult)
extern "C"  void NewCloudHandler_OnNewSearchResult_m2577809582 (NewCloudHandler_t3451870399 * __this, TargetSearchResult_t1958726506  ___targetSearchResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
