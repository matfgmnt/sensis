﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// FGMNT.ApplicationManager
struct ApplicationManager_t2067665347;
// FGMNT.LocationManager
struct LocationManager_t1546885680;
// FGMNT.DatabaseEntry
struct DatabaseEntry_t2505461191;
// System.String
struct String_t;
// System.Net.Mail.MailAddress
struct MailAddress_t1980919589;
// System.Net.ICredentialsByHost
struct ICredentialsByHost_t1409008158;
// System.Net.Mail.SmtpClient
struct SmtpClient_t2719616745;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.FeedbackManager
struct  FeedbackManager_t2969052828  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean FGMNT.FeedbackManager::returned
	bool ___returned_2;
	// UnityEngine.GameObject FGMNT.FeedbackManager::waitingScreen
	GameObject_t1756533147 * ___waitingScreen_3;
	// UnityEngine.UI.Text FGMNT.FeedbackManager::errorMessage
	Text_t356221433 * ___errorMessage_4;
	// System.Boolean FGMNT.FeedbackManager::screenCapture
	bool ___screenCapture_7;
	// UnityEngine.Texture2D FGMNT.FeedbackManager::tex
	Texture2D_t3542995729 * ___tex_8;
	// System.Boolean FGMNT.FeedbackManager::source
	bool ___source_9;
	// FGMNT.ApplicationManager FGMNT.FeedbackManager::appManager
	ApplicationManager_t2067665347 * ___appManager_10;
	// FGMNT.LocationManager FGMNT.FeedbackManager::gps
	LocationManager_t1546885680 * ___gps_11;
	// FGMNT.DatabaseEntry FGMNT.FeedbackManager::business
	DatabaseEntry_t2505461191 * ___business_12;
	// System.String FGMNT.FeedbackManager::function
	String_t* ___function_13;
	// System.Net.Mail.MailAddress FGMNT.FeedbackManager::sender
	MailAddress_t1980919589 * ___sender_14;
	// System.String FGMNT.FeedbackManager::recipient
	String_t* ___recipient_15;
	// System.Net.ICredentialsByHost FGMNT.FeedbackManager::credentials
	Il2CppObject * ___credentials_16;
	// System.Net.Mail.SmtpClient FGMNT.FeedbackManager::smtpClient
	SmtpClient_t2719616745 * ___smtpClient_17;
	// System.Int32 FGMNT.FeedbackManager::port
	int32_t ___port_18;
	// System.String FGMNT.FeedbackManager::path
	String_t* ___path_19;

public:
	inline static int32_t get_offset_of_returned_2() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___returned_2)); }
	inline bool get_returned_2() const { return ___returned_2; }
	inline bool* get_address_of_returned_2() { return &___returned_2; }
	inline void set_returned_2(bool value)
	{
		___returned_2 = value;
	}

	inline static int32_t get_offset_of_waitingScreen_3() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___waitingScreen_3)); }
	inline GameObject_t1756533147 * get_waitingScreen_3() const { return ___waitingScreen_3; }
	inline GameObject_t1756533147 ** get_address_of_waitingScreen_3() { return &___waitingScreen_3; }
	inline void set_waitingScreen_3(GameObject_t1756533147 * value)
	{
		___waitingScreen_3 = value;
		Il2CppCodeGenWriteBarrier(&___waitingScreen_3, value);
	}

	inline static int32_t get_offset_of_errorMessage_4() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___errorMessage_4)); }
	inline Text_t356221433 * get_errorMessage_4() const { return ___errorMessage_4; }
	inline Text_t356221433 ** get_address_of_errorMessage_4() { return &___errorMessage_4; }
	inline void set_errorMessage_4(Text_t356221433 * value)
	{
		___errorMessage_4 = value;
		Il2CppCodeGenWriteBarrier(&___errorMessage_4, value);
	}

	inline static int32_t get_offset_of_screenCapture_7() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___screenCapture_7)); }
	inline bool get_screenCapture_7() const { return ___screenCapture_7; }
	inline bool* get_address_of_screenCapture_7() { return &___screenCapture_7; }
	inline void set_screenCapture_7(bool value)
	{
		___screenCapture_7 = value;
	}

	inline static int32_t get_offset_of_tex_8() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___tex_8)); }
	inline Texture2D_t3542995729 * get_tex_8() const { return ___tex_8; }
	inline Texture2D_t3542995729 ** get_address_of_tex_8() { return &___tex_8; }
	inline void set_tex_8(Texture2D_t3542995729 * value)
	{
		___tex_8 = value;
		Il2CppCodeGenWriteBarrier(&___tex_8, value);
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___source_9)); }
	inline bool get_source_9() const { return ___source_9; }
	inline bool* get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(bool value)
	{
		___source_9 = value;
	}

	inline static int32_t get_offset_of_appManager_10() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___appManager_10)); }
	inline ApplicationManager_t2067665347 * get_appManager_10() const { return ___appManager_10; }
	inline ApplicationManager_t2067665347 ** get_address_of_appManager_10() { return &___appManager_10; }
	inline void set_appManager_10(ApplicationManager_t2067665347 * value)
	{
		___appManager_10 = value;
		Il2CppCodeGenWriteBarrier(&___appManager_10, value);
	}

	inline static int32_t get_offset_of_gps_11() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___gps_11)); }
	inline LocationManager_t1546885680 * get_gps_11() const { return ___gps_11; }
	inline LocationManager_t1546885680 ** get_address_of_gps_11() { return &___gps_11; }
	inline void set_gps_11(LocationManager_t1546885680 * value)
	{
		___gps_11 = value;
		Il2CppCodeGenWriteBarrier(&___gps_11, value);
	}

	inline static int32_t get_offset_of_business_12() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___business_12)); }
	inline DatabaseEntry_t2505461191 * get_business_12() const { return ___business_12; }
	inline DatabaseEntry_t2505461191 ** get_address_of_business_12() { return &___business_12; }
	inline void set_business_12(DatabaseEntry_t2505461191 * value)
	{
		___business_12 = value;
		Il2CppCodeGenWriteBarrier(&___business_12, value);
	}

	inline static int32_t get_offset_of_function_13() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___function_13)); }
	inline String_t* get_function_13() const { return ___function_13; }
	inline String_t** get_address_of_function_13() { return &___function_13; }
	inline void set_function_13(String_t* value)
	{
		___function_13 = value;
		Il2CppCodeGenWriteBarrier(&___function_13, value);
	}

	inline static int32_t get_offset_of_sender_14() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___sender_14)); }
	inline MailAddress_t1980919589 * get_sender_14() const { return ___sender_14; }
	inline MailAddress_t1980919589 ** get_address_of_sender_14() { return &___sender_14; }
	inline void set_sender_14(MailAddress_t1980919589 * value)
	{
		___sender_14 = value;
		Il2CppCodeGenWriteBarrier(&___sender_14, value);
	}

	inline static int32_t get_offset_of_recipient_15() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___recipient_15)); }
	inline String_t* get_recipient_15() const { return ___recipient_15; }
	inline String_t** get_address_of_recipient_15() { return &___recipient_15; }
	inline void set_recipient_15(String_t* value)
	{
		___recipient_15 = value;
		Il2CppCodeGenWriteBarrier(&___recipient_15, value);
	}

	inline static int32_t get_offset_of_credentials_16() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___credentials_16)); }
	inline Il2CppObject * get_credentials_16() const { return ___credentials_16; }
	inline Il2CppObject ** get_address_of_credentials_16() { return &___credentials_16; }
	inline void set_credentials_16(Il2CppObject * value)
	{
		___credentials_16 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_16, value);
	}

	inline static int32_t get_offset_of_smtpClient_17() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___smtpClient_17)); }
	inline SmtpClient_t2719616745 * get_smtpClient_17() const { return ___smtpClient_17; }
	inline SmtpClient_t2719616745 ** get_address_of_smtpClient_17() { return &___smtpClient_17; }
	inline void set_smtpClient_17(SmtpClient_t2719616745 * value)
	{
		___smtpClient_17 = value;
		Il2CppCodeGenWriteBarrier(&___smtpClient_17, value);
	}

	inline static int32_t get_offset_of_port_18() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___port_18)); }
	inline int32_t get_port_18() const { return ___port_18; }
	inline int32_t* get_address_of_port_18() { return &___port_18; }
	inline void set_port_18(int32_t value)
	{
		___port_18 = value;
	}

	inline static int32_t get_offset_of_path_19() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828, ___path_19)); }
	inline String_t* get_path_19() const { return ___path_19; }
	inline String_t** get_address_of_path_19() { return &___path_19; }
	inline void set_path_19(String_t* value)
	{
		___path_19 = value;
		Il2CppCodeGenWriteBarrier(&___path_19, value);
	}
};

struct FeedbackManager_t2969052828_StaticFields
{
public:
	// System.Boolean FGMNT.FeedbackManager::emailError
	bool ___emailError_5;
	// System.Boolean FGMNT.FeedbackManager::sentMail
	bool ___sentMail_6;
	// System.Net.Security.RemoteCertificateValidationCallback FGMNT.FeedbackManager::<>f__am$cache0
	RemoteCertificateValidationCallback_t2756269959 * ___U3CU3Ef__amU24cache0_20;

public:
	inline static int32_t get_offset_of_emailError_5() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828_StaticFields, ___emailError_5)); }
	inline bool get_emailError_5() const { return ___emailError_5; }
	inline bool* get_address_of_emailError_5() { return &___emailError_5; }
	inline void set_emailError_5(bool value)
	{
		___emailError_5 = value;
	}

	inline static int32_t get_offset_of_sentMail_6() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828_StaticFields, ___sentMail_6)); }
	inline bool get_sentMail_6() const { return ___sentMail_6; }
	inline bool* get_address_of_sentMail_6() { return &___sentMail_6; }
	inline void set_sentMail_6(bool value)
	{
		___sentMail_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_20() { return static_cast<int32_t>(offsetof(FeedbackManager_t2969052828_StaticFields, ___U3CU3Ef__amU24cache0_20)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_U3CU3Ef__amU24cache0_20() const { return ___U3CU3Ef__amU24cache0_20; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_U3CU3Ef__amU24cache0_20() { return &___U3CU3Ef__amU24cache0_20; }
	inline void set_U3CU3Ef__amU24cache0_20(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___U3CU3Ef__amU24cache0_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
