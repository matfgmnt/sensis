﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.DigitalEyewearBehaviour
struct DigitalEyewearBehaviour_t495394589;

// System.Void Vuforia.DigitalEyewearBehaviour::.ctor()
extern "C"  void DigitalEyewearBehaviour__ctor_m1415568416 (DigitalEyewearBehaviour_t495394589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DigitalEyewearBehaviour Vuforia.DigitalEyewearBehaviour::get_Instance()
extern "C"  DigitalEyewearBehaviour_t495394589 * DigitalEyewearBehaviour_get_Instance_m3807753842 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearBehaviour::.cctor()
extern "C"  void DigitalEyewearBehaviour__cctor_m2294357409 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
