﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FGMNT_DatabaseEntry2505461191.h"

// FGMNT.LocationManager
struct LocationManager_t1546885680;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// FGMNT.DatabaseEntry
struct DatabaseEntry_t2505461191;
// System.Collections.Generic.List`1<FGMNT.DatabaseEntry>
struct List_1_t1874582323;

// System.Void FGMNT.LocationManager::.ctor()
extern "C"  void LocationManager__ctor_m3139901537 (LocationManager_t1546885680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FGMNT.LocationManager::Start()
extern "C"  Il2CppObject * LocationManager_Start_m2709176915 (LocationManager_t1546885680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FGMNT.DatabaseEntry FGMNT.LocationManager::FindNearestLocation(System.Collections.Generic.List`1<FGMNT.DatabaseEntry>)
extern "C"  DatabaseEntry_t2505461191 * LocationManager_FindNearestLocation_m1564866212 (LocationManager_t1546885680 * __this, List_1_t1874582323 * ___businesses0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FGMNT.LocationManager::<FindNearestLocation>m__0(FGMNT.DatabaseEntry)
extern "C"  float LocationManager_U3CFindNearestLocationU3Em__0_m4071291575 (Il2CppObject * __this /* static, unused */, DatabaseEntry_t2505461191 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
