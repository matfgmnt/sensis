﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_Provider2882126354.h"
#include "System_Configuration_System_Configuration_Provider2548499159.h"
#include "System_Configuration_System_Configuration_ClientCo4294641134.h"
#include "System_Configuration_System_Configuration_ConfigNa2395569530.h"
#include "System_Configuration_System_Configuration_ConfigInf546730838.h"
#include "System_Configuration_System_Configuration_Configur3335372970.h"
#include "System_Configuration_System_Configuration_Configur3250313246.h"
#include "System_Configuration_System_Configuration_Configur3860111898.h"
#include "System_Configuration_System_Configuration_Configur2811353736.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"
#include "System_Configuration_System_Configuration_ElementMa997038224.h"
#include "System_Configuration_System_Configuration_Configur1911180302.h"
#include "System_Configuration_System_Configuration_Configur3305291330.h"
#include "System_Configuration_System_Configuration_Configur1806001494.h"
#include "System_Configuration_System_Configuration_Configur1362721126.h"
#include "System_Configuration_System_Configuration_Configur2625210096.h"
#include "System_Configuration_System_Configuration_Configur1895107553.h"
#include "System_Configuration_System_Configuration_Configur1903842989.h"
#include "System_Configuration_System_Configuration_Configura131834733.h"
#include "System_Configuration_System_Configuration_Configur1011762925.h"
#include "System_Configuration_System_Configuration_Configur2608608455.h"
#include "System_Configuration_System_Configuration_Configur2048066811.h"
#include "System_Configuration_System_Configuration_Configur3655647199.h"
#include "System_Configuration_System_Configuration_Configur3473514151.h"
#include "System_Configuration_System_Configuration_Configur3219689025.h"
#include "System_Configuration_System_Configuration_Configura700320212.h"
#include "System_Configuration_System_Configuration_Configur2600766927.h"
#include "System_Configuration_System_Configuration_Configur4261113299.h"
#include "System_Configuration_System_Configuration_Configur1795270620.h"
#include "System_Configuration_System_Configuration_Configur2230982736.h"
#include "System_Configuration_System_Configuration_Configura575145286.h"
#include "System_Configuration_System_Configuration_Configur1204907851.h"
#include "System_Configuration_System_Configuration_Configur1007519140.h"
#include "System_Configuration_System_Configuration_Configura210547623.h"
#include "System_Configuration_ConfigXmlTextReader3212066157.h"
#include "System_Configuration_System_Configuration_DefaultS3840532724.h"
#include "System_Configuration_System_Configuration_DefaultVa300527515.h"
#include "System_Configuration_System_Configuration_ElementI3165583784.h"
#include "System_Configuration_System_Configuration_ExeConfi1419586304.h"
#include "System_Configuration_System_Configuration_IgnoreSec681509237.h"
#include "System_Configuration_System_Configuration_Internal3846641927.h"
#include "System_Configuration_System_Configuration_Internal2108740756.h"
#include "System_Configuration_System_Configuration_InternalC547577555.h"
#include "System_Configuration_System_Configuration_ExeConfi2778769322.h"
#include "System_Configuration_System_Configuration_InternalC547578517.h"
#include "System_Configuration_System_Configuration_Property2089433965.h"
#include "System_Configuration_System_Configuration_PropertyI954922393.h"
#include "System_Configuration_System_Configuration_Property1453501302.h"
#include "System_Configuration_System_Configuration_Property1217826846.h"
#include "System_Configuration_System_Configuration_Protecte1807950812.h"
#include "System_Configuration_System_Configuration_Protecte3971982415.h"
#include "System_Configuration_System_Configuration_Protected388338823.h"
#include "System_Configuration_System_Configuration_Protecte3541826375.h"
#include "System_Configuration_System_Configuration_ProviderS873049714.h"
#include "System_Configuration_System_Configuration_ProviderS585304908.h"
#include "System_Configuration_System_Configuration_SectionI1739019515.h"
#include "System_Configuration_System_Configuration_SectionG2346323570.h"
#include "System_Configuration_System_Configuration_ConfigIn3264723080.h"
#include "System_Configuration_System_Configuration_SectionI2754609709.h"
#include "System_Configuration_System_MonoTODOAttribute3487514019.h"
#include "System_Configuration_System_MonoInternalNoteAttrib4192790486.h"
#include "System_U3CModuleU3E3783534214.h"
#include "System_System_Runtime_InteropServices_DefaultParame589541489.h"
#include "System_Locale4255929014.h"
#include "System_System_MonoTODOAttribute3487514019.h"
#include "System_System_MonoLimitationAttribute17004303.h"
#include "System_System_Collections_Specialized_HybridDiction290043810.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098.h"
#include "System_System_Collections_Specialized_ListDictiona1923170152.h"
#include "System_System_Collections_Specialized_ListDictionar528898270.h"
#include "System_System_Collections_Specialized_ListDictiona2037848305.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "System_System_Collections_Specialized_NameObjectCo3244489099.h"
#include "System_System_Collections_Specialized_NameObjectCo1718269396.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Collections_Specialized_StringCollect352985975.h"
#include "System_System_Collections_Specialized_StringDictio1070889667.h"
#include "System_System_Collections_Specialized_StringEnumera441637433.h"
#include "System_System_ComponentModel_ArrayConverter2804512129.h"
#include "System_System_ComponentModel_ArrayConverter_ArrayPr599180064.h"
#include "System_System_ComponentModel_AsyncCompletedEventArgs83270938.h"
#include "System_System_ComponentModel_AsyncOperation1185541675.h"
#include "System_System_ComponentModel_AsyncOperationManager3553158318.h"
#include "System_System_ComponentModel_AttributeCollection1925812292.h"
#include "System_System_ComponentModel_BackgroundWorker4230068110.h"
#include "System_System_ComponentModel_BackgroundWorker_Proce308732489.h"
#include "System_System_ComponentModel_BaseNumberConverter1130358776.h"




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (ProviderBase_t2882126354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1506[3] = 
{
	ProviderBase_t2882126354::get_offset_of_alreadyInitialized_0(),
	ProviderBase_t2882126354::get_offset_of__description_1(),
	ProviderBase_t2882126354::get_offset_of__name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (ProviderCollection_t2548499159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1507[3] = 
{
	ProviderCollection_t2548499159::get_offset_of_lookup_0(),
	ProviderCollection_t2548499159::get_offset_of_readOnly_1(),
	ProviderCollection_t2548499159::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (ClientConfigurationSystem_t4294641134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1508[1] = 
{
	ClientConfigurationSystem_t4294641134::get_offset_of_cfg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (ConfigNameValueCollection_t2395569530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1509[1] = 
{
	ConfigNameValueCollection_t2395569530::get_offset_of_modified_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (ConfigInfo_t546730838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1510[6] = 
{
	ConfigInfo_t546730838::get_offset_of_Name_0(),
	ConfigInfo_t546730838::get_offset_of_TypeName_1(),
	ConfigInfo_t546730838::get_offset_of_Type_2(),
	ConfigInfo_t546730838::get_offset_of_streamName_3(),
	ConfigInfo_t546730838::get_offset_of_Parent_4(),
	ConfigInfo_t546730838::get_offset_of_ConfigHost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (Configuration_t3335372970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1511[12] = 
{
	Configuration_t3335372970::get_offset_of_parent_0(),
	Configuration_t3335372970::get_offset_of_elementData_1(),
	Configuration_t3335372970::get_offset_of_streamName_2(),
	Configuration_t3335372970::get_offset_of_rootSectionGroup_3(),
	Configuration_t3335372970::get_offset_of_locations_4(),
	Configuration_t3335372970::get_offset_of_rootGroup_5(),
	Configuration_t3335372970::get_offset_of_system_6(),
	Configuration_t3335372970::get_offset_of_hasFile_7(),
	Configuration_t3335372970::get_offset_of_rootNamespace_8(),
	Configuration_t3335372970::get_offset_of_configPath_9(),
	Configuration_t3335372970::get_offset_of_locationConfigPath_10(),
	Configuration_t3335372970::get_offset_of_locationSubPath_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (ConfigurationAllowDefinition_t3250313246)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1512[5] = 
{
	ConfigurationAllowDefinition_t3250313246::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (ConfigurationAllowExeDefinition_t3860111898)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1513[5] = 
{
	ConfigurationAllowExeDefinition_t3860111898::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (ConfigurationCollectionAttribute_t2811353736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1514[5] = 
{
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_addItemName_0(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_clearItemsName_1(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_removeItemName_2(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_collectionType_3(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_itemType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (ConfigurationElement_t1776195828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1515[13] = 
{
	ConfigurationElement_t1776195828::get_offset_of_rawXml_0(),
	ConfigurationElement_t1776195828::get_offset_of_modified_1(),
	ConfigurationElement_t1776195828::get_offset_of_map_2(),
	ConfigurationElement_t1776195828::get_offset_of_keyProps_3(),
	ConfigurationElement_t1776195828::get_offset_of_defaultCollection_4(),
	ConfigurationElement_t1776195828::get_offset_of_readOnly_5(),
	ConfigurationElement_t1776195828::get_offset_of_elementInfo_6(),
	ConfigurationElement_t1776195828::get_offset_of__configuration_7(),
	ConfigurationElement_t1776195828::get_offset_of_lockAllAttributesExcept_8(),
	ConfigurationElement_t1776195828::get_offset_of_lockAllElementsExcept_9(),
	ConfigurationElement_t1776195828::get_offset_of_lockAttributes_10(),
	ConfigurationElement_t1776195828::get_offset_of_lockElements_11(),
	ConfigurationElement_t1776195828::get_offset_of_lockItem_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (ElementMap_t997038224), -1, sizeof(ElementMap_t997038224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1516[3] = 
{
	ElementMap_t997038224_StaticFields::get_offset_of_elementMaps_0(),
	ElementMap_t997038224::get_offset_of_properties_1(),
	ElementMap_t997038224::get_offset_of_collectionAttribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (ConfigurationElementCollection_t1911180302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1517[10] = 
{
	ConfigurationElementCollection_t1911180302::get_offset_of_list_13(),
	ConfigurationElementCollection_t1911180302::get_offset_of_removed_14(),
	ConfigurationElementCollection_t1911180302::get_offset_of_inherited_15(),
	ConfigurationElementCollection_t1911180302::get_offset_of_emitClear_16(),
	ConfigurationElementCollection_t1911180302::get_offset_of_modified_17(),
	ConfigurationElementCollection_t1911180302::get_offset_of_comparer_18(),
	ConfigurationElementCollection_t1911180302::get_offset_of_inheritedLimitIndex_19(),
	ConfigurationElementCollection_t1911180302::get_offset_of_addElementName_20(),
	ConfigurationElementCollection_t1911180302::get_offset_of_clearElementName_21(),
	ConfigurationElementCollection_t1911180302::get_offset_of_removeElementName_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (ConfigurationRemoveElement_t3305291330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1518[3] = 
{
	ConfigurationRemoveElement_t3305291330::get_offset_of_properties_13(),
	ConfigurationRemoveElement_t3305291330::get_offset_of__origElement_14(),
	ConfigurationRemoveElement_t3305291330::get_offset_of__origCollection_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (ConfigurationElementCollectionType_t1806001494)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1519[5] = 
{
	ConfigurationElementCollectionType_t1806001494::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (ConfigurationErrorsException_t1362721126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1520[2] = 
{
	ConfigurationErrorsException_t1362721126::get_offset_of_filename_13(),
	ConfigurationErrorsException_t1362721126::get_offset_of_line_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (ConfigurationFileMap_t2625210096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1521[1] = 
{
	ConfigurationFileMap_t2625210096::get_offset_of_machineConfigFilename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (ConfigurationLocation_t1895107553), -1, sizeof(ConfigurationLocation_t1895107553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1522[7] = 
{
	ConfigurationLocation_t1895107553_StaticFields::get_offset_of_pathTrimChars_0(),
	ConfigurationLocation_t1895107553::get_offset_of_path_1(),
	ConfigurationLocation_t1895107553::get_offset_of_configuration_2(),
	ConfigurationLocation_t1895107553::get_offset_of_parent_3(),
	ConfigurationLocation_t1895107553::get_offset_of_xmlContent_4(),
	ConfigurationLocation_t1895107553::get_offset_of_parentResolved_5(),
	ConfigurationLocation_t1895107553::get_offset_of_allowOverride_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (ConfigurationLocationCollection_t1903842989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (ConfigurationLockType_t131834733)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1524[4] = 
{
	ConfigurationLockType_t131834733::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (ConfigurationLockCollection_t1011762925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1525[6] = 
{
	ConfigurationLockCollection_t1011762925::get_offset_of_names_0(),
	ConfigurationLockCollection_t1011762925::get_offset_of_element_1(),
	ConfigurationLockCollection_t1011762925::get_offset_of_lockType_2(),
	ConfigurationLockCollection_t1011762925::get_offset_of_is_modified_3(),
	ConfigurationLockCollection_t1011762925::get_offset_of_valid_name_hash_4(),
	ConfigurationLockCollection_t1011762925::get_offset_of_valid_names_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (ConfigurationManager_t2608608455), -1, sizeof(ConfigurationManager_t2608608455_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1526[3] = 
{
	ConfigurationManager_t2608608455_StaticFields::get_offset_of_configFactory_0(),
	ConfigurationManager_t2608608455_StaticFields::get_offset_of_configSystem_1(),
	ConfigurationManager_t2608608455_StaticFields::get_offset_of_lockobj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (ConfigurationProperty_t2048066811), -1, sizeof(ConfigurationProperty_t2048066811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1527[9] = 
{
	ConfigurationProperty_t2048066811_StaticFields::get_offset_of_NoDefaultValue_0(),
	ConfigurationProperty_t2048066811::get_offset_of_name_1(),
	ConfigurationProperty_t2048066811::get_offset_of_type_2(),
	ConfigurationProperty_t2048066811::get_offset_of_default_value_3(),
	ConfigurationProperty_t2048066811::get_offset_of_converter_4(),
	ConfigurationProperty_t2048066811::get_offset_of_validation_5(),
	ConfigurationProperty_t2048066811::get_offset_of_flags_6(),
	ConfigurationProperty_t2048066811::get_offset_of_description_7(),
	ConfigurationProperty_t2048066811::get_offset_of_collectionAttribute_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (ConfigurationPropertyAttribute_t3655647199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1528[3] = 
{
	ConfigurationPropertyAttribute_t3655647199::get_offset_of_name_0(),
	ConfigurationPropertyAttribute_t3655647199::get_offset_of_default_value_1(),
	ConfigurationPropertyAttribute_t3655647199::get_offset_of_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (ConfigurationPropertyCollection_t3473514151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1529[1] = 
{
	ConfigurationPropertyCollection_t3473514151::get_offset_of_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (ConfigurationPropertyOptions_t3219689025)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1530[5] = 
{
	ConfigurationPropertyOptions_t3219689025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (ConfigurationSaveMode_t700320212)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1531[4] = 
{
	ConfigurationSaveMode_t700320212::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (ConfigurationSection_t2600766927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1532[4] = 
{
	ConfigurationSection_t2600766927::get_offset_of_sectionInformation_13(),
	ConfigurationSection_t2600766927::get_offset_of_section_handler_14(),
	ConfigurationSection_t2600766927::get_offset_of_externalDataXml_15(),
	ConfigurationSection_t2600766927::get_offset_of__configContext_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (ConfigurationSectionCollection_t4261113299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1533[2] = 
{
	ConfigurationSectionCollection_t4261113299::get_offset_of_group_10(),
	ConfigurationSectionCollection_t4261113299::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t1795270620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1534[5] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U3CU24s_32U3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U3CkeyU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24PC_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (ConfigurationSectionGroup_t2230982736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1535[5] = 
{
	ConfigurationSectionGroup_t2230982736::get_offset_of_sections_0(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_groups_1(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_config_2(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_group_3(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_initialized_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (ConfigurationSectionGroupCollection_t575145286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1536[2] = 
{
	ConfigurationSectionGroupCollection_t575145286::get_offset_of_group_10(),
	ConfigurationSectionGroupCollection_t575145286::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (ConfigurationUserLevel_t1204907851)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1537[4] = 
{
	ConfigurationUserLevel_t1204907851::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (ConfigurationValidatorAttribute_t1007519140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1538[2] = 
{
	ConfigurationValidatorAttribute_t1007519140::get_offset_of_validatorType_0(),
	ConfigurationValidatorAttribute_t1007519140::get_offset_of_instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (ConfigurationValidatorBase_t210547623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (ConfigXmlTextReader_t3212066157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1540[1] = 
{
	ConfigXmlTextReader_t3212066157::get_offset_of_fileName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (DefaultSection_t3840532724), -1, sizeof(DefaultSection_t3840532724_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1541[1] = 
{
	DefaultSection_t3840532724_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (DefaultValidator_t300527515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (ElementInformation_t3165583784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1543[3] = 
{
	ElementInformation_t3165583784::get_offset_of_propertyInfo_0(),
	ElementInformation_t3165583784::get_offset_of_owner_1(),
	ElementInformation_t3165583784::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (ExeConfigurationFileMap_t1419586304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1544[3] = 
{
	ExeConfigurationFileMap_t1419586304::get_offset_of_exeConfigFilename_1(),
	ExeConfigurationFileMap_t1419586304::get_offset_of_localUserConfigFilename_2(),
	ExeConfigurationFileMap_t1419586304::get_offset_of_roamingUserConfigFilename_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (IgnoreSection_t681509237), -1, sizeof(IgnoreSection_t681509237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1545[2] = 
{
	IgnoreSection_t681509237::get_offset_of_xml_17(),
	IgnoreSection_t681509237_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (InternalConfigurationFactory_t3846641927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (InternalConfigurationSystem_t2108740756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1547[3] = 
{
	InternalConfigurationSystem_t2108740756::get_offset_of_host_0(),
	InternalConfigurationSystem_t2108740756::get_offset_of_root_1(),
	InternalConfigurationSystem_t2108740756::get_offset_of_hostInitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (InternalConfigurationHost_t547577555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (ExeConfigurationHost_t2778769322), -1, sizeof(ExeConfigurationHost_t2778769322_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1549[3] = 
{
	ExeConfigurationHost_t2778769322::get_offset_of_map_0(),
	ExeConfigurationHost_t2778769322::get_offset_of_level_1(),
	ExeConfigurationHost_t2778769322_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (InternalConfigurationRoot_t547578517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1550[2] = 
{
	InternalConfigurationRoot_t547578517::get_offset_of_host_0(),
	InternalConfigurationRoot_t547578517::get_offset_of_isDesignTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (PropertyInformation_t2089433965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1551[5] = 
{
	PropertyInformation_t2089433965::get_offset_of_isModified_0(),
	PropertyInformation_t2089433965::get_offset_of_val_1(),
	PropertyInformation_t2089433965::get_offset_of_origin_2(),
	PropertyInformation_t2089433965::get_offset_of_owner_3(),
	PropertyInformation_t2089433965::get_offset_of_property_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (PropertyInformationCollection_t954922393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (PropertyInformationEnumerator_t1453501302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1553[2] = 
{
	PropertyInformationEnumerator_t1453501302::get_offset_of_collection_0(),
	PropertyInformationEnumerator_t1453501302::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (PropertyValueOrigin_t1217826846)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1554[4] = 
{
	PropertyValueOrigin_t1217826846::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (ProtectedConfiguration_t1807950812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (ProtectedConfigurationProvider_t3971982415), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (ProtectedConfigurationProviderCollection_t388338823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (ProtectedConfigurationSection_t3541826375), -1, sizeof(ProtectedConfigurationSection_t3541826375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1558[4] = 
{
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_defaultProviderProp_17(),
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_providersProp_18(),
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_properties_19(),
	ProtectedConfigurationSection_t3541826375::get_offset_of_providers_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (ProviderSettings_t873049714), -1, sizeof(ProviderSettings_t873049714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1559[4] = 
{
	ProviderSettings_t873049714::get_offset_of_parameters_13(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_nameProp_14(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_typeProp_15(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_properties_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (ProviderSettingsCollection_t585304908), -1, sizeof(ProviderSettingsCollection_t585304908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1560[1] = 
{
	ProviderSettingsCollection_t585304908_StaticFields::get_offset_of_props_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (SectionInfo_t1739019515), -1, sizeof(SectionInfo_t1739019515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1561[6] = 
{
	SectionInfo_t1739019515::get_offset_of_allowLocation_6(),
	SectionInfo_t1739019515::get_offset_of_requirePermission_7(),
	SectionInfo_t1739019515::get_offset_of_restartOnExternalChanges_8(),
	SectionInfo_t1739019515::get_offset_of_allowDefinition_9(),
	SectionInfo_t1739019515::get_offset_of_allowExeDefinition_10(),
	SectionInfo_t1739019515_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (SectionGroupInfo_t2346323570), -1, sizeof(SectionGroupInfo_t2346323570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1562[3] = 
{
	SectionGroupInfo_t2346323570::get_offset_of_sections_6(),
	SectionGroupInfo_t2346323570::get_offset_of_groups_7(),
	SectionGroupInfo_t2346323570_StaticFields::get_offset_of_emptyList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (ConfigInfoCollection_t3264723080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (SectionInformation_t2754609709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1564[13] = 
{
	SectionInformation_t2754609709::get_offset_of_parent_0(),
	SectionInformation_t2754609709::get_offset_of_allow_definition_1(),
	SectionInformation_t2754609709::get_offset_of_allow_exe_definition_2(),
	SectionInformation_t2754609709::get_offset_of_allow_location_3(),
	SectionInformation_t2754609709::get_offset_of_allow_override_4(),
	SectionInformation_t2754609709::get_offset_of_inherit_on_child_apps_5(),
	SectionInformation_t2754609709::get_offset_of_restart_on_external_changes_6(),
	SectionInformation_t2754609709::get_offset_of_require_permission_7(),
	SectionInformation_t2754609709::get_offset_of_config_source_8(),
	SectionInformation_t2754609709::get_offset_of_name_9(),
	SectionInformation_t2754609709::get_offset_of_raw_xml_10(),
	SectionInformation_t2754609709::get_offset_of_protection_provider_11(),
	SectionInformation_t2754609709::get_offset_of_U3CConfigFilePathU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (MonoTODOAttribute_t3487514021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1565[1] = 
{
	MonoTODOAttribute_t3487514021::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (MonoInternalNoteAttribute_t4192790486), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (U3CModuleU3E_t3783534218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (DefaultParameterValueAttribute_t589541489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1568[1] = 
{
	DefaultParameterValueAttribute_t589541489::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (Locale_t4255929016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (MonoTODOAttribute_t3487514022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1570[1] = 
{
	MonoTODOAttribute_t3487514022::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (MonoLimitationAttribute_t17004304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1572[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1573[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1574[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1575[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1576[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (HybridDictionary_t290043810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1577[3] = 
{
	HybridDictionary_t290043810::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t290043810::get_offset_of_hashtable_1(),
	HybridDictionary_t290043810::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (ListDictionary_t3458713452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1578[4] = 
{
	ListDictionary_t3458713452::get_offset_of_count_0(),
	ListDictionary_t3458713452::get_offset_of_version_1(),
	ListDictionary_t3458713452::get_offset_of_head_2(),
	ListDictionary_t3458713452::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (DictionaryNode_t2725637098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1579[3] = 
{
	DictionaryNode_t2725637098::get_offset_of_key_0(),
	DictionaryNode_t2725637098::get_offset_of_value_1(),
	DictionaryNode_t2725637098::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (DictionaryNodeEnumerator_t1923170152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1580[4] = 
{
	DictionaryNodeEnumerator_t1923170152::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (DictionaryNodeCollection_t528898270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1581[2] = 
{
	DictionaryNodeCollection_t528898270::get_offset_of_dict_0(),
	DictionaryNodeCollection_t528898270::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (DictionaryNodeCollectionEnumerator_t2037848305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1582[2] = 
{
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (NameObjectCollectionBase_t2034248631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1583[10] = 
{
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t2034248631::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t2034248631::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t2034248631::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (_Item_t3244489099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1584[2] = 
{
	_Item_t3244489099::get_offset_of_key_0(),
	_Item_t3244489099::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (_KeysEnumerator_t1718269396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1585[2] = 
{
	_KeysEnumerator_t1718269396::get_offset_of_m_collection_0(),
	_KeysEnumerator_t1718269396::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (KeysCollection_t633582367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1586[1] = 
{
	KeysCollection_t633582367::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (NameValueCollection_t3047564564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1587[2] = 
{
	NameValueCollection_t3047564564::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t3047564564::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (StringCollection_t352985975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1588[1] = 
{
	StringCollection_t352985975::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (StringDictionary_t1070889667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[1] = 
{
	StringDictionary_t1070889667::get_offset_of_contents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (StringEnumerator_t441637433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[1] = 
{
	StringEnumerator_t441637433::get_offset_of_enumerable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (ArrayConverter_t2804512129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (ArrayPropertyDescriptor_t599180064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1592[2] = 
{
	ArrayPropertyDescriptor_t599180064::get_offset_of_index_3(),
	ArrayPropertyDescriptor_t599180064::get_offset_of_array_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (AsyncCompletedEventArgs_t83270938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1593[3] = 
{
	AsyncCompletedEventArgs_t83270938::get_offset_of__error_1(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__cancelled_2(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__userState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (AsyncOperation_t1185541675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1594[3] = 
{
	AsyncOperation_t1185541675::get_offset_of_ctx_0(),
	AsyncOperation_t1185541675::get_offset_of_state_1(),
	AsyncOperation_t1185541675::get_offset_of_done_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (AsyncOperationManager_t3553158318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (AttributeCollection_t1925812292), -1, sizeof(AttributeCollection_t1925812292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1596[2] = 
{
	AttributeCollection_t1925812292::get_offset_of_attrList_0(),
	AttributeCollection_t1925812292_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (BackgroundWorker_t4230068110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1597[7] = 
{
	BackgroundWorker_t4230068110::get_offset_of_async_4(),
	BackgroundWorker_t4230068110::get_offset_of_cancel_pending_5(),
	BackgroundWorker_t4230068110::get_offset_of_report_progress_6(),
	BackgroundWorker_t4230068110::get_offset_of_support_cancel_7(),
	BackgroundWorker_t4230068110::get_offset_of_DoWork_8(),
	BackgroundWorker_t4230068110::get_offset_of_ProgressChanged_9(),
	BackgroundWorker_t4230068110::get_offset_of_RunWorkerCompleted_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (ProcessWorkerEventHandler_t308732489), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (BaseNumberConverter_t1130358776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1599[1] = 
{
	BaseNumberConverter_t1130358776::get_offset_of_InnerType_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
