﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// FGMNT.ContactManager/<ExecuteAction>c__Iterator0
struct U3CExecuteActionU3Ec__Iterator0_t383275672;
// System.Object
struct Il2CppObject;

// System.Void FGMNT.ContactManager/<ExecuteAction>c__Iterator0::.ctor()
extern "C"  void U3CExecuteActionU3Ec__Iterator0__ctor_m85300485 (U3CExecuteActionU3Ec__Iterator0_t383275672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FGMNT.ContactManager/<ExecuteAction>c__Iterator0::MoveNext()
extern "C"  bool U3CExecuteActionU3Ec__Iterator0_MoveNext_m1626272595 (U3CExecuteActionU3Ec__Iterator0_t383275672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FGMNT.ContactManager/<ExecuteAction>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExecuteActionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1735902379 (U3CExecuteActionU3Ec__Iterator0_t383275672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FGMNT.ContactManager/<ExecuteAction>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteActionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1319216563 (U3CExecuteActionU3Ec__Iterator0_t383275672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ContactManager/<ExecuteAction>c__Iterator0::Dispose()
extern "C"  void U3CExecuteActionU3Ec__Iterator0_Dispose_m2517893298 (U3CExecuteActionU3Ec__Iterator0_t383275672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ContactManager/<ExecuteAction>c__Iterator0::Reset()
extern "C"  void U3CExecuteActionU3Ec__Iterator0_Reset_m1806767468 (U3CExecuteActionU3Ec__Iterator0_t383275672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FGMNT.ContactManager/<ExecuteAction>c__Iterator0::<>m__0()
extern "C"  bool U3CExecuteActionU3Ec__Iterator0_U3CU3Em__0_m3558862920 (U3CExecuteActionU3Ec__Iterator0_t383275672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
