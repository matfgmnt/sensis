﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3383807694MethodDeclarations.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1154463730(__this, ___host0, method) ((  void (*) (Enumerator_t2257441079 *, Dictionary_2_t570908315 *, const MethodInfo*))Enumerator__ctor_m2988407410_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1681005229(__this, method) ((  Il2CppObject * (*) (Enumerator_t2257441079 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1648049763_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1860741309(__this, method) ((  void (*) (Enumerator_t2257441079 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m655633499_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::Dispose()
#define Enumerator_Dispose_m1065201210(__this, method) ((  void (*) (Enumerator_t2257441079 *, const MethodInfo*))Enumerator_Dispose_m2369319718_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::MoveNext()
#define Enumerator_MoveNext_m2754295353(__this, method) ((  bool (*) (Enumerator_t2257441079 *, const MethodInfo*))Enumerator_MoveNext_m1091131935_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>::get_Current()
#define Enumerator_get_Current_m101830350(__this, method) ((  Il2CppObject * (*) (Enumerator_t2257441079 *, const MethodInfo*))Enumerator_get_Current_m2132741765_gshared)(__this, method)
