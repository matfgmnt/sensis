﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// FGMNT.ApplicationManager
struct ApplicationManager_t2067665347;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t3077176941;
// System.String
struct String_t;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.NewCloudHandler
struct  NewCloudHandler_t3451870399  : public MonoBehaviour_t1158329972
{
public:
	// FGMNT.ApplicationManager FGMNT.NewCloudHandler::applicationManager
	ApplicationManager_t2067665347 * ___applicationManager_2;
	// Vuforia.CloudRecoBehaviour FGMNT.NewCloudHandler::mCloudRecoBehaviour
	CloudRecoBehaviour_t3077176941 * ___mCloudRecoBehaviour_3;
	// System.Boolean FGMNT.NewCloudHandler::mIsScanning
	bool ___mIsScanning_4;
	// System.String FGMNT.NewCloudHandler::mTargetMetaData
	String_t* ___mTargetMetaData_5;

public:
	inline static int32_t get_offset_of_applicationManager_2() { return static_cast<int32_t>(offsetof(NewCloudHandler_t3451870399, ___applicationManager_2)); }
	inline ApplicationManager_t2067665347 * get_applicationManager_2() const { return ___applicationManager_2; }
	inline ApplicationManager_t2067665347 ** get_address_of_applicationManager_2() { return &___applicationManager_2; }
	inline void set_applicationManager_2(ApplicationManager_t2067665347 * value)
	{
		___applicationManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___applicationManager_2, value);
	}

	inline static int32_t get_offset_of_mCloudRecoBehaviour_3() { return static_cast<int32_t>(offsetof(NewCloudHandler_t3451870399, ___mCloudRecoBehaviour_3)); }
	inline CloudRecoBehaviour_t3077176941 * get_mCloudRecoBehaviour_3() const { return ___mCloudRecoBehaviour_3; }
	inline CloudRecoBehaviour_t3077176941 ** get_address_of_mCloudRecoBehaviour_3() { return &___mCloudRecoBehaviour_3; }
	inline void set_mCloudRecoBehaviour_3(CloudRecoBehaviour_t3077176941 * value)
	{
		___mCloudRecoBehaviour_3 = value;
		Il2CppCodeGenWriteBarrier(&___mCloudRecoBehaviour_3, value);
	}

	inline static int32_t get_offset_of_mIsScanning_4() { return static_cast<int32_t>(offsetof(NewCloudHandler_t3451870399, ___mIsScanning_4)); }
	inline bool get_mIsScanning_4() const { return ___mIsScanning_4; }
	inline bool* get_address_of_mIsScanning_4() { return &___mIsScanning_4; }
	inline void set_mIsScanning_4(bool value)
	{
		___mIsScanning_4 = value;
	}

	inline static int32_t get_offset_of_mTargetMetaData_5() { return static_cast<int32_t>(offsetof(NewCloudHandler_t3451870399, ___mTargetMetaData_5)); }
	inline String_t* get_mTargetMetaData_5() const { return ___mTargetMetaData_5; }
	inline String_t** get_address_of_mTargetMetaData_5() { return &___mTargetMetaData_5; }
	inline void set_mTargetMetaData_5(String_t* value)
	{
		___mTargetMetaData_5 = value;
		Il2CppCodeGenWriteBarrier(&___mTargetMetaData_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
