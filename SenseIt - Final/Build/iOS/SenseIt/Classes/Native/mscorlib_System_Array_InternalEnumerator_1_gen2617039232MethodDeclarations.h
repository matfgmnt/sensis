﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"


// System.Void System.Array/InternalEnumerator`1<Mono.Xml.DTDNode>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3154609917(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2617039232 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Xml.DTDNode>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2970306121(__this, method) ((  void (*) (InternalEnumerator_1_t2617039232 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Xml.DTDNode>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m278669697(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2617039232 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Xml.DTDNode>::Dispose()
#define InternalEnumerator_1_Dispose_m2923224182(__this, method) ((  void (*) (InternalEnumerator_1_t2617039232 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Xml.DTDNode>::MoveNext()
#define InternalEnumerator_1_MoveNext_m29195225(__this, method) ((  bool (*) (InternalEnumerator_1_t2617039232 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Xml.DTDNode>::get_Current()
#define InternalEnumerator_1_get_Current_m3110487612(__this, method) ((  DTDNode_t1758286970 * (*) (InternalEnumerator_1_t2617039232 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
