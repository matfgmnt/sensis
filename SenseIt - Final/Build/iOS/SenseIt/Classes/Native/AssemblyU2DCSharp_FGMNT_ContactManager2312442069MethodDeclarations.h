﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FGMNT_DatabaseEntry2505461191.h"
#include "mscorlib_System_String2029220233.h"

// FGMNT.ContactManager
struct ContactManager_t2312442069;
// FGMNT.DatabaseEntry
struct DatabaseEntry_t2505461191;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

// System.Void FGMNT.ContactManager::.ctor()
extern "C"  void ContactManager__ctor_m3274917340 (ContactManager_t2312442069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ContactManager::Start()
extern "C"  void ContactManager_Start_m3605669228 (ContactManager_t2312442069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ContactManager::UpdateContact(FGMNT.DatabaseEntry)
extern "C"  void ContactManager_UpdateContact_m1450825916 (ContactManager_t2312442069 * __this, DatabaseEntry_t2505461191 * ___business0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ContactManager::Call()
extern "C"  void ContactManager_Call_m1399424034 (ContactManager_t2312442069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ContactManager::Website()
extern "C"  void ContactManager_Website_m1544523563 (ContactManager_t2312442069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ContactManager::Nearest()
extern "C"  void ContactManager_Nearest_m3020412186 (ContactManager_t2312442069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FGMNT.ContactManager::ExecuteAction(System.String,System.String)
extern "C"  Il2CppObject * ContactManager_ExecuteAction_m3935879055 (ContactManager_t2312442069 * __this, String_t* ___function0, String_t* ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
