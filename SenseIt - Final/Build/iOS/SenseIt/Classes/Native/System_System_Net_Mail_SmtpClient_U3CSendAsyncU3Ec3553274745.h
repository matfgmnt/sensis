﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Net.Mail.MailMessage
struct MailMessage_t460962712;
// System.Net.Mail.SmtpClient
struct SmtpClient_t2719616745;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mail.SmtpClient/<SendAsync>c__AnonStorey5
struct  U3CSendAsyncU3Ec__AnonStorey5_t3553274745  : public Il2CppObject
{
public:
	// System.Net.Mail.MailMessage System.Net.Mail.SmtpClient/<SendAsync>c__AnonStorey5::message
	MailMessage_t460962712 * ___message_0;
	// System.Net.Mail.SmtpClient System.Net.Mail.SmtpClient/<SendAsync>c__AnonStorey5::<>f__this
	SmtpClient_t2719616745 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey5_t3553274745, ___message_0)); }
	inline MailMessage_t460962712 * get_message_0() const { return ___message_0; }
	inline MailMessage_t460962712 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(MailMessage_t460962712 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier(&___message_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey5_t3553274745, ___U3CU3Ef__this_1)); }
	inline SmtpClient_t2719616745 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline SmtpClient_t2719616745 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(SmtpClient_t2719616745 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
