﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.Image
struct Image_t1391689025;

// System.Void Vuforia.Image::.ctor()
extern "C"  void Image__ctor_m290697421 (Image_t1391689025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
