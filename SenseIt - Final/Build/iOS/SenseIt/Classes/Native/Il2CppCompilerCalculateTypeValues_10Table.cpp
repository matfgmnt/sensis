﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExc1722175009.h"
#include "mscorlib_Mono_Math_Prime_PrimalityTest572679901.h"
#include "mscorlib_System_AppDomainInitializer3898244613.h"
#include "mscorlib_System_AssemblyLoadEventHandler2169307382.h"
#include "mscorlib_System_ConsoleCancelEventHandler1138054497.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "mscorlib_System_ResolveEventHandler3842432458.h"
#include "mscorlib_System_UnhandledExceptionEventHandler1916531888.h"
#include "mscorlib_System_Reflection_MemberFilter3405857066.h"
#include "mscorlib_System_Reflection_ModuleResolveEventHandle403447254.h"
#include "mscorlib_System_Reflection_TypeFilter2905709404.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossCont754146990.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHa324204131.h"
#include "mscorlib_System_Threading_ParameterizedThreadStart2412552885.h"
#include "mscorlib_System_Threading_SendOrPostCallback296893742.h"
#include "mscorlib_System_Threading_ThreadStart3437517264.h"
#include "mscorlib_System_Threading_TimerCallback1684927372.h"
#include "mscorlib_System_Threading_WaitCallback2798937288.h"
#include "mscorlib_System_Threading_WaitOrTimerCallback2724438238.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3672778798.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2866209749.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1703410334.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra116038562.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1892466092.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra540610921.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3672778804.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra896841275.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2866209745.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3672778802.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra420490420.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3055265540.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2914102937.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2844921915.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1957337327.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2866209739.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3672778800.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra116038558.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1703410326.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra702815517.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr1703410330.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2038352954.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2672183894.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr3604436769.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arr2441637390.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24Arra116038554.h"
#include "mscorlib_System___Il2CppComObject4064417062.h"
#include "mscorlib_System___Il2CppComDelegate3311706788.h"
#include "System_Xml_U3CModuleU3E3783534214.h"
#include "System_Xml_System_MonoTODOAttribute3487514019.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentitySelector185499482.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityField2563516441.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityPath2037874.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityStep452377251.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryField3632833996.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryFieldCollect2946985218.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntry3532015222.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryCollection1714053544.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyTable2980902100.h"
#include "System_Xml_Mono_Xml_Schema_XsdParticleStateManager4119977941.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationState3545965403.h"
#include "System_Xml_Mono_Xml_Schema_XsdElementValidationStat152111323.h"
#include "System_Xml_Mono_Xml_Schema_XsdSequenceValidationSt3542030006.h"
#include "System_Xml_Mono_Xml_Schema_XsdChoiceValidationStat1506407122.h"
#include "System_Xml_Mono_Xml_Schema_XsdAllValidationState1028212608.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyValidationState204702461.h"
#include "System_Xml_Mono_Xml_Schema_XsdAppendedValidationSt3724408090.h"
#include "System_Xml_Mono_Xml_Schema_XsdEmptyValidationState1914323912.h"
#include "System_Xml_Mono_Xml_Schema_XsdInvalidValidationSta1112885736.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidatingReader1704923617.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationContext3720679969.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDManager3860002335.h"
#include "System_Xml_Mono_Xml_Schema_XsdWildcard625524157.h"
#include "System_Xml_System_Xml_ConformanceLevel3761201363.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory3605390810.h"
#include "System_Xml_Mono_Xml_DTDAutomata545990600.h"
#include "System_Xml_Mono_Xml_DTDElementAutomata2864881036.h"
#include "System_Xml_Mono_Xml_DTDChoiceAutomata2810241733.h"
#include "System_Xml_Mono_Xml_DTDSequenceAutomata1228770437.h"
#include "System_Xml_Mono_Xml_DTDOneOrMoreAutomata1559764132.h"




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1000 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1000[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1001 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1001[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1002 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1003 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1004 = { sizeof (KeyNotFoundException_t1722175009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1005 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1005[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1006 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1006[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1007 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1008 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1009 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1009[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1010 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1010[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1011 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1011[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1012 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1012[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1013 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1014 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1015 = { sizeof (PrimalityTest_t572679901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1016 = { sizeof (AppDomainInitializer_t3898244613), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1017 = { sizeof (AssemblyLoadEventHandler_t2169307382), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1018 = { sizeof (ConsoleCancelEventHandler_t1138054497), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1019 = { sizeof (EventHandler_t277755526), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1020 = { sizeof (ResolveEventHandler_t3842432458), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1021 = { sizeof (UnhandledExceptionEventHandler_t1916531888), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1022 = { sizeof (MemberFilter_t3405857066), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1023 = { sizeof (ModuleResolveEventHandler_t403447254), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1024 = { sizeof (TypeFilter_t2905709404), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1025 = { sizeof (CrossContextDelegate_t754146990), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1026 = { sizeof (HeaderHandler_t324204131), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1027 = { sizeof (ParameterizedThreadStart_t2412552885), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1028 = { sizeof (SendOrPostCallback_t296893742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1029 = { sizeof (ThreadStart_t3437517264), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1030 = { sizeof (TimerCallback_t1684927372), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1031 = { sizeof (WaitCallback_t2798937288), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1032 = { sizeof (WaitOrTimerCallback_t2724438238), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1033 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1034 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1035 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1036 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1037 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305137), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1037[59] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D15_7(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D16_8(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D17_9(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D18_10(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D19_11(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D20_12(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D23_15(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D24_16(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D25_17(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D26_18(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D27_19(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D28_20(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D29_21(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D30_22(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D31_23(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D32_24(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D34_25(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D35_26(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D36_27(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D37_28(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D38_29(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D39_30(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D40_31(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D41_32(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D42_33(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D43_34(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D44_35(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D45_36(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D46_37(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D47_38(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D48_39(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D49_40(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D50_41(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D51_42(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D52_43(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D53_44(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D54_45(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D55_46(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D56_47(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D57_48(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D58_49(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D59_50(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D60_51(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D61_52(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D62_53(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D63_54(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D64_55(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D65_56(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D67_57(),
	U3CPrivateImplementationDetailsU3E_t1486305137_StaticFields::get_offset_of_U24U24fieldU2D68_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1038 = { sizeof (U24ArrayTypeU2452_t3672778798)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2452_t3672778798 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1039 = { sizeof (U24ArrayTypeU2424_t2866209749)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2424_t2866209749 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1040 = { sizeof (U24ArrayTypeU2416_t1703410334)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410334 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1041 = { sizeof (U24ArrayTypeU24120_t116038562)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t116038562 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1042 = { sizeof (U24ArrayTypeU243132_t1892466092)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t1892466092 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1043 = { sizeof (U24ArrayTypeU2420_t540610921)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t540610921 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1044 = { sizeof (U24ArrayTypeU2432_t3672778804)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3672778804 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1045 = { sizeof (U24ArrayTypeU2448_t896841275)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t896841275 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1046 = { sizeof (U24ArrayTypeU2464_t2866209745)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t2866209745 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1047 = { sizeof (U24ArrayTypeU2412_t3672778802)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778802 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1048 = { sizeof (U24ArrayTypeU241668_t420490420)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241668_t420490420 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1049 = { sizeof (U24ArrayTypeU242100_t3055265540)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU242100_t3055265540 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1050 = { sizeof (U24ArrayTypeU241452_t2914102937)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241452_t2914102937 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1051 = { sizeof (U24ArrayTypeU24136_t2844921915)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t2844921915 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1052 = { sizeof (U24ArrayTypeU248_t1957337327)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337327 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1053 = { sizeof (U24ArrayTypeU2484_t2866209739)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2484_t2866209739 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1054 = { sizeof (U24ArrayTypeU2472_t3672778800)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2472_t3672778800 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1055 = { sizeof (U24ArrayTypeU24124_t116038558)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24124_t116038558 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1056 = { sizeof (U24ArrayTypeU2496_t1703410326)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2496_t1703410326 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1057 = { sizeof (U24ArrayTypeU242048_t702815517)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU242048_t702815517 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1058 = { sizeof (U24ArrayTypeU2456_t1703410330)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2456_t1703410330 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1059 = { sizeof (U24ArrayTypeU24256_t2038352954)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352954 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1060 = { sizeof (U24ArrayTypeU241024_t2672183894)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t2672183894 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1061 = { sizeof (U24ArrayTypeU24640_t3604436769)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24640_t3604436769 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1062 = { sizeof (U24ArrayTypeU24160_t2441637390)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24160_t2441637390 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1063 = { sizeof (U24ArrayTypeU24128_t116038554)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t116038554 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1064 = { sizeof (Il2CppComObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1065 = { sizeof (__Il2CppComDelegate_t3311706788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1066 = { sizeof (U3CModuleU3E_t3783534215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1067 = { sizeof (MonoTODOAttribute_t3487514020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1068 = { sizeof (XsdIdentitySelector_t185499482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1068[3] = 
{
	XsdIdentitySelector_t185499482::get_offset_of_selectorPaths_0(),
	XsdIdentitySelector_t185499482::get_offset_of_fields_1(),
	XsdIdentitySelector_t185499482::get_offset_of_cachedFields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1069 = { sizeof (XsdIdentityField_t2563516441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1069[2] = 
{
	XsdIdentityField_t2563516441::get_offset_of_fieldPaths_0(),
	XsdIdentityField_t2563516441::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1070 = { sizeof (XsdIdentityPath_t2037874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1070[2] = 
{
	XsdIdentityPath_t2037874::get_offset_of_OrderedSteps_0(),
	XsdIdentityPath_t2037874::get_offset_of_Descendants_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1071 = { sizeof (XsdIdentityStep_t452377251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1071[6] = 
{
	XsdIdentityStep_t452377251::get_offset_of_IsCurrent_0(),
	XsdIdentityStep_t452377251::get_offset_of_IsAttribute_1(),
	XsdIdentityStep_t452377251::get_offset_of_IsAnyName_2(),
	XsdIdentityStep_t452377251::get_offset_of_NsName_3(),
	XsdIdentityStep_t452377251::get_offset_of_Name_4(),
	XsdIdentityStep_t452377251::get_offset_of_Namespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1072 = { sizeof (XsdKeyEntryField_t3632833996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1072[13] = 
{
	XsdKeyEntryField_t3632833996::get_offset_of_entry_0(),
	XsdKeyEntryField_t3632833996::get_offset_of_field_1(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFound_2(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldLineNumber_3(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldLinePosition_4(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldHasLineInfo_5(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldType_6(),
	XsdKeyEntryField_t3632833996::get_offset_of_Identity_7(),
	XsdKeyEntryField_t3632833996::get_offset_of_IsXsiNil_8(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFoundDepth_9(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFoundPath_10(),
	XsdKeyEntryField_t3632833996::get_offset_of_Consuming_11(),
	XsdKeyEntryField_t3632833996::get_offset_of_Consumed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1073 = { sizeof (XsdKeyEntryFieldCollection_t2946985218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1074 = { sizeof (XsdKeyEntry_t3532015222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1074[8] = 
{
	XsdKeyEntry_t3532015222::get_offset_of_StartDepth_0(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorLineNumber_1(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorLinePosition_2(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorHasLineInfo_3(),
	XsdKeyEntry_t3532015222::get_offset_of_KeyFields_4(),
	XsdKeyEntry_t3532015222::get_offset_of_KeyRefFound_5(),
	XsdKeyEntry_t3532015222::get_offset_of_OwnerSequence_6(),
	XsdKeyEntry_t3532015222::get_offset_of_keyFound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1075 = { sizeof (XsdKeyEntryCollection_t1714053544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1076 = { sizeof (XsdKeyTable_t2980902100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1076[9] = 
{
	XsdKeyTable_t2980902100::get_offset_of_alwaysTrue_0(),
	XsdKeyTable_t2980902100::get_offset_of_selector_1(),
	XsdKeyTable_t2980902100::get_offset_of_source_2(),
	XsdKeyTable_t2980902100::get_offset_of_qname_3(),
	XsdKeyTable_t2980902100::get_offset_of_refKeyName_4(),
	XsdKeyTable_t2980902100::get_offset_of_Entries_5(),
	XsdKeyTable_t2980902100::get_offset_of_FinishedEntries_6(),
	XsdKeyTable_t2980902100::get_offset_of_StartDepth_7(),
	XsdKeyTable_t2980902100::get_offset_of_ReferencedKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1077 = { sizeof (XsdParticleStateManager_t4119977941), -1, sizeof(XsdParticleStateManager_t4119977941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1077[6] = 
{
	XsdParticleStateManager_t4119977941::get_offset_of_table_0(),
	XsdParticleStateManager_t4119977941::get_offset_of_processContents_1(),
	XsdParticleStateManager_t4119977941::get_offset_of_CurrentElement_2(),
	XsdParticleStateManager_t4119977941::get_offset_of_ContextStack_3(),
	XsdParticleStateManager_t4119977941::get_offset_of_Context_4(),
	XsdParticleStateManager_t4119977941_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1078 = { sizeof (XsdValidationState_t3545965403), -1, sizeof(XsdValidationState_t3545965403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1078[3] = 
{
	XsdValidationState_t3545965403_StaticFields::get_offset_of_invalid_0(),
	XsdValidationState_t3545965403::get_offset_of_occured_1(),
	XsdValidationState_t3545965403::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1079 = { sizeof (XsdElementValidationState_t152111323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1079[1] = 
{
	XsdElementValidationState_t152111323::get_offset_of_element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1080 = { sizeof (XsdSequenceValidationState_t3542030006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1080[4] = 
{
	XsdSequenceValidationState_t3542030006::get_offset_of_seq_3(),
	XsdSequenceValidationState_t3542030006::get_offset_of_current_4(),
	XsdSequenceValidationState_t3542030006::get_offset_of_currentAutomata_5(),
	XsdSequenceValidationState_t3542030006::get_offset_of_emptiable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1081 = { sizeof (XsdChoiceValidationState_t1506407122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1081[3] = 
{
	XsdChoiceValidationState_t1506407122::get_offset_of_choice_3(),
	XsdChoiceValidationState_t1506407122::get_offset_of_emptiable_4(),
	XsdChoiceValidationState_t1506407122::get_offset_of_emptiableComputed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1082 = { sizeof (XsdAllValidationState_t1028212608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1082[2] = 
{
	XsdAllValidationState_t1028212608::get_offset_of_all_3(),
	XsdAllValidationState_t1028212608::get_offset_of_consumed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1083 = { sizeof (XsdAnyValidationState_t204702461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1083[1] = 
{
	XsdAnyValidationState_t204702461::get_offset_of_any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1084 = { sizeof (XsdAppendedValidationState_t3724408090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1084[2] = 
{
	XsdAppendedValidationState_t3724408090::get_offset_of_head_3(),
	XsdAppendedValidationState_t3724408090::get_offset_of_rest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1085 = { sizeof (XsdEmptyValidationState_t1914323912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1086 = { sizeof (XsdInvalidValidationState_t1112885736), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1087 = { sizeof (XsdValidatingReader_t1704923617), -1, sizeof(XsdValidatingReader_t1704923617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1087[30] = 
{
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_emptyAttributeArray_2(),
	XsdValidatingReader_t1704923617::get_offset_of_reader_3(),
	XsdValidatingReader_t1704923617::get_offset_of_resolver_4(),
	XsdValidatingReader_t1704923617::get_offset_of_sourceReaderSchemaInfo_5(),
	XsdValidatingReader_t1704923617::get_offset_of_readerLineInfo_6(),
	XsdValidatingReader_t1704923617::get_offset_of_validationType_7(),
	XsdValidatingReader_t1704923617::get_offset_of_schemas_8(),
	XsdValidatingReader_t1704923617::get_offset_of_namespaces_9(),
	XsdValidatingReader_t1704923617::get_offset_of_validationStarted_10(),
	XsdValidatingReader_t1704923617::get_offset_of_checkIdentity_11(),
	XsdValidatingReader_t1704923617::get_offset_of_idManager_12(),
	XsdValidatingReader_t1704923617::get_offset_of_checkKeyConstraints_13(),
	XsdValidatingReader_t1704923617::get_offset_of_keyTables_14(),
	XsdValidatingReader_t1704923617::get_offset_of_currentKeyFieldConsumers_15(),
	XsdValidatingReader_t1704923617::get_offset_of_tmpKeyrefPool_16(),
	XsdValidatingReader_t1704923617::get_offset_of_elementQNameStack_17(),
	XsdValidatingReader_t1704923617::get_offset_of_state_18(),
	XsdValidatingReader_t1704923617::get_offset_of_skipValidationDepth_19(),
	XsdValidatingReader_t1704923617::get_offset_of_xsiNilDepth_20(),
	XsdValidatingReader_t1704923617::get_offset_of_storedCharacters_21(),
	XsdValidatingReader_t1704923617::get_offset_of_shouldValidateCharacters_22(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributes_23(),
	XsdValidatingReader_t1704923617::get_offset_of_currentDefaultAttribute_24(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributesCache_25(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributeConsumed_26(),
	XsdValidatingReader_t1704923617::get_offset_of_currentAttrType_27(),
	XsdValidatingReader_t1704923617::get_offset_of_ValidationEventHandler_28(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_29(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_30(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1088 = { sizeof (XsdValidationContext_t3720679969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1088[3] = 
{
	XsdValidationContext_t3720679969::get_offset_of_xsi_type_0(),
	XsdValidationContext_t3720679969::get_offset_of_State_1(),
	XsdValidationContext_t3720679969::get_offset_of_element_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1089 = { sizeof (XsdIDManager_t3860002335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1089[3] = 
{
	XsdIDManager_t3860002335::get_offset_of_idList_0(),
	XsdIDManager_t3860002335::get_offset_of_missingIDReferences_1(),
	XsdIDManager_t3860002335::get_offset_of_thisElementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1090 = { sizeof (XsdWildcard_t625524157), -1, sizeof(XsdWildcard_t625524157_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1090[10] = 
{
	XsdWildcard_t625524157::get_offset_of_xsobj_0(),
	XsdWildcard_t625524157::get_offset_of_ResolvedProcessing_1(),
	XsdWildcard_t625524157::get_offset_of_TargetNamespace_2(),
	XsdWildcard_t625524157::get_offset_of_SkipCompile_3(),
	XsdWildcard_t625524157::get_offset_of_HasValueAny_4(),
	XsdWildcard_t625524157::get_offset_of_HasValueLocal_5(),
	XsdWildcard_t625524157::get_offset_of_HasValueOther_6(),
	XsdWildcard_t625524157::get_offset_of_HasValueTargetNamespace_7(),
	XsdWildcard_t625524157::get_offset_of_ResolvedNamespaces_8(),
	XsdWildcard_t625524157_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1091 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1093 = { sizeof (ConformanceLevel_t3761201363)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1093[4] = 
{
	ConformanceLevel_t3761201363::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1094 = { sizeof (DTDAutomataFactory_t3605390810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1094[3] = 
{
	DTDAutomataFactory_t3605390810::get_offset_of_root_0(),
	DTDAutomataFactory_t3605390810::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t3605390810::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1095 = { sizeof (DTDAutomata_t545990600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1095[1] = 
{
	DTDAutomata_t545990600::get_offset_of_root_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1096 = { sizeof (DTDElementAutomata_t2864881036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1096[1] = 
{
	DTDElementAutomata_t2864881036::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1097 = { sizeof (DTDChoiceAutomata_t2810241733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1097[4] = 
{
	DTDChoiceAutomata_t2810241733::get_offset_of_left_1(),
	DTDChoiceAutomata_t2810241733::get_offset_of_right_2(),
	DTDChoiceAutomata_t2810241733::get_offset_of_hasComputedEmptiable_3(),
	DTDChoiceAutomata_t2810241733::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1098 = { sizeof (DTDSequenceAutomata_t1228770437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1098[4] = 
{
	DTDSequenceAutomata_t1228770437::get_offset_of_left_1(),
	DTDSequenceAutomata_t1228770437::get_offset_of_right_2(),
	DTDSequenceAutomata_t1228770437::get_offset_of_hasComputedEmptiable_3(),
	DTDSequenceAutomata_t1228770437::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1099 = { sizeof (DTDOneOrMoreAutomata_t1559764132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1099[1] = 
{
	DTDOneOrMoreAutomata_t1559764132::get_offset_of_children_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
