﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearAbst3719235061.h"

// Vuforia.DigitalEyewearBehaviour
struct DigitalEyewearBehaviour_t495394589;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearBehaviour
struct  DigitalEyewearBehaviour_t495394589  : public DigitalEyewearAbstractBehaviour_t3719235061
{
public:

public:
};

struct DigitalEyewearBehaviour_t495394589_StaticFields
{
public:
	// Vuforia.DigitalEyewearBehaviour Vuforia.DigitalEyewearBehaviour::mDigitalEyewearBehaviour
	DigitalEyewearBehaviour_t495394589 * ___mDigitalEyewearBehaviour_28;

public:
	inline static int32_t get_offset_of_mDigitalEyewearBehaviour_28() { return static_cast<int32_t>(offsetof(DigitalEyewearBehaviour_t495394589_StaticFields, ___mDigitalEyewearBehaviour_28)); }
	inline DigitalEyewearBehaviour_t495394589 * get_mDigitalEyewearBehaviour_28() const { return ___mDigitalEyewearBehaviour_28; }
	inline DigitalEyewearBehaviour_t495394589 ** get_address_of_mDigitalEyewearBehaviour_28() { return &___mDigitalEyewearBehaviour_28; }
	inline void set_mDigitalEyewearBehaviour_28(DigitalEyewearBehaviour_t495394589 * value)
	{
		___mDigitalEyewearBehaviour_28 = value;
		Il2CppCodeGenWriteBarrier(&___mDigitalEyewearBehaviour_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
