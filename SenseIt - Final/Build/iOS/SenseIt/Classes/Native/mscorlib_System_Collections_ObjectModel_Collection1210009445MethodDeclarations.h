﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2231194049MethodDeclarations.h"


// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::.ctor()
#define Collection_1__ctor_m1311299063(__this, method) ((  void (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1__ctor_m3383758099_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3196076772(__this, method) ((  bool (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2832435102_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m2676435081(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1210009445 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2795445359_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m414463712(__this, method) ((  Il2CppObject * (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m539985258_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m4233628317(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1210009445 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m916188271_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m1921106873(__this, ___value0, method) ((  bool (*) (Collection_1_t1210009445 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3240760119_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m555132503(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1210009445 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3460849589_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m3004320886(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1210009445 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3482199744_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m3109460972(__this, ___value0, method) ((  void (*) (Collection_1_t1210009445 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1739078822_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m129638917(__this, method) ((  bool (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1442644511_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4035616649(__this, method) ((  Il2CppObject * (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1422512927_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3877418662(__this, method) ((  bool (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2968235316_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2248264137(__this, method) ((  bool (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1990189611_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m3571517122(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1210009445 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m75082808_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m3058499495(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1210009445 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m507853765_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Add(T)
#define Collection_1_Add_m2256100392(__this, ___item0, method) ((  void (*) (Collection_1_t1210009445 *, AlternateView_t1668264691 *, const MethodInfo*))Collection_1_Add_m2987402052_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Clear()
#define Collection_1_Clear_m1535235398(__this, method) ((  void (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_Clear_m1596645192_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::ClearItems()
#define Collection_1_ClearItems_m268919558(__this, method) ((  void (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_ClearItems_m1175603758_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Contains(T)
#define Collection_1_Contains_m2524020392(__this, ___item0, method) ((  bool (*) (Collection_1_t1210009445 *, AlternateView_t1668264691 *, const MethodInfo*))Collection_1_Contains_m2116635914_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m2000991818(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1210009445 *, AlternateViewU5BU5D_t354016226*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1578267616_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::GetEnumerator()
#define Collection_1_GetEnumerator_m4285701640(__this, method) ((  Il2CppObject* (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_GetEnumerator_m2963411583_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::IndexOf(T)
#define Collection_1_IndexOf_m3854780816(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1210009445 *, AlternateView_t1668264691 *, const MethodInfo*))Collection_1_IndexOf_m3885709710_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Insert(System.Int32,T)
#define Collection_1_Insert_m1933571070(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1210009445 *, int32_t, AlternateView_t1668264691 *, const MethodInfo*))Collection_1_Insert_m2334889193_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m541709825(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1210009445 *, int32_t, AlternateView_t1668264691 *, const MethodInfo*))Collection_1_InsertItem_m3611385334_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Remove(T)
#define Collection_1_Remove_m3290215722(__this, ___item0, method) ((  bool (*) (Collection_1_t1210009445 *, AlternateView_t1668264691 *, const MethodInfo*))Collection_1_Remove_m452558737_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m3964444523(__this, ___index0, method) ((  void (*) (Collection_1_t1210009445 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1632496813_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m266958097(__this, ___index0, method) ((  void (*) (Collection_1_t1210009445 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4104600353_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::get_Count()
#define Collection_1_get_Count_m2256441363(__this, method) ((  int32_t (*) (Collection_1_t1210009445 *, const MethodInfo*))Collection_1_get_Count_m2250721247_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::get_Item(System.Int32)
#define Collection_1_get_Item_m3783948580(__this, ___index0, method) ((  AlternateView_t1668264691 * (*) (Collection_1_t1210009445 *, int32_t, const MethodInfo*))Collection_1_get_Item_m266052953_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m2922910944(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1210009445 *, int32_t, AlternateView_t1668264691 *, const MethodInfo*))Collection_1_set_Item_m3489932746_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m2457316364(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1210009445 *, int32_t, AlternateView_t1668264691 *, const MethodInfo*))Collection_1_SetItem_m1075410277_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m4037591850(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3443424420_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m431316144(__this /* static, unused */, ___item0, method) ((  AlternateView_t1668264691 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1521356246_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m2771904386(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m215419136_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m2083785288(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m328767958_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m1890349759(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3594284193_gshared)(__this /* static, unused */, ___list0, method)
