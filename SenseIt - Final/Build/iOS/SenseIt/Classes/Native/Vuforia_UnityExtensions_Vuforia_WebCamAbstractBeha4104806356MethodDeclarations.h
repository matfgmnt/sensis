﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t4104806356;
// System.String
struct String_t;
// Vuforia.WebCamImpl
struct WebCamImpl_t2771725761;

// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
extern "C"  String_t* WebCamAbstractBehaviour_get_DeviceName_m3825355463 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
extern "C"  void WebCamAbstractBehaviour_set_DeviceName_m3996503348 (WebCamAbstractBehaviour_t4104806356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
extern "C"  bool WebCamAbstractBehaviour_get_FlipHorizontally_m2125937853 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
extern "C"  void WebCamAbstractBehaviour_set_FlipHorizontally_m2691354538 (WebCamAbstractBehaviour_t4104806356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
extern "C"  bool WebCamAbstractBehaviour_get_TurnOffWebCam_m3286695458 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
extern "C"  void WebCamAbstractBehaviour_set_TurnOffWebCam_m2368175719 (WebCamAbstractBehaviour_t4104806356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
extern "C"  bool WebCamAbstractBehaviour_get_IsPlaying_m2238369263 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
extern "C"  bool WebCamAbstractBehaviour_IsWebCamUsed_m270997834 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
extern "C"  WebCamImpl_t2771725761 * WebCamAbstractBehaviour_get_ImplementationClass_m2095274123 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
extern "C"  void WebCamAbstractBehaviour_InitCamera_m4218926567 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
extern "C"  void WebCamAbstractBehaviour_OnLevelWasLoaded_m1932410509 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
extern "C"  void WebCamAbstractBehaviour_OnDestroy_m3862356247 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
extern "C"  void WebCamAbstractBehaviour_Update_m3178871861 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
extern "C"  void WebCamAbstractBehaviour__ctor_m535376024 (WebCamAbstractBehaviour_t4104806356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
