﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.DatabaseLoadBehaviour
struct DatabaseLoadBehaviour_t450246482;

// System.Void Vuforia.DatabaseLoadBehaviour::.ctor()
extern "C"  void DatabaseLoadBehaviour__ctor_m1834104267 (DatabaseLoadBehaviour_t450246482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C"  void DatabaseLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m528687951 (DatabaseLoadBehaviour_t450246482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
