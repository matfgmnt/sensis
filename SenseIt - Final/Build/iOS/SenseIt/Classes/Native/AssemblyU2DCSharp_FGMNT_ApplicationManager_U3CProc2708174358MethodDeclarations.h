﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// FGMNT.ApplicationManager/<ProcessResults>c__Iterator0
struct U3CProcessResultsU3Ec__Iterator0_t2708174358;
// System.Object
struct Il2CppObject;

// System.Void FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::.ctor()
extern "C"  void U3CProcessResultsU3Ec__Iterator0__ctor_m3699165631 (U3CProcessResultsU3Ec__Iterator0_t2708174358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::MoveNext()
extern "C"  bool U3CProcessResultsU3Ec__Iterator0_MoveNext_m4070560865 (U3CProcessResultsU3Ec__Iterator0_t2708174358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CProcessResultsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2707124977 (U3CProcessResultsU3Ec__Iterator0_t2708174358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CProcessResultsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3670689865 (U3CProcessResultsU3Ec__Iterator0_t2708174358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::Dispose()
extern "C"  void U3CProcessResultsU3Ec__Iterator0_Dispose_m3376231488 (U3CProcessResultsU3Ec__Iterator0_t2708174358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::Reset()
extern "C"  void U3CProcessResultsU3Ec__Iterator0_Reset_m220952322 (U3CProcessResultsU3Ec__Iterator0_t2708174358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FGMNT.ApplicationManager/<ProcessResults>c__Iterator0::<>m__0()
extern "C"  bool U3CProcessResultsU3Ec__Iterator0_U3CU3Em__0_m603330366 (U3CProcessResultsU3Ec__Iterator0_t2708174358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
