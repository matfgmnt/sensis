﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageImpl2564717533.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder2457446201.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetImpl869063580.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTracker1568044035.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl1691118791.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerImpl2101172390.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerTracker2959118800.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerTrackerImpl654952814.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetImpl2151485576.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor2106169489.h"
#include "Vuforia_UnityExtensions_Vuforia_NullWebCamTexAdapt2136158928.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtil1237840826.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtil1550651348.h"
#include "Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor787152098.h"
#include "Vuforia_UnityExtensions_Vuforia_PremiumObjectFacto2032979060.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra1329355276.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3289840897.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1264148721.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1223885651.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3491121689.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3491121690.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3742236631.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1273964084.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3150040852.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1863273727.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2613089982.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2130450083.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1453372369.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2968608989.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl4033919773.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl4188455712.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3635099966.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3090691518.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2337328198.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer2933102835.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Fp1598668988.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4106934884.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4137084396.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec829768013.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi2617831468.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Ren804170727.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRendererImp3510735303.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRendererImp4076072164.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnityImpl149264205.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackabl32254201.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceImpl3646117491.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder447373045.h"
#include "Vuforia_UnityExtensions_Vuforia_PropImpl1187075139.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke1462833936.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke2474837022.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTracker89845299.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl211382711.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD944577254.h"
#include "Vuforia_UnityExtensions_Vuforia_TypeMapping254417876.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptorIm1817875757.h"
#include "Vuforia_UnityExtensions_Vuforia_WordImpl1843145168.h"
#include "Vuforia_UnityExtensions_Vuforia_WordPrefabCreation3171836134.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager1585193471.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManagerImpl4282786523.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResultImpl911273601.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList1278495262.h"
#include "Vuforia_UnityExtensions_Vuforia_WordListImpl2150426444.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNativeIosWr1210651633.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNullWrapper3644069544.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNativeWrapp2645113514.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaWrapper3750170617.h"
#include "Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractB1486411203.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionAbst3509595417.h"
#include "Vuforia_UnityExtensions_Vuforia_PropAbstractBehavi1047177596.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManager3369465942.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManagerImpl3885489748.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder1347637805.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_InitStat4409649.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Updat1473252352.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Filte3082493643.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe1958726506.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl1380851697.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_T3807887646.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_I2369108641.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSource2832298792.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSourceImp2574642394.h"
#include "Vuforia_UnityExtensions_Vuforia_TextureRenderer3312477626.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager308318605.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl381223961.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton3703236737.h"




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (ImageImpl_t2564717533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[10] = 
{
	ImageImpl_t2564717533::get_offset_of_mWidth_0(),
	ImageImpl_t2564717533::get_offset_of_mHeight_1(),
	ImageImpl_t2564717533::get_offset_of_mStride_2(),
	ImageImpl_t2564717533::get_offset_of_mBufferWidth_3(),
	ImageImpl_t2564717533::get_offset_of_mBufferHeight_4(),
	ImageImpl_t2564717533::get_offset_of_mPixelFormat_5(),
	ImageImpl_t2564717533::get_offset_of_mData_6(),
	ImageImpl_t2564717533::get_offset_of_mUnmanagedData_7(),
	ImageImpl_t2564717533::get_offset_of_mDataSet_8(),
	ImageImpl_t2564717533::get_offset_of_mPixel32_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (ImageTargetBuilderImpl_t2457446201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[2] = 
{
	ImageTargetBuilderImpl_t2457446201::get_offset_of_mTrackableSource_0(),
	ImageTargetBuilderImpl_t2457446201::get_offset_of_mIsScanning_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (ImageTargetImpl_t869063580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[2] = 
{
	ImageTargetImpl_t869063580::get_offset_of_mImageTargetType_4(),
	ImageTargetImpl_t869063580::get_offset_of_mVirtualButtons_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (ObjectTracker_t1568044035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (ObjectTrackerImpl_t1691118791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[4] = 
{
	ObjectTrackerImpl_t1691118791::get_offset_of_mActiveDataSets_1(),
	ObjectTrackerImpl_t1691118791::get_offset_of_mDataSets_2(),
	ObjectTrackerImpl_t1691118791::get_offset_of_mImageTargetBuilder_3(),
	ObjectTrackerImpl_t1691118791::get_offset_of_mTargetFinder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (MarkerImpl_t2101172390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[2] = 
{
	MarkerImpl_t2101172390::get_offset_of_mSize_2(),
	MarkerImpl_t2101172390::get_offset_of_U3CMarkerIDU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (MarkerTracker_t2959118800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (MarkerTrackerImpl_t654952814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[1] = 
{
	MarkerTrackerImpl_t654952814::get_offset_of_mMarkerDict_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (MultiTargetImpl_t2151485576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (WebCamTexAdaptor_t2106169489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (NullWebCamTexAdaptor_t2136158928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[5] = 
{
	0,
	NullWebCamTexAdaptor_t2136158928::get_offset_of_mTexture_1(),
	NullWebCamTexAdaptor_t2136158928::get_offset_of_mPseudoPlaying_2(),
	NullWebCamTexAdaptor_t2136158928::get_offset_of_mMsBetweenFrames_3(),
	NullWebCamTexAdaptor_t2136158928::get_offset_of_mLastFrame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (PlayModeEditorUtility_t1237840826), -1, sizeof(PlayModeEditorUtility_t1237840826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2714[1] = 
{
	PlayModeEditorUtility_t1237840826_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (NullPlayModeEditorUtility_t1550651348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (PremiumObjectFactory_t787152098), -1, sizeof(PremiumObjectFactory_t787152098_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2717[1] = 
{
	PremiumObjectFactory_t787152098_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (NullPremiumObjectFactory_t2032979060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (VuforiaManager_t2424874861), -1, sizeof(VuforiaManager_t2424874861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2719[1] = 
{
	VuforiaManager_t2424874861_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (TrackableIdPair_t1329355276)+ sizeof (Il2CppObject), sizeof(TrackableIdPair_t1329355276 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2720[2] = 
{
	TrackableIdPair_t1329355276::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableIdPair_t1329355276::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (VuforiaManagerImpl_t3289840897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[26] = 
{
	VuforiaManagerImpl_t3289840897::get_offset_of_mWorldCenterMode_1(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mWorldCenter_2(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mVuMarkWorldCenter_3(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mARCameraTransform_4(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mCentralAnchorPoint_5(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mParentAnchorPoint_6(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mTrackableResultDataArray_7(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mWordDataArray_8(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mWordResultDataArray_9(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mVuMarkDataArray_10(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mVuMarkResultDataArray_11(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mTrackableFoundQueue_12(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mImageHeaderData_13(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mNumImageHeaders_14(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mInjectedFrameIdx_15(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mLastProcessedFrameStatePtr_16(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mInitialized_17(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mPaused_18(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mFrameState_19(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mAutoRotationState_20(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mVideoBackgroundNeedsRedrawing_21(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mDiscardStatesForRendering_22(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mVideoBackgroundMgr_23(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mLastFrameIdx_24(),
	VuforiaManagerImpl_t3289840897::get_offset_of_mIsSeeThroughDevice_25(),
	VuforiaManagerImpl_t3289840897::get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (PoseData_t1264148721)+ sizeof (Il2CppObject), sizeof(PoseData_t1264148721 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2722[3] = 
{
	PoseData_t1264148721::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseData_t1264148721::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseData_t1264148721::get_offset_of_csInteger_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (TrackableResultData_t1947527974)+ sizeof (Il2CppObject), sizeof(TrackableResultData_t1947527974 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2723[4] = 
{
	TrackableResultData_t1947527974::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t1947527974::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t1947527974::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t1947527974::get_offset_of_id_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (VirtualButtonData_t1223885651)+ sizeof (Il2CppObject), sizeof(VirtualButtonData_t1223885651 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2724[2] = 
{
	VirtualButtonData_t1223885651::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VirtualButtonData_t1223885651::get_offset_of_isPressed_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (Obb2D_t3491121689)+ sizeof (Il2CppObject), sizeof(Obb2D_t3491121689 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2725[4] = 
{
	Obb2D_t3491121689::get_offset_of_center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t3491121689::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t3491121689::get_offset_of_rotation_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t3491121689::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (Obb3D_t3491121690)+ sizeof (Il2CppObject), sizeof(Obb3D_t3491121690 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2726[4] = 
{
	Obb3D_t3491121690::get_offset_of_center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t3491121690::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t3491121690::get_offset_of_rotationZ_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t3491121690::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (WordResultData_t3742236631)+ sizeof (Il2CppObject), sizeof(WordResultData_t3742236631 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2727[5] = 
{
	WordResultData_t3742236631::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t3742236631::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t3742236631::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t3742236631::get_offset_of_id_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t3742236631::get_offset_of_orientedBoundingBox_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (WordData_t1273964084)+ sizeof (Il2CppObject), sizeof(WordData_t1273964084 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2728[4] = 
{
	WordData_t1273964084::get_offset_of_stringValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t1273964084::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t1273964084::get_offset_of_size_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t1273964084::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (ImageHeaderData_t3150040852)+ sizeof (Il2CppObject), sizeof(ImageHeaderData_t3150040852 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2729[9] = 
{
	ImageHeaderData_t3150040852::get_offset_of_data_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_width_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_height_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_stride_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_bufferWidth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_bufferHeight_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_format_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_reallocate_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t3150040852::get_offset_of_updated_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (MeshData_t1863273727)+ sizeof (Il2CppObject), sizeof(MeshData_t1863273727 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2730[8] = 
{
	MeshData_t1863273727::get_offset_of_positionsArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_normalsArray_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_texCoordsArray_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_triangleIdxArray_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_numVertexValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_hasNormals_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_hasTexCoords_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1863273727::get_offset_of_numTriangleIndices_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (InstanceIdData_t2613089982)+ sizeof (Il2CppObject), sizeof(InstanceIdData_t2613089982 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2731[5] = 
{
	InstanceIdData_t2613089982::get_offset_of_numericValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InstanceIdData_t2613089982::get_offset_of_buffer_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InstanceIdData_t2613089982::get_offset_of_reserved_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InstanceIdData_t2613089982::get_offset_of_dataLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InstanceIdData_t2613089982::get_offset_of_dataType_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (VuMarkTargetData_t2130450083)+ sizeof (Il2CppObject), sizeof(VuMarkTargetData_t2130450083 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2732[5] = 
{
	VuMarkTargetData_t2130450083::get_offset_of_instanceId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetData_t2130450083::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetData_t2130450083::get_offset_of_templateId_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetData_t2130450083::get_offset_of_size_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetData_t2130450083::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (SmartTerrainRevisionData_t1453372369)+ sizeof (Il2CppObject), sizeof(SmartTerrainRevisionData_t1453372369 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2733[2] = 
{
	SmartTerrainRevisionData_t1453372369::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SmartTerrainRevisionData_t1453372369::get_offset_of_revision_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (SurfaceData_t2968608989)+ sizeof (Il2CppObject), sizeof(SurfaceData_t2968608989 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2734[9] = 
{
	SurfaceData_t2968608989::get_offset_of_meshBoundaryArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_meshData_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_navMeshData_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_localPose_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_id_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_parentID_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_numBoundaryIndices_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t2968608989::get_offset_of_revision_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (PropData_t4033919773)+ sizeof (Il2CppObject), sizeof(PropData_t4033919773 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2735[8] = 
{
	PropData_t4033919773::get_offset_of_meshData_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_parentID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_localPosition_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_localPose_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_revision_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t4033919773::get_offset_of_unused_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (VuMarkTargetResultData_t4188455712)+ sizeof (Il2CppObject), sizeof(VuMarkTargetResultData_t4188455712 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2736[6] = 
{
	VuMarkTargetResultData_t4188455712::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t4188455712::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t4188455712::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t4188455712::get_offset_of_targetID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t4188455712::get_offset_of_resultID_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t4188455712::get_offset_of_unused_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (FrameState_t3635099966)+ sizeof (Il2CppObject), sizeof(FrameState_t3635099966 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2737[22] = 
{
	FrameState_t3635099966::get_offset_of_trackableDataArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_vbDataArray_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_wordResultArray_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_newWordDataArray_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_vuMarkResultArray_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_newVuMarkDataArray_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_propTrackableDataArray_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_smartTerrainRevisionsArray_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_updatedSurfacesArray_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_updatedPropsArray_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numTrackableResults_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numVirtualButtonResults_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_frameIndex_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numWordResults_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numNewWords_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numVuMarkResults_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numNewVuMarks_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numPropTrackableResults_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numSmartTerrainRevisions_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numUpdatedSurfaces_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_numUpdatedProps_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t3635099966::get_offset_of_deviceTrackableId_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (AutoRotationState_t3090691518)+ sizeof (Il2CppObject), sizeof(AutoRotationState_t3090691518_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2738[5] = 
{
	AutoRotationState_t3090691518::get_offset_of_setOnPause_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToPortrait_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToPortraitUpsideDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToLandscapeLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToLandscapeRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (U3CU3Ec__DisplayClass9_t2337328198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[1] = 
{
	U3CU3Ec__DisplayClass9_t2337328198::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (VuforiaRenderer_t2933102835), -1, sizeof(VuforiaRenderer_t2933102835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2740[1] = 
{
	VuforiaRenderer_t2933102835_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (FpsHint_t1598668988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2741[5] = 
{
	FpsHint_t1598668988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (VideoBackgroundReflection_t4106934884)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2742[4] = 
{
	VideoBackgroundReflection_t4106934884::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (VideoBGCfgData_t4137084396)+ sizeof (Il2CppObject), sizeof(VideoBGCfgData_t4137084396 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2743[4] = 
{
	VideoBGCfgData_t4137084396::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_reflectionInteger_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (Vec2I_t829768013)+ sizeof (Il2CppObject), sizeof(Vec2I_t829768013 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2744[2] = 
{
	Vec2I_t829768013::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vec2I_t829768013::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (VideoTextureInfo_t2617831468)+ sizeof (Il2CppObject), sizeof(VideoTextureInfo_t2617831468 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2745[2] = 
{
	VideoTextureInfo_t2617831468::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoTextureInfo_t2617831468::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (RendererAPI_t804170727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2746[4] = 
{
	RendererAPI_t804170727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (VuforiaRendererImpl_t3510735303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[6] = 
{
	VuforiaRendererImpl_t3510735303::get_offset_of_mVideoBGConfig_1(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mVideoBGConfigSet_2(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mVideoBackgroundTexture_3(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mBackgroundTextureHasChanged_4(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mLastSetReflection_5(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mNativeRenderingCallback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (RenderEvent_t4076072164)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2748[7] = 
{
	RenderEvent_t4076072164::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (VuforiaUnityImpl_t149264205), -1, sizeof(VuforiaUnityImpl_t149264205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2749[1] = 
{
	VuforiaUnityImpl_t149264205_StaticFields::get_offset_of_mRendererDirty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (SmartTerrainTrackableImpl_t32254201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[5] = 
{
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mChildren_2(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mMesh_3(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mMeshRevision_4(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mLocalPose_5(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_U3CParentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (SurfaceImpl_t3646117491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[5] = 
{
	SurfaceImpl_t3646117491::get_offset_of_mNavMesh_7(),
	SurfaceImpl_t3646117491::get_offset_of_mMeshBoundaries_8(),
	SurfaceImpl_t3646117491::get_offset_of_mBoundingBox_9(),
	SurfaceImpl_t3646117491::get_offset_of_mSurfaceArea_10(),
	SurfaceImpl_t3646117491::get_offset_of_mAreaNeedsUpdate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (SmartTerrainBuilderImpl_t447373045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[2] = 
{
	SmartTerrainBuilderImpl_t447373045::get_offset_of_mReconstructionBehaviours_0(),
	SmartTerrainBuilderImpl_t447373045::get_offset_of_mIsInitialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (PropImpl_t1187075139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[1] = 
{
	PropImpl_t1187075139::get_offset_of_mOrientedBoundingBox3D_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (SmartTerrainTracker_t1462833936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (SmartTerrainTrackerImpl_t2474837022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[2] = 
{
	SmartTerrainTrackerImpl_t2474837022::get_offset_of_mScaleToMillimeter_1(),
	SmartTerrainTrackerImpl_t2474837022::get_offset_of_mSmartTerrainBuilder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (TextTracker_t89845299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (TextTrackerImpl_t211382711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[1] = 
{
	TextTrackerImpl_t211382711::get_offset_of_mWordList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (UpDirection_t944577254)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2761[5] = 
{
	UpDirection_t944577254::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (TypeMapping_t254417876), -1, sizeof(TypeMapping_t254417876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2762[1] = 
{
	TypeMapping_t254417876_StaticFields::get_offset_of_sTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (WebCamTexAdaptorImpl_t1817875757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[2] = 
{
	WebCamTexAdaptorImpl_t1817875757::get_offset_of_mWebCamTexture_0(),
	WebCamTexAdaptorImpl_t1817875757::get_offset_of_mCheckCameraPermissions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (WordImpl_t1843145168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[5] = 
{
	WordImpl_t1843145168::get_offset_of_mText_2(),
	WordImpl_t1843145168::get_offset_of_mSize_3(),
	WordImpl_t1843145168::get_offset_of_mLetterMask_4(),
	WordImpl_t1843145168::get_offset_of_mLetterImageHeader_5(),
	WordImpl_t1843145168::get_offset_of_mLetterBoundingBoxes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (WordPrefabCreationMode_t3171836134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2766[3] = 
{
	WordPrefabCreationMode_t3171836134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (WordManager_t1585193471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (WordManagerImpl_t4282786523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[12] = 
{
	0,
	WordManagerImpl_t4282786523::get_offset_of_mTrackedWords_1(),
	WordManagerImpl_t4282786523::get_offset_of_mNewWords_2(),
	WordManagerImpl_t4282786523::get_offset_of_mLostWords_3(),
	WordManagerImpl_t4282786523::get_offset_of_mActiveWordBehaviours_4(),
	WordManagerImpl_t4282786523::get_offset_of_mWordBehavioursMarkedForDeletion_5(),
	WordManagerImpl_t4282786523::get_offset_of_mWaitingQueue_6(),
	WordManagerImpl_t4282786523::get_offset_of_mWordBehaviours_7(),
	WordManagerImpl_t4282786523::get_offset_of_mAutomaticTemplate_8(),
	WordManagerImpl_t4282786523::get_offset_of_mMaxInstances_9(),
	WordManagerImpl_t4282786523::get_offset_of_mWordPrefabCreationMode_10(),
	WordManagerImpl_t4282786523::get_offset_of_mVuforiaBehaviour_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (WordResult_t1915507197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (WordResultImpl_t911273601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2770[5] = 
{
	WordResultImpl_t911273601::get_offset_of_mObb_0(),
	WordResultImpl_t911273601::get_offset_of_mPosition_1(),
	WordResultImpl_t911273601::get_offset_of_mOrientation_2(),
	WordResultImpl_t911273601::get_offset_of_mWord_3(),
	WordResultImpl_t911273601::get_offset_of_mStatus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (WordList_t1278495262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (WordListImpl_t2150426444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (VuforiaNativeIosWrapper_t1210651633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (VuforiaNullWrapper_t3644069544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (VuforiaNativeWrapper_t2645113514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (VuforiaWrapper_t3750170617), -1, sizeof(VuforiaWrapper_t3750170617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2777[2] = 
{
	VuforiaWrapper_t3750170617_StaticFields::get_offset_of_sWrapper_0(),
	VuforiaWrapper_t3750170617_StaticFields::get_offset_of_sCamIndependentWrapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (KeepAliveAbstractBehaviour_t1486411203), -1, sizeof(KeepAliveAbstractBehaviour_t1486411203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2781[8] = 
{
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepARCameraAlive_2(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepTrackableBehavioursAlive_3(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepTextRecoBehaviourAlive_4(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepUDTBuildingBehaviourAlive_5(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepCloudRecoBehaviourAlive_6(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mKeepSmartTerrainAlive_7(),
	KeepAliveAbstractBehaviour_t1486411203_StaticFields::get_offset_of_sKeepAliveBehaviour_8(),
	KeepAliveAbstractBehaviour_t1486411203::get_offset_of_mHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (ReconstructionAbstractBehaviour_t3509595417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[22] = 
{
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mHasInitialized_2(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mSmartTerrainEventHandlers_3(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnInitialized_4(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropCreated_5(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropUpdated_6(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropDeleted_7(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceCreated_8(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceUpdated_9(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceDeleted_10(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mInitializedInEditor_11(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mMaximumExtentEnabled_12(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mMaximumExtent_13(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mAutomaticStart_14(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mNavMeshUpdates_15(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mNavMeshPadding_16(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mReconstruction_17(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mSurfaces_18(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mActiveSurfaceBehaviours_19(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mProps_20(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mActivePropBehaviours_21(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mIgnoreNextUpdate_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (PropAbstractBehaviour_t1047177596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[2] = 
{
	PropAbstractBehaviour_t1047177596::get_offset_of_mProp_14(),
	PropAbstractBehaviour_t1047177596::get_offset_of_mBoxColliderToUpdate_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (StateManager_t3369465942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (StateManagerImpl_t3885489748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[9] = 
{
	StateManagerImpl_t3885489748::get_offset_of_mTrackableBehaviours_0(),
	StateManagerImpl_t3885489748::get_offset_of_mAutomaticallyCreatedBehaviours_1(),
	StateManagerImpl_t3885489748::get_offset_of_mBehavioursMarkedForDeletion_2(),
	StateManagerImpl_t3885489748::get_offset_of_mActiveTrackableBehaviours_3(),
	StateManagerImpl_t3885489748::get_offset_of_mWordManager_4(),
	StateManagerImpl_t3885489748::get_offset_of_mVuMarkManager_5(),
	StateManagerImpl_t3885489748::get_offset_of_mDeviceTrackingManager_6(),
	StateManagerImpl_t3885489748::get_offset_of_mCameraPositioningHelper_7(),
	StateManagerImpl_t3885489748::get_offset_of_mExtendedTrackingManager_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (TargetFinder_t1347637805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (InitState_t4409649)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2787[6] = 
{
	InitState_t4409649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (UpdateState_t1473252352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2788[12] = 
{
	UpdateState_t1473252352::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (FilterMode_t3082493643)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2789[3] = 
{
	FilterMode_t3082493643::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (TargetSearchResult_t1958726506)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t1958726506_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2790[6] = 
{
	TargetSearchResult_t1958726506::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (TargetFinderImpl_t1380851697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[4] = 
{
	TargetFinderImpl_t1380851697::get_offset_of_mTargetFinderStatePtr_0(),
	TargetFinderImpl_t1380851697::get_offset_of_mTargetFinderState_1(),
	TargetFinderImpl_t1380851697::get_offset_of_mNewResults_2(),
	TargetFinderImpl_t1380851697::get_offset_of_mImageTargets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (TargetFinderState_t3807887646)+ sizeof (Il2CppObject), sizeof(TargetFinderState_t3807887646 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2792[4] = 
{
	TargetFinderState_t3807887646::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_ResultCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (InternalTargetSearchResult_t2369108641)+ sizeof (Il2CppObject), sizeof(InternalTargetSearchResult_t2369108641 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2793[6] = 
{
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (TrackableSource_t2832298792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (TrackableSourceImpl_t2574642394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[1] = 
{
	TrackableSourceImpl_t2574642394::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (TextureRenderer_t3312477626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[3] = 
{
	TextureRenderer_t3312477626::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t3312477626::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t3312477626::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (TrackerManager_t308318605), -1, sizeof(TrackerManager_t308318605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2797[1] = 
{
	TrackerManager_t308318605_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (TrackerManagerImpl_t381223961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[6] = 
{
	TrackerManagerImpl_t381223961::get_offset_of_mObjectTracker_1(),
	TrackerManagerImpl_t381223961::get_offset_of_mMarkerTracker_2(),
	TrackerManagerImpl_t381223961::get_offset_of_mTextTracker_3(),
	TrackerManagerImpl_t381223961::get_offset_of_mSmartTerrainTracker_4(),
	TrackerManagerImpl_t381223961::get_offset_of_mDeviceTracker_5(),
	TrackerManagerImpl_t381223961::get_offset_of_mStateManager_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (VirtualButton_t3703236737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[1] = 
{
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
