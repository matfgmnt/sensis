﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// System.Net.Mail.SmtpClient/CancellationException
struct CancellationException_t544132923;

// System.Void System.Net.Mail.SmtpClient/CancellationException::.ctor()
extern "C"  void CancellationException__ctor_m992419999 (CancellationException_t544132923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
