﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// FGMNT.CameraAutoFocus
struct CameraAutoFocus_t117912794;

// System.Void FGMNT.CameraAutoFocus::.ctor()
extern "C"  void CameraAutoFocus__ctor_m524826241 (CameraAutoFocus_t117912794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.CameraAutoFocus::Start()
extern "C"  void CameraAutoFocus_Start_m3836661561 (CameraAutoFocus_t117912794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.CameraAutoFocus::OnVuforiaStarted()
extern "C"  void CameraAutoFocus_OnVuforiaStarted_m958582617 (CameraAutoFocus_t117912794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.CameraAutoFocus::OnPaused(System.Boolean)
extern "C"  void CameraAutoFocus_OnPaused_m242491365 (CameraAutoFocus_t117912794 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
