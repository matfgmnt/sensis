﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_MarkerAbstractBeha1456101953.h"


#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MarkerBehaviour
struct  MarkerBehaviour_t1265232977  : public MarkerAbstractBehaviour_t1456101953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
