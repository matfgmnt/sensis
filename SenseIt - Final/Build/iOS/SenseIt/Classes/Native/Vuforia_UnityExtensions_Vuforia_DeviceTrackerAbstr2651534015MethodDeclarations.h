﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// Vuforia.DeviceTrackerAbstractBehaviour
struct DeviceTrackerAbstractBehaviour_t2651534015;
// System.Action
struct Action_t3226471752;

// System.Void Vuforia.DeviceTrackerAbstractBehaviour::Awake()
extern "C"  void DeviceTrackerAbstractBehaviour_Awake_m534170446 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::OnEnable()
extern "C"  void DeviceTrackerAbstractBehaviour_OnEnable_m3738847311 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::OnDisable()
extern "C"  void DeviceTrackerAbstractBehaviour_OnDisable_m1546378426 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::OnDestroy()
extern "C"  void DeviceTrackerAbstractBehaviour_OnDestroy_m322086996 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::Update()
extern "C"  void DeviceTrackerAbstractBehaviour_Update_m2554596380 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
extern "C"  void DeviceTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m104623459 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
extern "C"  void DeviceTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m3671878796 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::RegisterBeforeDevicePoseUpdateCallback(System.Action)
extern "C"  void DeviceTrackerAbstractBehaviour_RegisterBeforeDevicePoseUpdateCallback_m1614607475 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::UnregisterBeforeDevicePoseUpdateCallback(System.Action)
extern "C"  void DeviceTrackerAbstractBehaviour_UnregisterBeforeDevicePoseUpdateCallback_m793947560 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::RegisterDevicePoseUpdatedCallback(System.Action)
extern "C"  void DeviceTrackerAbstractBehaviour_RegisterDevicePoseUpdatedCallback_m4124773350 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::UnregisterDevicePoseUpdatedCallback(System.Action)
extern "C"  void DeviceTrackerAbstractBehaviour_UnregisterDevicePoseUpdatedCallback_m3264243337 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, Action_t3226471752 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::StartDeviceTracker()
extern "C"  void DeviceTrackerAbstractBehaviour_StartDeviceTracker_m4265475823 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::StopDeviceTracker()
extern "C"  void DeviceTrackerAbstractBehaviour_StopDeviceTracker_m341136931 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::InitDeviceTracker()
extern "C"  void DeviceTrackerAbstractBehaviour_InitDeviceTracker_m162413981 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::ApplySettings()
extern "C"  void DeviceTrackerAbstractBehaviour_ApplySettings_m3541681372 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::OnVuforiaInitialized()
extern "C"  void DeviceTrackerAbstractBehaviour_OnVuforiaInitialized_m2370181056 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::OnVuforiaStarted()
extern "C"  void DeviceTrackerAbstractBehaviour_OnVuforiaStarted_m3018409807 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::OnPause(System.Boolean)
extern "C"  void DeviceTrackerAbstractBehaviour_OnPause_m3914159427 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::.ctor()
extern "C"  void DeviceTrackerAbstractBehaviour__ctor_m2417598235 (DeviceTrackerAbstractBehaviour_t2651534015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DeviceTrackerAbstractBehaviour::.cctor()
extern "C"  void DeviceTrackerAbstractBehaviour__cctor_m3990242760 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
