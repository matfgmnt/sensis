﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton_Sens1678924861.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonImpl2449737797.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamImpl2771725761.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile3757625748.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof3644865120.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetAbstrac3327552701.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerAbstractBeha1456101953.h"
#include "Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeh3489038957.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac3616801211.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity657456673.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE2149396216.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Vufor3491240575.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Stora3897282321.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3132552034.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaMacros1884408435.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3083157244.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil1916387570.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceUtilities4096327849.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773.h"
#include "Vuforia_UnityExtensions_Vuforia_SimpleTargetData3993525265.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705.h"
#include "Vuforia_UnityExtensions_Vuforia_UserDefinedTargetB3589690572.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst395384314.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundMan3765780423.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamAbstractBeha4104806356.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe1020506756.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDet776153458.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_CameraSettings3536359094.h"
#include "AssemblyU2DCSharp_CameraSettings_U3CRestoreOrigina3562025758.h"
#include "AssemblyU2DCSharp_InitErrorHandler1791388012.h"
#include "AssemblyU2DCSharp_MenuAnimator2049002970.h"
#include "AssemblyU2DCSharp_MenuOptions3210604277.h"
#include "AssemblyU2DCSharp_AboutScreen3562380015.h"
#include "AssemblyU2DCSharp_AsyncSceneLoader2733707743.h"
#include "AssemblyU2DCSharp_AsyncSceneLoader_U3CLoadNextScen1824579776.h"
#include "AssemblyU2DCSharp_LoadingScreen2880017196.h"
#include "AssemblyU2DCSharp_TapHandler3409799063.h"
#include "AssemblyU2DCSharp_TrackableSettings4265251850.h"
#include "AssemblyU2DCSharp_ObbComparison4173085576.h"
#include "AssemblyU2DCSharp_TextEventHandler4116109227.h"
#include "AssemblyU2DCSharp_FGMNT_ApplicationGUIManager235235642.h"
#include "AssemblyU2DCSharp_FGMNT_ApplicationManager2067665347.h"
#include "AssemblyU2DCSharp_FGMNT_ApplicationManager_U3CProc2708174358.h"
#include "AssemblyU2DCSharp_FGMNT_CameraAutoFocus117912794.h"
#include "AssemblyU2DCSharp_FGMNT_ContactManager2312442069.h"
#include "AssemblyU2DCSharp_FGMNT_ContactManager_U3CExecuteAc383275672.h"
#include "AssemblyU2DCSharp_FGMNT_DatabaseEntry2505461191.h"
#include "AssemblyU2DCSharp_FGMNT_DatabaseManager4081956334.h"
#include "AssemblyU2DCSharp_FGMNT_FeedbackManager2969052828.h"
#include "AssemblyU2DCSharp_FGMNT_FeedbackManagerOld108831159.h"
#include "AssemblyU2DCSharp_FGMNT_LocationManager1546885680.h"
#include "AssemblyU2DCSharp_FGMNT_LocationManager_U3CStartU3Ec73113186.h"
#include "AssemblyU2DCSharp_FGMNT_NewCloudHandler3451870399.h"
#include "AssemblyU2DCSharp_FGMNT_NewTextRecoHandler527272212.h"
#include "AssemblyU2DCSharp_ScreenshotManager2800354649.h"
#include "AssemblyU2DCSharp_TextureScale2270433493.h"
#include "AssemblyU2DCSharp_TextureScale_ThreadData1483341464.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour450246482.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726.h"
#include "AssemblyU2DCSharp_Vuforia_DeviceTrackerBehaviour1884988499.h"
#include "AssemblyU2DCSharp_Vuforia_DigitalEyewearBehaviour495394589.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028.h"
#include "AssemblyU2DCSharp_Vuforia_WSAUnityPlayer425981959.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour1532923751.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour1265232977.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour966064926.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour4009935945.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTarget2111803406.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehav3844158157.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour2405314212.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour584991835.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4184040062.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper556656694.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3161817952.h"




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (Sensitivity_t1678924861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2800[4] = 
{
	Sensitivity_t1678924861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (VirtualButtonImpl_t2449737797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[6] = 
{
	VirtualButtonImpl_t2449737797::get_offset_of_mName_1(),
	VirtualButtonImpl_t2449737797::get_offset_of_mID_2(),
	VirtualButtonImpl_t2449737797::get_offset_of_mArea_3(),
	VirtualButtonImpl_t2449737797::get_offset_of_mIsEnabled_4(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentImageTarget_5(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentDataSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (WebCamImpl_t2771725761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[15] = 
{
	WebCamImpl_t2771725761::get_offset_of_mARCameras_0(),
	WebCamImpl_t2771725761::get_offset_of_mOriginalCameraCullMask_1(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamTexture_2(),
	WebCamImpl_t2771725761::get_offset_of_mVideoModeData_3(),
	WebCamImpl_t2771725761::get_offset_of_mVideoTextureInfo_4(),
	WebCamImpl_t2771725761::get_offset_of_mTextureRenderer_5(),
	WebCamImpl_t2771725761::get_offset_of_mBufferReadTexture_6(),
	WebCamImpl_t2771725761::get_offset_of_mReadPixelsRect_7(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamProfile_8(),
	WebCamImpl_t2771725761::get_offset_of_mFlipHorizontally_9(),
	WebCamImpl_t2771725761::get_offset_of_mIsDirty_10(),
	WebCamImpl_t2771725761::get_offset_of_mLastFrameIdx_11(),
	WebCamImpl_t2771725761::get_offset_of_mRenderTextureLayer_12(),
	WebCamImpl_t2771725761::get_offset_of_mWebcamPlaying_13(),
	WebCamImpl_t2771725761::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (WebCamProfile_t3757625748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[1] = 
{
	WebCamProfile_t3757625748::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (ProfileData_t1724666488)+ sizeof (Il2CppObject), sizeof(ProfileData_t1724666488 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2804[3] = 
{
	ProfileData_t1724666488::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (ProfileCollection_t3644865120)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[2] = 
{
	ProfileCollection_t3644865120::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileCollection_t3644865120::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (ImageTargetAbstractBehaviour_t3327552701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[8] = 
{
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mAspectRatio_20(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTargetType_21(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mWidth_22(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mHeight_23(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTarget_24(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mVirtualButtonBehaviours_25(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mLastTransformScale_26(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mLastSize_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (MarkerAbstractBehaviour_t1456101953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[2] = 
{
	MarkerAbstractBehaviour_t1456101953::get_offset_of_mMarkerID_10(),
	MarkerAbstractBehaviour_t1456101953::get_offset_of_mMarker_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (MaskOutAbstractBehaviour_t3489038957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[1] = 
{
	MaskOutAbstractBehaviour_t3489038957::get_offset_of_maskMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (MultiTargetAbstractBehaviour_t3616801211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[1] = 
{
	MultiTargetAbstractBehaviour_t3616801211::get_offset_of_mMultiTarget_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (VuforiaUnity_t657456673), -1, sizeof(VuforiaUnity_t657456673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2813[1] = 
{
	VuforiaUnity_t657456673_StaticFields::get_offset_of_mHoloLensApiAbstraction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (InitError_t2149396216)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2814[12] = 
{
	InitError_t2149396216::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (VuforiaHint_t3491240575)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2815[4] = 
{
	VuforiaHint_t3491240575::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (StorageType_t3897282321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2816[4] = 
{
	StorageType_t3897282321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (VuforiaAbstractBehaviour_t3319870759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[42] = 
{
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_VuforiaLicenseKey_2(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_CameraDeviceModeSetting_3(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_MaxSimultaneousImageTargets_4(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_MaxSimultaneousObjectTargets_5(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_UseDelayedLoadingObjectTargets_6(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_CameraDirection_7(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_MirrorVideoBackground_8(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mWorldCenterMode_9(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mWorldCenter_10(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mTrackerEventHandlers_11(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mVideoBgEventHandlers_12(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnVuforiaInitError_13(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnVuforiaInitialized_14(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnVuforiaStarted_15(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnTrackablesUpdated_16(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mRenderOnUpdate_17(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnPause_18(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mPaused_19(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mOnBackgroundTextureChanged_20(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mStartHasBeenInvoked_21(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mHasStarted_22(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mFailedToInitialize_23(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mBackgroundTextureHasChanged_24(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mInitError_25(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mCameraConfiguration_26(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mEyewearBehaviour_27(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mVideoBackgroundMgr_28(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mCheckStopCamera_29(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mClearMaterial_30(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMetalRendering_31(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mHasStartedOnce_32(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mWasEnabledBeforePause_33(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mObjectTrackerWasActiveBeforePause_34(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_35(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMarkerTrackerWasActiveBeforePause_36(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMarkerTrackerWasActiveBeforeDisabling_37(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mLastUpdatedFrame_38(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mTrackersRequestedToDeinit_39(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMissedToApplyLeftProjectionMatrix_40(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mMissedToApplyRightProjectionMatrix_41(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mLeftProjectMatrixToApply_42(),
	VuforiaAbstractBehaviour_t3319870759::get_offset_of_mRightProjectMatrixToApply_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (WorldCenterMode_t3132552034)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2818[5] = 
{
	WorldCenterMode_t3132552034::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (VuforiaMacros_t1884408435)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t1884408435 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2819[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (VuforiaRuntimeUtilities_t3083157244), -1, sizeof(VuforiaRuntimeUtilities_t3083157244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2820[2] = 
{
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (InitializableBool_t1916387570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2821[4] = 
{
	InitializableBool_t1916387570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (SurfaceUtilities_t4096327849), -1, sizeof(SurfaceUtilities_t4096327849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2822[1] = 
{
	SurfaceUtilities_t4096327849_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (TextRecoAbstractBehaviour_t2386081773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[12] = 
{
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (SimpleTargetData_t3993525265)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t3993525265 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2824[2] = 
{
	SimpleTargetData_t3993525265::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t3993525265::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (TurnOffAbstractBehaviour_t4084926705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t3589690572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (VideoBackgroundAbstractBehaviour_t395384314), -1, sizeof(VideoBackgroundAbstractBehaviour_t395384314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2827[12] = 
{
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaAbstractBehaviour_4(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mResetMatrix_10(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaFrustumSkew_11(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCenterToEyeAxis_12(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mDisabledMeshRenderers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (VideoBackgroundManagerAbstractBehaviour_t3765780423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[7] = 
{
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mTexture_2(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mVideoBgConfigChanged_3(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mNativeTexturePtr_4(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mClippingMode_5(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mMatteShader_6(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mVideoBackgroundEnabled_7(),
	VideoBackgroundManagerAbstractBehaviour_t3765780423::get_offset_of_mVuforiaBehaviour_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (VirtualButtonAbstractBehaviour_t2478279366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[15] = 
{
	0,
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviousSensitivity_9(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviouslyEnabled_10(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPressed_11(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHandlers_12(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mLeftTop_13(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mRightBottom_14(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mUnregisterOnDestroy_15(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mVirtualButton_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (WebCamAbstractBehaviour_t4104806356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[6] = 
{
	WebCamAbstractBehaviour_t4104806356::get_offset_of_RenderTextureLayer_2(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mDeviceNameSetInEditor_3(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mFlipHorizontally_4(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mTurnOffWebCam_5(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mWebCamImpl_6(),
	WebCamAbstractBehaviour_t4104806356::get_offset_of_mBackgroundCameraInstance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (WordTemplateMode_t1097144495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2831[3] = 
{
	WordTemplateMode_t1097144495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (WordAbstractBehaviour_t2878458725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[3] = 
{
	WordAbstractBehaviour_t2878458725::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (WordFilterMode_t695600879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2833[4] = 
{
	WordFilterMode_t695600879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (U3CPrivateImplementationDetailsU3EU7BBAB5198FU2DE997U2D40F1U2D8D76U2D18FD8DEB376EU7D_t1020506756), -1, sizeof(U3CPrivateImplementationDetailsU3EU7BBAB5198FU2DE997U2D40F1U2D8D76U2D18FD8DEB376EU7D_t1020506756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2834[1] = 
{
	U3CPrivateImplementationDetailsU3EU7BBAB5198FU2DE997U2D40F1U2D8D76U2D18FD8DEB376EU7D_t1020506756_StaticFields::get_offset_of_U24U24method0x6000bfcU2D1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (__StaticArrayInitTypeSizeU3D24_t776153458)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t776153458 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (CameraSettings_t3536359094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[4] = 
{
	CameraSettings_t3536359094::get_offset_of_mVuforiaStarted_2(),
	CameraSettings_t3536359094::get_offset_of_mAutofocusEnabled_3(),
	CameraSettings_t3536359094::get_offset_of_mFlashTorchEnabled_4(),
	CameraSettings_t3536359094::get_offset_of_mActiveDirection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[4] = 
{
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_U24this_0(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_U24current_1(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_U24disposing_2(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (InitErrorHandler_t1791388012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[2] = 
{
	InitErrorHandler_t1791388012::get_offset_of_errorText_2(),
	InitErrorHandler_t1791388012::get_offset_of_errorCanvas_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (MenuAnimator_t2049002970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[7] = 
{
	MenuAnimator_t2049002970::get_offset_of_mVisiblePos_2(),
	MenuAnimator_t2049002970::get_offset_of_mInvisiblePos_3(),
	MenuAnimator_t2049002970::get_offset_of_mVisibility_4(),
	MenuAnimator_t2049002970::get_offset_of_mVisible_5(),
	MenuAnimator_t2049002970::get_offset_of_mCanvas_6(),
	MenuAnimator_t2049002970::get_offset_of_mMenuOptions_7(),
	MenuAnimator_t2049002970::get_offset_of_SlidingTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (MenuOptions_t3210604277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[3] = 
{
	MenuOptions_t3210604277::get_offset_of_mCamSettings_2(),
	MenuOptions_t3210604277::get_offset_of_mTrackableSettings_3(),
	MenuOptions_t3210604277::get_offset_of_mMenuAnim_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (AboutScreen_t3562380015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (AsyncSceneLoader_t2733707743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[1] = 
{
	AsyncSceneLoader_t2733707743::get_offset_of_loadingDelay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[4] = 
{
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_seconds_0(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_U24current_1(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_U24disposing_2(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (LoadingScreen_t2880017196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[2] = 
{
	LoadingScreen_t2880017196::get_offset_of_mChangeLevel_2(),
	LoadingScreen_t2880017196::get_offset_of_mUISpinner_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (TapHandler_t3409799063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[4] = 
{
	0,
	TapHandler_t3409799063::get_offset_of_mTimeSinceLastTap_3(),
	TapHandler_t3409799063::get_offset_of_mMenuAnim_4(),
	TapHandler_t3409799063::get_offset_of_mTapCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (TrackableSettings_t4265251850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[1] = 
{
	TrackableSettings_t4265251850::get_offset_of_mExtTrackingEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (ObbComparison_t4173085576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (TextEventHandler_t4116109227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2849[14] = 
{
	TextEventHandler_t4116109227::get_offset_of_mLoupeWidth_2(),
	TextEventHandler_t4116109227::get_offset_of_mLoupeHeight_3(),
	TextEventHandler_t4116109227::get_offset_of_mBBoxLineWidth_4(),
	TextEventHandler_t4116109227::get_offset_of_mBBoxPadding_5(),
	TextEventHandler_t4116109227::get_offset_of_mBBoxColor_6(),
	TextEventHandler_t4116109227::get_offset_of_mDetectionAndTrackingRect_7(),
	TextEventHandler_t4116109227::get_offset_of_mBoundingBoxTexture_8(),
	TextEventHandler_t4116109227::get_offset_of_mBoundingBoxMaterial_9(),
	TextEventHandler_t4116109227::get_offset_of_mIsInitialized_10(),
	TextEventHandler_t4116109227::get_offset_of_mVideoBackgroundChanged_11(),
	TextEventHandler_t4116109227::get_offset_of_mSortedWords_12(),
	TextEventHandler_t4116109227::get_offset_of_mDisplayedWords_13(),
	TextEventHandler_t4116109227::get_offset_of_boundingBoxMaterial_14(),
	TextEventHandler_t4116109227::get_offset_of_textRecoCanvas_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (ApplicationGUIManager_t235235642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[8] = 
{
	ApplicationGUIManager_t235235642::get_offset_of_searchScreen_2(),
	ApplicationGUIManager_t235235642::get_offset_of_contactScreen_3(),
	ApplicationGUIManager_t235235642::get_offset_of_introScreen_4(),
	ApplicationGUIManager_t235235642::get_offset_of_websiteScreen_5(),
	ApplicationGUIManager_t235235642::get_offset_of_waitingScreen_6(),
	ApplicationGUIManager_t235235642::get_offset_of_feedbackScreen_7(),
	ApplicationGUIManager_t235235642::get_offset_of_feedbackText_8(),
	ApplicationGUIManager_t235235642::get_offset_of_contactManager_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (ApplicationManager_t2067665347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[4] = 
{
	ApplicationManager_t2067665347::get_offset_of_appGUIManager_2(),
	ApplicationManager_t2067665347::get_offset_of_dbManager_3(),
	ApplicationManager_t2067665347::get_offset_of_locationManager_4(),
	ApplicationManager_t2067665347::get_offset_of_fbManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (U3CProcessResultsU3Ec__Iterator0_t2708174358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[8] = 
{
	U3CProcessResultsU3Ec__Iterator0_t2708174358::get_offset_of_metaData_0(),
	U3CProcessResultsU3Ec__Iterator0_t2708174358::get_offset_of_U3CresultsU3E__1_1(),
	U3CProcessResultsU3Ec__Iterator0_t2708174358::get_offset_of_U3CbusinessU3E__2_2(),
	U3CProcessResultsU3Ec__Iterator0_t2708174358::get_offset_of_reco_3(),
	U3CProcessResultsU3Ec__Iterator0_t2708174358::get_offset_of_U24this_4(),
	U3CProcessResultsU3Ec__Iterator0_t2708174358::get_offset_of_U24current_5(),
	U3CProcessResultsU3Ec__Iterator0_t2708174358::get_offset_of_U24disposing_6(),
	U3CProcessResultsU3Ec__Iterator0_t2708174358::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (CameraAutoFocus_t117912794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (ContactManager_t2312442069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[9] = 
{
	ContactManager_t2312442069::get_offset_of_name_2(),
	ContactManager_t2312442069::get_offset_of_logo_3(),
	ContactManager_t2312442069::get_offset_of_callText_4(),
	ContactManager_t2312442069::get_offset_of_findText_5(),
	ContactManager_t2312442069::get_offset_of_mapText_6(),
	ContactManager_t2312442069::get_offset_of_number_7(),
	ContactManager_t2312442069::get_offset_of_website_8(),
	ContactManager_t2312442069::get_offset_of_address_9(),
	ContactManager_t2312442069::get_offset_of_fbManager_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (U3CExecuteActionU3Ec__Iterator0_t383275672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[6] = 
{
	U3CExecuteActionU3Ec__Iterator0_t383275672::get_offset_of_function_0(),
	U3CExecuteActionU3Ec__Iterator0_t383275672::get_offset_of_target_1(),
	U3CExecuteActionU3Ec__Iterator0_t383275672::get_offset_of_U24this_2(),
	U3CExecuteActionU3Ec__Iterator0_t383275672::get_offset_of_U24current_3(),
	U3CExecuteActionU3Ec__Iterator0_t383275672::get_offset_of_U24disposing_4(),
	U3CExecuteActionU3Ec__Iterator0_t383275672::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (DatabaseEntry_t2505461191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[15] = 
{
	DatabaseEntry_t2505461191::get_offset_of_SensisID_2(),
	DatabaseEntry_t2505461191::get_offset_of_contactName_3(),
	DatabaseEntry_t2505461191::get_offset_of_contactLogo_4(),
	DatabaseEntry_t2505461191::get_offset_of_contactPhoneNumber1_5(),
	DatabaseEntry_t2505461191::get_offset_of_contactPhoneNumber2_6(),
	DatabaseEntry_t2505461191::get_offset_of_contactWebsite1_7(),
	DatabaseEntry_t2505461191::get_offset_of_contactWebsiteFakeImage_8(),
	DatabaseEntry_t2505461191::get_offset_of_contactEmailAddress1_9(),
	DatabaseEntry_t2505461191::get_offset_of_facebookAddress_10(),
	DatabaseEntry_t2505461191::get_offset_of_nearestLocation_11(),
	DatabaseEntry_t2505461191::get_offset_of_promotionText_12(),
	DatabaseEntry_t2505461191::get_offset_of_promotionImage_13(),
	DatabaseEntry_t2505461191::get_offset_of_twitterAddress_14(),
	DatabaseEntry_t2505461191::get_offset_of_location_15(),
	DatabaseEntry_t2505461191::get_offset_of_distance_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (DatabaseManager_t4081956334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[1] = 
{
	DatabaseManager_t4081956334::get_offset_of_dbEntries_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (FeedbackManager_t2969052828), -1, sizeof(FeedbackManager_t2969052828_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2858[19] = 
{
	FeedbackManager_t2969052828::get_offset_of_returned_2(),
	FeedbackManager_t2969052828::get_offset_of_waitingScreen_3(),
	FeedbackManager_t2969052828::get_offset_of_errorMessage_4(),
	FeedbackManager_t2969052828_StaticFields::get_offset_of_emailError_5(),
	FeedbackManager_t2969052828_StaticFields::get_offset_of_sentMail_6(),
	FeedbackManager_t2969052828::get_offset_of_screenCapture_7(),
	FeedbackManager_t2969052828::get_offset_of_tex_8(),
	FeedbackManager_t2969052828::get_offset_of_source_9(),
	FeedbackManager_t2969052828::get_offset_of_appManager_10(),
	FeedbackManager_t2969052828::get_offset_of_gps_11(),
	FeedbackManager_t2969052828::get_offset_of_business_12(),
	FeedbackManager_t2969052828::get_offset_of_function_13(),
	FeedbackManager_t2969052828::get_offset_of_sender_14(),
	FeedbackManager_t2969052828::get_offset_of_recipient_15(),
	FeedbackManager_t2969052828::get_offset_of_credentials_16(),
	FeedbackManager_t2969052828::get_offset_of_smtpClient_17(),
	FeedbackManager_t2969052828::get_offset_of_port_18(),
	FeedbackManager_t2969052828::get_offset_of_path_19(),
	FeedbackManager_t2969052828_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (FeedbackManagerOld_t108831159), -1, sizeof(FeedbackManagerOld_t108831159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2859[6] = 
{
	FeedbackManagerOld_t108831159::get_offset_of_tex_2(),
	FeedbackManagerOld_t108831159::get_offset_of_sendPositiveFeedback_3(),
	FeedbackManagerOld_t108831159::get_offset_of_sendNegativeFeedback_4(),
	FeedbackManagerOld_t108831159::get_offset_of_source_5(),
	FeedbackManagerOld_t108831159_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	FeedbackManagerOld_t108831159_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (LocationManager_t1546885680), -1, sizeof(LocationManager_t1546885680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2860[2] = 
{
	LocationManager_t1546885680::get_offset_of_currentPosition_2(),
	LocationManager_t1546885680_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (U3CStartU3Ec__Iterator0_t73113186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[5] = 
{
	U3CStartU3Ec__Iterator0_t73113186::get_offset_of_U3CmaxWaitU3E__1_0(),
	U3CStartU3Ec__Iterator0_t73113186::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t73113186::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t73113186::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t73113186::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (NewCloudHandler_t3451870399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[4] = 
{
	NewCloudHandler_t3451870399::get_offset_of_applicationManager_2(),
	NewCloudHandler_t3451870399::get_offset_of_mCloudRecoBehaviour_3(),
	NewCloudHandler_t3451870399::get_offset_of_mIsScanning_4(),
	NewCloudHandler_t3451870399::get_offset_of_mTargetMetaData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (NewTextRecoHandler_t527272212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[3] = 
{
	NewTextRecoHandler_t527272212::get_offset_of_applicationManager_2(),
	NewTextRecoHandler_t527272212::get_offset_of_mTextRecoBehaviour_3(),
	NewTextRecoHandler_t527272212::get_offset_of_textTracker_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (ScreenshotManager_t2800354649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[3] = 
{
	ScreenshotManager_t2800354649::get_offset_of_now_2(),
	ScreenshotManager_t2800354649::get_offset_of_path_3(),
	ScreenshotManager_t2800354649::get_offset_of_finished_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (TextureScale_t2270433493), -1, sizeof(TextureScale_t2270433493_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2865[8] = 
{
	TextureScale_t2270433493_StaticFields::get_offset_of_texColors_0(),
	TextureScale_t2270433493_StaticFields::get_offset_of_newColors_1(),
	TextureScale_t2270433493_StaticFields::get_offset_of_w_2(),
	TextureScale_t2270433493_StaticFields::get_offset_of_ratioX_3(),
	TextureScale_t2270433493_StaticFields::get_offset_of_ratioY_4(),
	TextureScale_t2270433493_StaticFields::get_offset_of_w2_5(),
	TextureScale_t2270433493_StaticFields::get_offset_of_finishCount_6(),
	TextureScale_t2270433493_StaticFields::get_offset_of_mutex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (ThreadData_t1483341464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[2] = 
{
	ThreadData_t1483341464::get_offset_of_start_0(),
	ThreadData_t1483341464::get_offset_of_end_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (BackgroundPlaneBehaviour_t2431285219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (CloudRecoBehaviour_t3077176941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (CylinderTargetBehaviour_t2091399712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (DatabaseLoadBehaviour_t450246482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (DefaultInitializationErrorHandler_t965510117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[3] = 
{
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (DefaultSmartTerrainEventHandler_t870608571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[3] = 
{
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (DefaultTrackableEventHandler_t1082256726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[1] = 
{
	DefaultTrackableEventHandler_t1082256726::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (DeviceTrackerBehaviour_t1884988499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (DigitalEyewearBehaviour_t495394589), -1, sizeof(DigitalEyewearBehaviour_t495394589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2875[1] = 
{
	DigitalEyewearBehaviour_t495394589_StaticFields::get_offset_of_mDigitalEyewearBehaviour_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (GLErrorHandler_t3809113141), -1, sizeof(GLErrorHandler_t3809113141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2876[3] = 
{
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorText_2(),
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (HideExcessAreaBehaviour_t3495034315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (ImageTargetBehaviour_t2654589389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (AndroidUnityPlayer_t852788525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t852788525::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t852788525::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (ComponentFactoryStarterBehaviour_t3249343815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (IOSUnityPlayer_t3656371703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[1] = 
{
	IOSUnityPlayer_t3656371703::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (VuforiaBehaviourComponentFactory_t1383853028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (WSAUnityPlayer_t425981959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[1] = 
{
	WSAUnityPlayer_t425981959::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (KeepAliveBehaviour_t1532923751), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (MarkerBehaviour_t1265232977), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (MaskOutBehaviour_t2994129365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (MultiTargetBehaviour_t3504654311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (ObjectTargetBehaviour_t3836044259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (PropBehaviour_t966064926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (ReconstructionBehaviour_t4009935945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (ReconstructionFromTargetBehaviour_t2111803406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (SmartTerrainTrackerBehaviour_t3844158157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (SurfaceBehaviour_t2405314212), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (TextRecoBehaviour_t3400239837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (TurnOffBehaviour_t3058161409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (TurnOffWordBehaviour_t584991835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (UserDefinedTargetBuildingBehaviour_t4184040062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (VRIntegrationHelper_t556656694), -1, sizeof(VRIntegrationHelper_t556656694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2898[12] = 
{
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraMatrixOriginal_2(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraMatrixOriginal_3(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCamera_4(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCamera_5(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftExcessAreaBehaviour_6(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightExcessAreaBehaviour_7(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraPixelRect_8(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraPixelRect_9(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraDataAcquired_10(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraDataAcquired_11(),
	VRIntegrationHelper_t556656694::get_offset_of_IsLeft_12(),
	VRIntegrationHelper_t556656694::get_offset_of_TrackableParent_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (VideoBackgroundBehaviour_t3161817952), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
