﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUI/ScrollViewState
struct ScrollViewState_t2792222924;

// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
extern "C"  void ScrollViewState__ctor_m853546402 (ScrollViewState_t2792222924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
