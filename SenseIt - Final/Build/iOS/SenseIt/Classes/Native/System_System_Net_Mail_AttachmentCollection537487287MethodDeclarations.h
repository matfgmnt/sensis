﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Mail_Attachment2609929423.h"

// System.Net.Mail.AttachmentCollection
struct AttachmentCollection_t537487287;
// System.Net.Mail.Attachment
struct Attachment_t2609929423;

// System.Void System.Net.Mail.AttachmentCollection::.ctor()
extern "C"  void AttachmentCollection__ctor_m3989921058 (AttachmentCollection_t537487287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::Dispose()
extern "C"  void AttachmentCollection_Dispose_m3904387123 (AttachmentCollection_t537487287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::ClearItems()
extern "C"  void AttachmentCollection_ClearItems_m1972484543 (AttachmentCollection_t537487287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::InsertItem(System.Int32,System.Net.Mail.Attachment)
extern "C"  void AttachmentCollection_InsertItem_m3769153535 (AttachmentCollection_t537487287 * __this, int32_t ___index0, Attachment_t2609929423 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::RemoveItem(System.Int32)
extern "C"  void AttachmentCollection_RemoveItem_m2837634070 (AttachmentCollection_t537487287 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::SetItem(System.Int32,System.Net.Mail.Attachment)
extern "C"  void AttachmentCollection_SetItem_m342635130 (AttachmentCollection_t537487287 * __this, int32_t ___index0, Attachment_t2609929423 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
