﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2231194049MethodDeclarations.h"


// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::.ctor()
#define Collection_1__ctor_m1400340605(__this, method) ((  void (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1__ctor_m3383758099_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1455793284(__this, method) ((  bool (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2832435102_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m756877455(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t372211589 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2795445359_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1947649752(__this, method) ((  Il2CppObject * (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m539985258_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m1024724743(__this, ___value0, method) ((  int32_t (*) (Collection_1_t372211589 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m916188271_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m919582727(__this, ___value0, method) ((  bool (*) (Collection_1_t372211589 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3240760119_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m3219420985(__this, ___value0, method) ((  int32_t (*) (Collection_1_t372211589 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3460849589_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m3739724778(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t372211589 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3482199744_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m3375151004(__this, ___value0, method) ((  void (*) (Collection_1_t372211589 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1739078822_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m279505983(__this, method) ((  bool (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1442644511_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1373153483(__this, method) ((  Il2CppObject * (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1422512927_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3973355278(__this, method) ((  bool (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2968235316_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3005854731(__this, method) ((  bool (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1990189611_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m2394987704(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t372211589 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m75082808_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m857003181(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t372211589 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m507853765_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Add(T)
#define Collection_1_Add_m2059908838(__this, ___item0, method) ((  void (*) (Collection_1_t372211589 *, LinkedResource_t830466835 *, const MethodInfo*))Collection_1_Add_m2987402052_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Clear()
#define Collection_1_Clear_m2949294450(__this, method) ((  void (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_Clear_m1596645192_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::ClearItems()
#define Collection_1_ClearItems_m2782963094(__this, method) ((  void (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_ClearItems_m1175603758_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Contains(T)
#define Collection_1_Contains_m3099753260(__this, ___item0, method) ((  bool (*) (Collection_1_t372211589 *, LinkedResource_t830466835 *, const MethodInfo*))Collection_1_Contains_m2116635914_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m893149194(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t372211589 *, LinkedResourceU5BU5D_t4022214466*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1578267616_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::GetEnumerator()
#define Collection_1_GetEnumerator_m1137411814(__this, method) ((  Il2CppObject* (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_GetEnumerator_m2963411583_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::IndexOf(T)
#define Collection_1_IndexOf_m2449915836(__this, ___item0, method) ((  int32_t (*) (Collection_1_t372211589 *, LinkedResource_t830466835 *, const MethodInfo*))Collection_1_IndexOf_m3885709710_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Insert(System.Int32,T)
#define Collection_1_Insert_m3868281281(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t372211589 *, int32_t, LinkedResource_t830466835 *, const MethodInfo*))Collection_1_Insert_m2334889193_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m1668431099(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t372211589 *, int32_t, LinkedResource_t830466835 *, const MethodInfo*))Collection_1_InsertItem_m3611385334_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::Remove(T)
#define Collection_1_Remove_m601756921(__this, ___item0, method) ((  bool (*) (Collection_1_t372211589 *, LinkedResource_t830466835 *, const MethodInfo*))Collection_1_Remove_m452558737_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m1355538117(__this, ___index0, method) ((  void (*) (Collection_1_t372211589 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1632496813_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m352842099(__this, ___index0, method) ((  void (*) (Collection_1_t372211589 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4104600353_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::get_Count()
#define Collection_1_get_Count_m1092679633(__this, method) ((  int32_t (*) (Collection_1_t372211589 *, const MethodInfo*))Collection_1_get_Count_m2250721247_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::get_Item(System.Int32)
#define Collection_1_get_Item_m3910367689(__this, ___index0, method) ((  LinkedResource_t830466835 * (*) (Collection_1_t372211589 *, int32_t, const MethodInfo*))Collection_1_get_Item_m266052953_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m2182297600(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t372211589 *, int32_t, LinkedResource_t830466835 *, const MethodInfo*))Collection_1_set_Item_m3489932746_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m2610869184(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t372211589 *, int32_t, LinkedResource_t830466835 *, const MethodInfo*))Collection_1_SetItem_m1075410277_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m3804389842(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3443424420_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m3942402946(__this /* static, unused */, ___item0, method) ((  LinkedResource_t830466835 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1521356246_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m1580650862(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m215419136_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m1281675928(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m328767958_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.LinkedResource>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m1121421769(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3594284193_gshared)(__this /* static, unused */, ___list0, method)
