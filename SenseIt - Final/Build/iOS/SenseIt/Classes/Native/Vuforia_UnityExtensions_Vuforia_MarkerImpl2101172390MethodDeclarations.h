﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// Vuforia.MarkerImpl
struct MarkerImpl_t2101172390;
// System.String
struct String_t;

// System.Int32 Vuforia.MarkerImpl::get_MarkerID()
extern "C"  int32_t MarkerImpl_get_MarkerID_m1080418176 (MarkerImpl_t2101172390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerImpl::set_MarkerID(System.Int32)
extern "C"  void MarkerImpl_set_MarkerID_m569478379 (MarkerImpl_t2101172390 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerImpl::.ctor(System.String,System.Int32,System.Single,System.Int32)
extern "C"  void MarkerImpl__ctor_m2866234631 (MarkerImpl_t2101172390 * __this, String_t* ___name0, int32_t ___id1, float ___size2, int32_t ___markerID3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.MarkerImpl::GetSize()
extern "C"  float MarkerImpl_GetSize_m2918518417 (MarkerImpl_t2101172390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerImpl::SetSize(System.Single)
extern "C"  void MarkerImpl_SetSize_m3043196394 (MarkerImpl_t2101172390 * __this, float ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
