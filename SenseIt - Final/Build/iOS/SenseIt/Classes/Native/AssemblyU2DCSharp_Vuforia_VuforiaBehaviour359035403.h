﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759.h"

// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t359035403;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaBehaviour
struct  VuforiaBehaviour_t359035403  : public VuforiaAbstractBehaviour_t3319870759
{
public:

public:
};

struct VuforiaBehaviour_t359035403_StaticFields
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::mVuforiaBehaviour
	VuforiaBehaviour_t359035403 * ___mVuforiaBehaviour_44;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_44() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t359035403_StaticFields, ___mVuforiaBehaviour_44)); }
	inline VuforiaBehaviour_t359035403 * get_mVuforiaBehaviour_44() const { return ___mVuforiaBehaviour_44; }
	inline VuforiaBehaviour_t359035403 ** get_address_of_mVuforiaBehaviour_44() { return &___mVuforiaBehaviour_44; }
	inline void set_mVuforiaBehaviour_44(VuforiaBehaviour_t359035403 * value)
	{
		___mVuforiaBehaviour_44 = value;
		Il2CppCodeGenWriteBarrier(&___mVuforiaBehaviour_44, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
