﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// TextureScale/ThreadData
struct ThreadData_t1483341464;

// System.Void TextureScale/ThreadData::.ctor(System.Int32,System.Int32)
extern "C"  void ThreadData__ctor_m2400393469 (ThreadData_t1483341464 * __this, int32_t ___s0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
