﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Mail_SmtpClient_SmtpResponse616735068.h"
#include "mscorlib_System_String2029220233.h"

// System.String
struct String_t;
// System.Net.Mail.SmtpClient/SmtpResponse
struct SmtpResponse_t616735068;
struct SmtpResponse_t616735068_marshaled_pinvoke;
struct SmtpResponse_t616735068_marshaled_com;

// System.Net.Mail.SmtpClient/SmtpResponse System.Net.Mail.SmtpClient/SmtpResponse::Parse(System.String)
extern "C"  SmtpResponse_t616735068  SmtpResponse_Parse_m3924575016 (Il2CppObject * __this /* static, unused */, String_t* ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SmtpResponse_t616735068;
struct SmtpResponse_t616735068_marshaled_pinvoke;

extern "C" void SmtpResponse_t616735068_marshal_pinvoke(const SmtpResponse_t616735068& unmarshaled, SmtpResponse_t616735068_marshaled_pinvoke& marshaled);
extern "C" void SmtpResponse_t616735068_marshal_pinvoke_back(const SmtpResponse_t616735068_marshaled_pinvoke& marshaled, SmtpResponse_t616735068& unmarshaled);
extern "C" void SmtpResponse_t616735068_marshal_pinvoke_cleanup(SmtpResponse_t616735068_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SmtpResponse_t616735068;
struct SmtpResponse_t616735068_marshaled_com;

extern "C" void SmtpResponse_t616735068_marshal_com(const SmtpResponse_t616735068& unmarshaled, SmtpResponse_t616735068_marshaled_com& marshaled);
extern "C" void SmtpResponse_t616735068_marshal_com_back(const SmtpResponse_t616735068_marshaled_com& marshaled, SmtpResponse_t616735068& unmarshaled);
extern "C" void SmtpResponse_t616735068_marshal_com_cleanup(SmtpResponse_t616735068_marshaled_com& marshaled);
