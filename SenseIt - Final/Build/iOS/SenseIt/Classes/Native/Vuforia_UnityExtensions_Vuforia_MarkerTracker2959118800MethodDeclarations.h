﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerTracker
struct MarkerTracker_t2959118800;

// System.Void Vuforia.MarkerTracker::.ctor()
extern "C"  void MarkerTracker__ctor_m1955807820 (MarkerTracker_t2959118800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
