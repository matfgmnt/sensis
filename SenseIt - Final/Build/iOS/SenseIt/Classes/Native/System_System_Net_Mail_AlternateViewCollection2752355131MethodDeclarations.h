﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Mail_AlternateView1668264691.h"

// System.Net.Mail.AlternateViewCollection
struct AlternateViewCollection_t2752355131;
// System.Net.Mail.AlternateView
struct AlternateView_t1668264691;

// System.Void System.Net.Mail.AlternateViewCollection::.ctor()
extern "C"  void AlternateViewCollection__ctor_m3113783692 (AlternateViewCollection_t2752355131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::Dispose()
extern "C"  void AlternateViewCollection_Dispose_m1743263127 (AlternateViewCollection_t2752355131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::ClearItems()
extern "C"  void AlternateViewCollection_ClearItems_m1367470755 (AlternateViewCollection_t2752355131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::InsertItem(System.Int32,System.Net.Mail.AlternateView)
extern "C"  void AlternateViewCollection_InsertItem_m2476031651 (AlternateViewCollection_t2752355131 * __this, int32_t ___index0, AlternateView_t1668264691 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::RemoveItem(System.Int32)
extern "C"  void AlternateViewCollection_RemoveItem_m637176136 (AlternateViewCollection_t2752355131 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::SetItem(System.Int32,System.Net.Mail.AlternateView)
extern "C"  void AlternateViewCollection_SetItem_m4191826138 (AlternateViewCollection_t2752355131 * __this, int32_t ___index0, AlternateView_t1668264691 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
