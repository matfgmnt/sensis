﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// FGMNT.ApplicationGUIManager
struct ApplicationGUIManager_t235235642;
// FGMNT.DatabaseManager
struct DatabaseManager_t4081956334;
// FGMNT.LocationManager
struct LocationManager_t1546885680;
// FGMNT.FeedbackManager
struct FeedbackManager_t2969052828;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.ApplicationManager
struct  ApplicationManager_t2067665347  : public MonoBehaviour_t1158329972
{
public:
	// FGMNT.ApplicationGUIManager FGMNT.ApplicationManager::appGUIManager
	ApplicationGUIManager_t235235642 * ___appGUIManager_2;
	// FGMNT.DatabaseManager FGMNT.ApplicationManager::dbManager
	DatabaseManager_t4081956334 * ___dbManager_3;
	// FGMNT.LocationManager FGMNT.ApplicationManager::locationManager
	LocationManager_t1546885680 * ___locationManager_4;
	// FGMNT.FeedbackManager FGMNT.ApplicationManager::fbManager
	FeedbackManager_t2969052828 * ___fbManager_5;

public:
	inline static int32_t get_offset_of_appGUIManager_2() { return static_cast<int32_t>(offsetof(ApplicationManager_t2067665347, ___appGUIManager_2)); }
	inline ApplicationGUIManager_t235235642 * get_appGUIManager_2() const { return ___appGUIManager_2; }
	inline ApplicationGUIManager_t235235642 ** get_address_of_appGUIManager_2() { return &___appGUIManager_2; }
	inline void set_appGUIManager_2(ApplicationGUIManager_t235235642 * value)
	{
		___appGUIManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___appGUIManager_2, value);
	}

	inline static int32_t get_offset_of_dbManager_3() { return static_cast<int32_t>(offsetof(ApplicationManager_t2067665347, ___dbManager_3)); }
	inline DatabaseManager_t4081956334 * get_dbManager_3() const { return ___dbManager_3; }
	inline DatabaseManager_t4081956334 ** get_address_of_dbManager_3() { return &___dbManager_3; }
	inline void set_dbManager_3(DatabaseManager_t4081956334 * value)
	{
		___dbManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___dbManager_3, value);
	}

	inline static int32_t get_offset_of_locationManager_4() { return static_cast<int32_t>(offsetof(ApplicationManager_t2067665347, ___locationManager_4)); }
	inline LocationManager_t1546885680 * get_locationManager_4() const { return ___locationManager_4; }
	inline LocationManager_t1546885680 ** get_address_of_locationManager_4() { return &___locationManager_4; }
	inline void set_locationManager_4(LocationManager_t1546885680 * value)
	{
		___locationManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___locationManager_4, value);
	}

	inline static int32_t get_offset_of_fbManager_5() { return static_cast<int32_t>(offsetof(ApplicationManager_t2067665347, ___fbManager_5)); }
	inline FeedbackManager_t2969052828 * get_fbManager_5() const { return ___fbManager_5; }
	inline FeedbackManager_t2969052828 ** get_address_of_fbManager_5() { return &___fbManager_5; }
	inline void set_fbManager_5(FeedbackManager_t2969052828 * value)
	{
		___fbManager_5 = value;
		Il2CppCodeGenWriteBarrier(&___fbManager_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
