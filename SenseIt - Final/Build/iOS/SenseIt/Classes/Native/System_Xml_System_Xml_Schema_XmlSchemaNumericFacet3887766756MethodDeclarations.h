﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// System.Xml.Schema.XmlSchemaNumericFacet
struct XmlSchemaNumericFacet_t3887766756;

// System.Void System.Xml.Schema.XmlSchemaNumericFacet::.ctor()
extern "C"  void XmlSchemaNumericFacet__ctor_m2292212506 (XmlSchemaNumericFacet_t3887766756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
