﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamBehaviour
struct WebCamBehaviour_t407765638;

// System.Void Vuforia.WebCamBehaviour::.ctor()
extern "C"  void WebCamBehaviour__ctor_m2114979319 (WebCamBehaviour_t407765638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
