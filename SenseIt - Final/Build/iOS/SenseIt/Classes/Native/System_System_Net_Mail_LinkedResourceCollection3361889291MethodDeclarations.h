﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Mail_LinkedResource830466835.h"

// System.Net.Mail.LinkedResourceCollection
struct LinkedResourceCollection_t3361889291;
// System.Net.Mail.LinkedResource
struct LinkedResource_t830466835;

// System.Void System.Net.Mail.LinkedResourceCollection::.ctor()
extern "C"  void LinkedResourceCollection__ctor_m3684794290 (LinkedResourceCollection_t3361889291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::Dispose()
extern "C"  void LinkedResourceCollection_Dispose_m4173656641 (LinkedResourceCollection_t3361889291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::Dispose(System.Boolean)
extern "C"  void LinkedResourceCollection_Dispose_m2261282958 (LinkedResourceCollection_t3361889291 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::ClearItems()
extern "C"  void LinkedResourceCollection_ClearItems_m2304948869 (LinkedResourceCollection_t3361889291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::InsertItem(System.Int32,System.Net.Mail.LinkedResource)
extern "C"  void LinkedResourceCollection_InsertItem_m70543999 (LinkedResourceCollection_t3361889291 * __this, int32_t ___index0, LinkedResource_t830466835 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::RemoveItem(System.Int32)
extern "C"  void LinkedResourceCollection_RemoveItem_m738488282 (LinkedResourceCollection_t3361889291 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.LinkedResourceCollection::SetItem(System.Int32,System.Net.Mail.LinkedResource)
extern "C"  void LinkedResourceCollection_SetItem_m3017891602 (LinkedResourceCollection_t3361889291 * __this, int32_t ___index0, LinkedResource_t830466835 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
