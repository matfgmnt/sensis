﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl4188455712.h"

// Vuforia.VuforiaManagerImpl/<>c__DisplayClass9
struct U3CU3Ec__DisplayClass9_t2337328198;

// System.Void Vuforia.VuforiaManagerImpl/<>c__DisplayClass9::.ctor()
extern "C"  void U3CU3Ec__DisplayClass9__ctor_m3536125712 (U3CU3Ec__DisplayClass9_t2337328198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaManagerImpl/<>c__DisplayClass9::<UpdateTrackableFoundQueue>b__5(Vuforia.VuforiaManagerImpl/TrackableResultData)
extern "C"  bool U3CU3Ec__DisplayClass9_U3CUpdateTrackableFoundQueueU3Eb__5_m141049830 (U3CU3Ec__DisplayClass9_t2337328198 * __this, TrackableResultData_t1947527974  ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaManagerImpl/<>c__DisplayClass9::<UpdateTrackableFoundQueue>b__6(Vuforia.VuforiaManagerImpl/VuMarkTargetResultData)
extern "C"  bool U3CU3Ec__DisplayClass9_U3CUpdateTrackableFoundQueueU3Eb__6_m3056678515 (U3CU3Ec__DisplayClass9_t2337328198 * __this, VuMarkTargetResultData_t4188455712  ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
