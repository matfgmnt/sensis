﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// FGMNT.DatabaseManager
struct DatabaseManager_t4081956334;
// System.Collections.Generic.List`1<FGMNT.DatabaseEntry>
struct List_1_t1874582323;
// System.String
struct String_t;

// System.Void FGMNT.DatabaseManager::.ctor()
extern "C"  void DatabaseManager__ctor_m3105601709 (DatabaseManager_t4081956334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.DatabaseManager::Awake()
extern "C"  void DatabaseManager_Awake_m1656851526 (DatabaseManager_t4081956334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FGMNT.DatabaseEntry> FGMNT.DatabaseManager::findBusinessInfo(System.String)
extern "C"  List_1_t1874582323 * DatabaseManager_findBusinessInfo_m2870073708 (DatabaseManager_t4081956334 * __this, String_t* ___metaData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
