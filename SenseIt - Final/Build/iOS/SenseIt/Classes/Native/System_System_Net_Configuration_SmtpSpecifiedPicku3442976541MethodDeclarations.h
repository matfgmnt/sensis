﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement
struct SmtpSpecifiedPickupDirectoryElement_t3442976541;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;

// System.Void System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement::.cctor()
extern "C"  void SmtpSpecifiedPickupDirectoryElement__cctor_m604302916 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * SmtpSpecifiedPickupDirectoryElement_get_Properties_m1538904016 (SmtpSpecifiedPickupDirectoryElement_t3442976541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
