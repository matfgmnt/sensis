﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"


// System.Void System.Collections.Generic.List`1/Enumerator<FGMNT.DatabaseEntry>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2327283724(__this, ___l0, method) ((  void (*) (Enumerator_t1409311997 *, List_1_t1874582323 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FGMNT.DatabaseEntry>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m546240782(__this, method) ((  void (*) (Enumerator_t1409311997 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<FGMNT.DatabaseEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1444239386(__this, method) ((  Il2CppObject * (*) (Enumerator_t1409311997 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FGMNT.DatabaseEntry>::Dispose()
#define Enumerator_Dispose_m988615007(__this, method) ((  void (*) (Enumerator_t1409311997 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FGMNT.DatabaseEntry>::VerifyState()
#define Enumerator_VerifyState_m4153627634(__this, method) ((  void (*) (Enumerator_t1409311997 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<FGMNT.DatabaseEntry>::MoveNext()
#define Enumerator_MoveNext_m3102748941(__this, method) ((  bool (*) (Enumerator_t1409311997 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<FGMNT.DatabaseEntry>::get_Current()
#define Enumerator_get_Current_m3085411957(__this, method) ((  DatabaseEntry_t2505461191 * (*) (Enumerator_t1409311997 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
