﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Threading.SynchronizationContext
struct SynchronizationContext_t3857790437;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SynchronizationContext
struct  SynchronizationContext_t3857790437  : public Il2CppObject
{
public:
	// System.Boolean System.Threading.SynchronizationContext::notification_required
	bool ___notification_required_0;

public:
	inline static int32_t get_offset_of_notification_required_0() { return static_cast<int32_t>(offsetof(SynchronizationContext_t3857790437, ___notification_required_0)); }
	inline bool get_notification_required_0() const { return ___notification_required_0; }
	inline bool* get_address_of_notification_required_0() { return &___notification_required_0; }
	inline void set_notification_required_0(bool value)
	{
		___notification_required_0 = value;
	}
};

struct SynchronizationContext_t3857790437_ThreadStaticFields
{
public:
	// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::currentContext
	SynchronizationContext_t3857790437 * ___currentContext_1;

public:
	inline static int32_t get_offset_of_currentContext_1() { return static_cast<int32_t>(offsetof(SynchronizationContext_t3857790437_ThreadStaticFields, ___currentContext_1)); }
	inline SynchronizationContext_t3857790437 * get_currentContext_1() const { return ___currentContext_1; }
	inline SynchronizationContext_t3857790437 ** get_address_of_currentContext_1() { return &___currentContext_1; }
	inline void set_currentContext_1(SynchronizationContext_t3857790437 * value)
	{
		___currentContext_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentContext_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
