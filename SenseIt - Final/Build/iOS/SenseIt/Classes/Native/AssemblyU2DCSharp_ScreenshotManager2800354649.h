﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenshotManager
struct  ScreenshotManager_t2800354649  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ScreenshotManager::now
	bool ___now_2;
	// System.String ScreenshotManager::path
	String_t* ___path_3;
	// System.Boolean ScreenshotManager::finished
	bool ___finished_4;

public:
	inline static int32_t get_offset_of_now_2() { return static_cast<int32_t>(offsetof(ScreenshotManager_t2800354649, ___now_2)); }
	inline bool get_now_2() const { return ___now_2; }
	inline bool* get_address_of_now_2() { return &___now_2; }
	inline void set_now_2(bool value)
	{
		___now_2 = value;
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(ScreenshotManager_t2800354649, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier(&___path_3, value);
	}

	inline static int32_t get_offset_of_finished_4() { return static_cast<int32_t>(offsetof(ScreenshotManager_t2800354649, ___finished_4)); }
	inline bool get_finished_4() const { return ___finished_4; }
	inline bool* get_address_of_finished_4() { return &___finished_4; }
	inline void set_finished_4(bool value)
	{
		___finished_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
