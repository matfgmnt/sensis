﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Func`2<FGMNT.DatabaseEntry,System.Single>
struct Func_2_t3268449786;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.LocationManager
struct  LocationManager_t1546885680  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 FGMNT.LocationManager::currentPosition
	Vector2_t2243707579  ___currentPosition_2;

public:
	inline static int32_t get_offset_of_currentPosition_2() { return static_cast<int32_t>(offsetof(LocationManager_t1546885680, ___currentPosition_2)); }
	inline Vector2_t2243707579  get_currentPosition_2() const { return ___currentPosition_2; }
	inline Vector2_t2243707579 * get_address_of_currentPosition_2() { return &___currentPosition_2; }
	inline void set_currentPosition_2(Vector2_t2243707579  value)
	{
		___currentPosition_2 = value;
	}
};

struct LocationManager_t1546885680_StaticFields
{
public:
	// System.Func`2<FGMNT.DatabaseEntry,System.Single> FGMNT.LocationManager::<>f__am$cache0
	Func_2_t3268449786 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(LocationManager_t1546885680_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t3268449786 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t3268449786 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t3268449786 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
