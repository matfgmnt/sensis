﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2212564818MethodDeclarations.h"


// System.Void System.Func`2<FGMNT.DatabaseEntry,System.Single>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3736397616(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3268449786 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1874497973_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<FGMNT.DatabaseEntry,System.Single>::Invoke(T)
#define Func_2_Invoke_m1313380808(__this, ___arg10, method) ((  float (*) (Func_2_t3268449786 *, DatabaseEntry_t2505461191 *, const MethodInfo*))Func_2_Invoke_m1144286175_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<FGMNT.DatabaseEntry,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m516967165(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3268449786 *, DatabaseEntry_t2505461191 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m669892004_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<FGMNT.DatabaseEntry,System.Single>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2909889522(__this, ___result0, method) ((  float (*) (Func_2_t3268449786 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m971580865_gshared)(__this, ___result0, method)
