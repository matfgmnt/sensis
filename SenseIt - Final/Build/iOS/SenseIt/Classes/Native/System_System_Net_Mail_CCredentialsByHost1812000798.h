﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mail.CCredentialsByHost
struct  CCredentialsByHost_t1812000798  : public Il2CppObject
{
public:
	// System.String System.Net.Mail.CCredentialsByHost::userName
	String_t* ___userName_0;
	// System.String System.Net.Mail.CCredentialsByHost::password
	String_t* ___password_1;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(CCredentialsByHost_t1812000798, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier(&___userName_0, value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(CCredentialsByHost_t1812000798, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier(&___password_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
