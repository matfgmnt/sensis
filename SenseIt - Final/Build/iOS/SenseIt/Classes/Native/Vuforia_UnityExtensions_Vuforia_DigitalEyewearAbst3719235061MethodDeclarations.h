﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "Vuforia_UnityExtensions_Vuforia_DistortionRenderin3766399464.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "Vuforia_UnityExtensions_Vuforia_Device_Mode2855916820.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearAbst1156375578.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearAbst1337058172.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearAbst4290835274.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer1654543970.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer2705300828.h"

// Vuforia.DigitalEyewearAbstractBehaviour
struct DigitalEyewearAbstractBehaviour_t3719235061;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Camera
struct Camera_t189460977;
// Vuforia.ICameraConfiguration
struct ICameraConfiguration_t2507317352;

// System.Single Vuforia.DigitalEyewearAbstractBehaviour::get_CameraOffset()
extern "C"  float DigitalEyewearAbstractBehaviour_get_CameraOffset_m2495773652 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::set_CameraOffset(System.Single)
extern "C"  void DigitalEyewearAbstractBehaviour_set_CameraOffset_m4251065297 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.DigitalEyewearAbstractBehaviour::get_CentralAnchorPoint()
extern "C"  Transform_t3275118058 * DigitalEyewearAbstractBehaviour_get_CentralAnchorPoint_m1622684502 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.DigitalEyewearAbstractBehaviour::get_ParentAnchorPoint()
extern "C"  Transform_t3275118058 * DigitalEyewearAbstractBehaviour_get_ParentAnchorPoint_m1865159193 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.DigitalEyewearAbstractBehaviour::get_IsStereoRendering()
extern "C"  bool DigitalEyewearAbstractBehaviour_get_IsStereoRendering_m1990104116 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.DigitalEyewearAbstractBehaviour::get_PrimaryCamera()
extern "C"  Camera_t189460977 * DigitalEyewearAbstractBehaviour_get_PrimaryCamera_m2911375652 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::set_PrimaryCamera(UnityEngine.Camera)
extern "C"  void DigitalEyewearAbstractBehaviour_set_PrimaryCamera_m499470935 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, Camera_t189460977 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.DigitalEyewearAbstractBehaviour::get_SecondaryCamera()
extern "C"  Camera_t189460977 * DigitalEyewearAbstractBehaviour_get_SecondaryCamera_m3931999956 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::set_SecondaryCamera(UnityEngine.Camera)
extern "C"  void DigitalEyewearAbstractBehaviour_set_SecondaryCamera_m571661197 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, Camera_t189460977 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.DigitalEyewearAbstractBehaviour::get_DistortionRenderingLayer()
extern "C"  int32_t DigitalEyewearAbstractBehaviour_get_DistortionRenderingLayer_m2070972044 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::set_DistortionRenderingLayer(System.Int32)
extern "C"  void DigitalEyewearAbstractBehaviour_set_DistortionRenderingLayer_m1425449867 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DistortionRenderingMode Vuforia.DigitalEyewearAbstractBehaviour::get_DistortionRendering()
extern "C"  int32_t DigitalEyewearAbstractBehaviour_get_DistortionRendering_m850336278 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::Awake()
extern "C"  void DigitalEyewearAbstractBehaviour_Awake_m1718483416 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::Start()
extern "C"  void DigitalEyewearAbstractBehaviour_Start_m2279789257 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::Stop()
extern "C"  void DigitalEyewearAbstractBehaviour_Stop_m3628379609 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::Update()
extern "C"  void DigitalEyewearAbstractBehaviour_Update_m323005830 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::OnDestroy()
extern "C"  void DigitalEyewearAbstractBehaviour_OnDestroy_m3421574806 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C"  void DigitalEyewearAbstractBehaviour_OnVideoBackgroundConfigChanged_m453316219 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetCentralAnchorPoint(UnityEngine.Transform)
extern "C"  void DigitalEyewearAbstractBehaviour_SetCentralAnchorPoint_m41456514 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, Transform_t3275118058 * ___anchorPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetParentAnchorPoint(UnityEngine.Transform)
extern "C"  void DigitalEyewearAbstractBehaviour_SetParentAnchorPoint_m667942721 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, Transform_t3275118058 * ___parentAnchorPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetCameraOffset(System.Single)
extern "C"  void DigitalEyewearAbstractBehaviour_SetCameraOffset_m1489594024 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, float ___Offset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetViewerActive(System.Boolean,System.Boolean)
extern "C"  void DigitalEyewearAbstractBehaviour_SetViewerActive_m2196624241 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, bool ___isActive0, bool ___reinitializeCamera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetViewerActive(System.Boolean)
extern "C"  void DigitalEyewearAbstractBehaviour_SetViewerActive_m1507277558 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, bool ___isActive0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetMode(Vuforia.Device/Mode)
extern "C"  void DigitalEyewearAbstractBehaviour_SetMode_m2984835790 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetDistortionRendering(Vuforia.DistortionRenderingMode)
extern "C"  void DigitalEyewearAbstractBehaviour_SetDistortionRendering_m3811382906 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DigitalEyewearAbstractBehaviour/EyewearType Vuforia.DigitalEyewearAbstractBehaviour::GetEyewearType()
extern "C"  int32_t DigitalEyewearAbstractBehaviour_GetEyewearType_m3242426012 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetEyewearType(Vuforia.DigitalEyewearAbstractBehaviour/EyewearType)
extern "C"  void DigitalEyewearAbstractBehaviour_SetEyewearType_m3340474493 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DigitalEyewearAbstractBehaviour/StereoFramework Vuforia.DigitalEyewearAbstractBehaviour::GetStereoCameraConfig()
extern "C"  int32_t DigitalEyewearAbstractBehaviour_GetStereoCameraConfig_m1062425613 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetStereoCameraConfiguration(Vuforia.DigitalEyewearAbstractBehaviour/StereoFramework)
extern "C"  void DigitalEyewearAbstractBehaviour_SetStereoCameraConfiguration_m881644458 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, int32_t ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DigitalEyewearAbstractBehaviour/SeeThroughConfiguration Vuforia.DigitalEyewearAbstractBehaviour::GetSeeThroughConfiguration()
extern "C"  int32_t DigitalEyewearAbstractBehaviour_GetSeeThroughConfiguration_m1001027100 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetSeeThroughConfiguration(Vuforia.DigitalEyewearAbstractBehaviour/SeeThroughConfiguration)
extern "C"  void DigitalEyewearAbstractBehaviour_SetSeeThroughConfiguration_m1263551165 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, int32_t ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::EnableAutomaticFocusPointSelection(System.Boolean)
extern "C"  void DigitalEyewearAbstractBehaviour_EnableAutomaticFocusPointSelection_m3880551124 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::EnableWorldAnchorUsage(System.Boolean)
extern "C"  void DigitalEyewearAbstractBehaviour_EnableWorldAnchorUsage_m2417345321 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetFocusPoint()
extern "C"  void DigitalEyewearAbstractBehaviour_SetFocusPoint_m3368388835 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::OnVuforiaInitialized()
extern "C"  void DigitalEyewearAbstractBehaviour_OnVuforiaInitialized_m2093149982 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::OnVuforiaStarted()
extern "C"  void DigitalEyewearAbstractBehaviour_OnVuforiaStarted_m3215353673 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::SetViewerActive(System.Boolean,System.Boolean,System.Boolean,Vuforia.CameraDevice/CameraDirection,Vuforia.CameraDevice/CameraDeviceMode)
extern "C"  void DigitalEyewearAbstractBehaviour_SetViewerActive_m1088165008 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, bool ___isActive0, bool ___deinitCam1, bool ___initCam2, int32_t ___camDirection3, int32_t ___mode4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::EnableDistortionRendering()
extern "C"  void DigitalEyewearAbstractBehaviour_EnableDistortionRendering_m3782662533 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::DisableDistortionRendering()
extern "C"  void DigitalEyewearAbstractBehaviour_DisableDistortionRendering_m3529110442 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::ConfigureView()
extern "C"  void DigitalEyewearAbstractBehaviour_ConfigureView_m3357155366 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ICameraConfiguration Vuforia.DigitalEyewearAbstractBehaviour::get_CameraConfiguration()
extern "C"  Il2CppObject * DigitalEyewearAbstractBehaviour_get_CameraConfiguration_m605436528 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::set_CameraConfiguration(Vuforia.ICameraConfiguration)
extern "C"  void DigitalEyewearAbstractBehaviour_set_CameraConfiguration_m3701483595 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DigitalEyewearAbstractBehaviour::.ctor()
extern "C"  void DigitalEyewearAbstractBehaviour__ctor_m1578138673 (DigitalEyewearAbstractBehaviour_t3719235061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
