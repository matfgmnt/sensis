﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// FGMNT.ApplicationManager
struct ApplicationManager_t2067665347;
// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t3400239837;
// Vuforia.TextTracker
struct TextTracker_t89845299;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.NewTextRecoHandler
struct  NewTextRecoHandler_t527272212  : public MonoBehaviour_t1158329972
{
public:
	// FGMNT.ApplicationManager FGMNT.NewTextRecoHandler::applicationManager
	ApplicationManager_t2067665347 * ___applicationManager_2;
	// Vuforia.TextRecoBehaviour FGMNT.NewTextRecoHandler::mTextRecoBehaviour
	TextRecoBehaviour_t3400239837 * ___mTextRecoBehaviour_3;
	// Vuforia.TextTracker FGMNT.NewTextRecoHandler::textTracker
	TextTracker_t89845299 * ___textTracker_4;

public:
	inline static int32_t get_offset_of_applicationManager_2() { return static_cast<int32_t>(offsetof(NewTextRecoHandler_t527272212, ___applicationManager_2)); }
	inline ApplicationManager_t2067665347 * get_applicationManager_2() const { return ___applicationManager_2; }
	inline ApplicationManager_t2067665347 ** get_address_of_applicationManager_2() { return &___applicationManager_2; }
	inline void set_applicationManager_2(ApplicationManager_t2067665347 * value)
	{
		___applicationManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___applicationManager_2, value);
	}

	inline static int32_t get_offset_of_mTextRecoBehaviour_3() { return static_cast<int32_t>(offsetof(NewTextRecoHandler_t527272212, ___mTextRecoBehaviour_3)); }
	inline TextRecoBehaviour_t3400239837 * get_mTextRecoBehaviour_3() const { return ___mTextRecoBehaviour_3; }
	inline TextRecoBehaviour_t3400239837 ** get_address_of_mTextRecoBehaviour_3() { return &___mTextRecoBehaviour_3; }
	inline void set_mTextRecoBehaviour_3(TextRecoBehaviour_t3400239837 * value)
	{
		___mTextRecoBehaviour_3 = value;
		Il2CppCodeGenWriteBarrier(&___mTextRecoBehaviour_3, value);
	}

	inline static int32_t get_offset_of_textTracker_4() { return static_cast<int32_t>(offsetof(NewTextRecoHandler_t527272212, ___textTracker_4)); }
	inline TextTracker_t89845299 * get_textTracker_4() const { return ___textTracker_4; }
	inline TextTracker_t89845299 ** get_address_of_textTracker_4() { return &___textTracker_4; }
	inline void set_textTracker_4(TextTracker_t89845299 * value)
	{
		___textTracker_4 = value;
		Il2CppCodeGenWriteBarrier(&___textTracker_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
