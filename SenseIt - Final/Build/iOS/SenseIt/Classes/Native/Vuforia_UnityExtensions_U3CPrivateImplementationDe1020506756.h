﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDet776153458.h"


#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{BAB5198F-E997-40F1-8D76-18FD8DEB376E}
struct  U3CPrivateImplementationDetailsU3EU7BBAB5198FU2DE997U2D40F1U2D8D76U2D18FD8DEB376EU7D_t1020506756  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7BBAB5198FU2DE997U2D40F1U2D8D76U2D18FD8DEB376EU7D_t1020506756_StaticFields
{
public:
	// <PrivateImplementationDetails>{BAB5198F-E997-40F1-8D76-18FD8DEB376E}/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>{BAB5198F-E997-40F1-8D76-18FD8DEB376E}::$$method0x6000bfc-1
	__StaticArrayInitTypeSizeU3D24_t776153458  ___U24U24method0x6000bfcU2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x6000bfcU2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7BBAB5198FU2DE997U2D40F1U2D8D76U2D18FD8DEB376EU7D_t1020506756_StaticFields, ___U24U24method0x6000bfcU2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t776153458  get_U24U24method0x6000bfcU2D1_0() const { return ___U24U24method0x6000bfcU2D1_0; }
	inline __StaticArrayInitTypeSizeU3D24_t776153458 * get_address_of_U24U24method0x6000bfcU2D1_0() { return &___U24U24method0x6000bfcU2D1_0; }
	inline void set_U24U24method0x6000bfcU2D1_0(__StaticArrayInitTypeSizeU3D24_t776153458  value)
	{
		___U24U24method0x6000bfcU2D1_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
