﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.StandardOleMarshalObject
struct StandardOleMarshalObject_t303634704;

// System.Void System.Runtime.InteropServices.StandardOleMarshalObject::.ctor()
extern "C"  void StandardOleMarshalObject__ctor_m3296759340 (StandardOleMarshalObject_t303634704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
