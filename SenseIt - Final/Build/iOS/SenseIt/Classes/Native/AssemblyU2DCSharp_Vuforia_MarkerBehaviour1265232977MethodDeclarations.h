﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerBehaviour
struct MarkerBehaviour_t1265232977;

// System.Void Vuforia.MarkerBehaviour::.ctor()
extern "C"  void MarkerBehaviour__ctor_m109505184 (MarkerBehaviour_t1265232977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
