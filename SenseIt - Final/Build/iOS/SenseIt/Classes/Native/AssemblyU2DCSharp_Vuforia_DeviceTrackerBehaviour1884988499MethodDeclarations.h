﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.DeviceTrackerBehaviour
struct DeviceTrackerBehaviour_t1884988499;

// System.Void Vuforia.DeviceTrackerBehaviour::.ctor()
extern "C"  void DeviceTrackerBehaviour__ctor_m1101790568 (DeviceTrackerBehaviour_t1884988499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
