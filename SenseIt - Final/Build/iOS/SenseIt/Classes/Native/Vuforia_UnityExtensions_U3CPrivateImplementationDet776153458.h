﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"


#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{BAB5198F-E997-40F1-8D76-18FD8DEB376E}/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t776153458 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t776153458__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
