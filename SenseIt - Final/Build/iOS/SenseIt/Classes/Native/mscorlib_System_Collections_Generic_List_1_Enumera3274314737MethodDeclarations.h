﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"


// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m575627506(__this, ___l0, method) ((  void (*) (Enumerator_t3274314737 *, List_1_t3739585063 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3359886712(__this, method) ((  void (*) (Enumerator_t3274314737 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3460232830(__this, method) ((  Il2CppObject * (*) (Enumerator_t3274314737 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::Dispose()
#define Enumerator_Dispose_m325181849(__this, method) ((  void (*) (Enumerator_t3274314737 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::VerifyState()
#define Enumerator_VerifyState_m3944063800(__this, method) ((  void (*) (Enumerator_t3274314737 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4228224572(__this, method) ((  bool (*) (Enumerator_t3274314737 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::get_Current()
#define Enumerator_get_Current_m2200764234(__this, method) ((  Il2CppObject * (*) (Enumerator_t3274314737 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
