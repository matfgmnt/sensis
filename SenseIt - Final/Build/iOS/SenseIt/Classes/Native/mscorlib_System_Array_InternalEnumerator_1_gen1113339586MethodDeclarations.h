﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"


// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/XmlTokenInfo>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4089529733(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1113339586 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/XmlTokenInfo>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m518158617(__this, method) ((  void (*) (InternalEnumerator_1_t1113339586 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/XmlTokenInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3818060521(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1113339586 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/XmlTokenInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m2489481572(__this, method) ((  void (*) (InternalEnumerator_1_t1113339586 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/XmlTokenInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m284396441(__this, method) ((  bool (*) (InternalEnumerator_1_t1113339586 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/XmlTokenInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m533846990(__this, method) ((  XmlTokenInfo_t254587324 * (*) (InternalEnumerator_1_t1113339586 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
