﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// FGMNT.ContactManager
struct ContactManager_t2312442069;
// System.Object
struct Il2CppObject;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.ContactManager/<ExecuteAction>c__Iterator0
struct  U3CExecuteActionU3Ec__Iterator0_t383275672  : public Il2CppObject
{
public:
	// System.String FGMNT.ContactManager/<ExecuteAction>c__Iterator0::function
	String_t* ___function_0;
	// System.String FGMNT.ContactManager/<ExecuteAction>c__Iterator0::target
	String_t* ___target_1;
	// FGMNT.ContactManager FGMNT.ContactManager/<ExecuteAction>c__Iterator0::$this
	ContactManager_t2312442069 * ___U24this_2;
	// System.Object FGMNT.ContactManager/<ExecuteAction>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean FGMNT.ContactManager/<ExecuteAction>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 FGMNT.ContactManager/<ExecuteAction>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_function_0() { return static_cast<int32_t>(offsetof(U3CExecuteActionU3Ec__Iterator0_t383275672, ___function_0)); }
	inline String_t* get_function_0() const { return ___function_0; }
	inline String_t** get_address_of_function_0() { return &___function_0; }
	inline void set_function_0(String_t* value)
	{
		___function_0 = value;
		Il2CppCodeGenWriteBarrier(&___function_0, value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CExecuteActionU3Ec__Iterator0_t383275672, ___target_1)); }
	inline String_t* get_target_1() const { return ___target_1; }
	inline String_t** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(String_t* value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier(&___target_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CExecuteActionU3Ec__Iterator0_t383275672, ___U24this_2)); }
	inline ContactManager_t2312442069 * get_U24this_2() const { return ___U24this_2; }
	inline ContactManager_t2312442069 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ContactManager_t2312442069 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CExecuteActionU3Ec__Iterator0_t383275672, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CExecuteActionU3Ec__Iterator0_t383275672, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CExecuteActionU3Ec__Iterator0_t383275672, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
