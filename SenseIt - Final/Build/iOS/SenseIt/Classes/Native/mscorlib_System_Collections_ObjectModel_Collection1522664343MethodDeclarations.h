﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2231194049MethodDeclarations.h"


// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::.ctor()
#define Collection_1__ctor_m864914337(__this, method) ((  void (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1__ctor_m3383758099_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m726396754(__this, method) ((  bool (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2832435102_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m3976468259(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1522664343 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2795445359_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m4221862318(__this, method) ((  Il2CppObject * (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m539985258_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m2751181675(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1522664343 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m916188271_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m1262236683(__this, ___value0, method) ((  bool (*) (Collection_1_t1522664343 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3240760119_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m4135556581(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1522664343 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3460849589_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m3813473404(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1522664343 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3482199744_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m3881490834(__this, ___value0, method) ((  void (*) (Collection_1_t1522664343 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1739078822_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1538093875(__this, method) ((  bool (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1442644511_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1077844323(__this, method) ((  Il2CppObject * (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1422512927_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4070057288(__this, method) ((  bool (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2968235316_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1484122087(__this, method) ((  bool (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1990189611_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m3766690828(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1522664343 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m75082808_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m3611756477(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1522664343 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m507853765_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Add(T)
#define Collection_1_Add_m3131689629(__this, ___item0, method) ((  void (*) (Collection_1_t1522664343 *, MailAddress_t1980919589 *, const MethodInfo*))Collection_1_Add_m2987402052_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Clear()
#define Collection_1_Clear_m950326476(__this, method) ((  void (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_Clear_m1596645192_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::ClearItems()
#define Collection_1_ClearItems_m230606486(__this, method) ((  void (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_ClearItems_m1175603758_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Contains(T)
#define Collection_1_Contains_m3556409610(__this, ___item0, method) ((  bool (*) (Collection_1_t1522664343 *, MailAddress_t1980919589 *, const MethodInfo*))Collection_1_Contains_m2116635914_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m121386180(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1522664343 *, MailAddressU5BU5D_t1111260840*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1578267616_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::GetEnumerator()
#define Collection_1_GetEnumerator_m200395222(__this, method) ((  Il2CppObject* (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_GetEnumerator_m2963411583_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::IndexOf(T)
#define Collection_1_IndexOf_m3682173746(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1522664343 *, MailAddress_t1980919589 *, const MethodInfo*))Collection_1_IndexOf_m3885709710_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Insert(System.Int32,T)
#define Collection_1_Insert_m860526993(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1522664343 *, int32_t, MailAddress_t1980919589 *, const MethodInfo*))Collection_1_Insert_m2334889193_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m3575242839(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1522664343 *, int32_t, MailAddress_t1980919589 *, const MethodInfo*))Collection_1_InsertItem_m3611385334_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::Remove(T)
#define Collection_1_Remove_m3544086321(__this, ___item0, method) ((  bool (*) (Collection_1_t1522664343 *, MailAddress_t1980919589 *, const MethodInfo*))Collection_1_Remove_m452558737_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m1453350597(__this, ___index0, method) ((  void (*) (Collection_1_t1522664343 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1632496813_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m2993375745(__this, ___index0, method) ((  void (*) (Collection_1_t1522664343 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4104600353_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::get_Count()
#define Collection_1_get_Count_m439269057(__this, method) ((  int32_t (*) (Collection_1_t1522664343 *, const MethodInfo*))Collection_1_get_Count_m2250721247_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::get_Item(System.Int32)
#define Collection_1_get_Item_m3795138102(__this, ___index0, method) ((  MailAddress_t1980919589 * (*) (Collection_1_t1522664343 *, int32_t, const MethodInfo*))Collection_1_get_Item_m266052953_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m931064678(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1522664343 *, int32_t, MailAddress_t1980919589 *, const MethodInfo*))Collection_1_set_Item_m3489932746_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m3804476214(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1522664343 *, int32_t, MailAddress_t1980919589 *, const MethodInfo*))Collection_1_SetItem_m1075410277_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m390411596(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3443424420_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m4266957734(__this /* static, unused */, ___item0, method) ((  MailAddress_t1980919589 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1521356246_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m331067352(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m215419136_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m472046950(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m328767958_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.MailAddress>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m822296113(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3594284193_gshared)(__this /* static, unused */, ___list0, method)
