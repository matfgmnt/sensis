﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// FGMNT.ApplicationManager
struct ApplicationManager_t2067665347;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

// System.Void FGMNT.ApplicationManager::.ctor()
extern "C"  void ApplicationManager__ctor_m4151983398 (ApplicationManager_t2067665347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationManager::Awake()
extern "C"  void ApplicationManager_Awake_m1747596971 (ApplicationManager_t2067665347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationManager::StartSearching()
extern "C"  void ApplicationManager_StartSearching_m4248614572 (ApplicationManager_t2067665347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationManager::LogoFound(System.String,System.Boolean)
extern "C"  void ApplicationManager_LogoFound_m2018846418 (ApplicationManager_t2067665347 * __this, String_t* ___metaData0, bool ___reco1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationManager::SendFeedback()
extern "C"  void ApplicationManager_SendFeedback_m627230131 (ApplicationManager_t2067665347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationManager::InfoFeedback()
extern "C"  void ApplicationManager_InfoFeedback_m3216891077 (ApplicationManager_t2067665347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FGMNT.ApplicationManager::ProcessResults(System.String,System.Boolean)
extern "C"  Il2CppObject * ApplicationManager_ProcessResults_m2092481680 (ApplicationManager_t2067665347 * __this, String_t* ___metaData0, bool ___reco1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
