﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enum456969527.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra1329355276.h"

// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>
struct LinkedList_1_t1634063505;
// System.Object
struct Il2CppObject;

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Enumerator__ctor_m1450321181_gshared (Enumerator_t456969527 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Enumerator__ctor_m1450321181(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t456969527 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Enumerator__ctor_m1450321181_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m3321573469_gshared (Enumerator_t456969527 * __this, LinkedList_1_t1634063505 * ___parent0, const MethodInfo* method);
#define Enumerator__ctor_m3321573469(__this, ___parent0, method) ((  void (*) (Enumerator_t456969527 *, LinkedList_1_t1634063505 *, const MethodInfo*))Enumerator__ctor_m3321573469_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2873201718_gshared (Enumerator_t456969527 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2873201718(__this, method) ((  Il2CppObject * (*) (Enumerator_t456969527 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2873201718_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m459118152_gshared (Enumerator_t456969527 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m459118152(__this, method) ((  void (*) (Enumerator_t456969527 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m459118152_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m731781777_gshared (Enumerator_t456969527 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m731781777(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t456969527 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m731781777_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C"  void Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m59412512_gshared (Enumerator_t456969527 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m59412512(__this, ___sender0, method) ((  void (*) (Enumerator_t456969527 *, Il2CppObject *, const MethodInfo*))Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m59412512_gshared)(__this, ___sender0, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::get_Current()
extern "C"  TrackableIdPair_t1329355276  Enumerator_get_Current_m4179622312_gshared (Enumerator_t456969527 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4179622312(__this, method) ((  TrackableIdPair_t1329355276  (*) (Enumerator_t456969527 *, const MethodInfo*))Enumerator_get_Current_m4179622312_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2416511834_gshared (Enumerator_t456969527 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2416511834(__this, method) ((  bool (*) (Enumerator_t456969527 *, const MethodInfo*))Enumerator_MoveNext_m2416511834_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::Dispose()
extern "C"  void Enumerator_Dispose_m2767861417_gshared (Enumerator_t456969527 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2767861417(__this, method) ((  void (*) (Enumerator_t456969527 *, const MethodInfo*))Enumerator_Dispose_m2767861417_gshared)(__this, method)
