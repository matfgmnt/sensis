﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// FGMNT.DatabaseEntry[]
struct DatabaseEntryU5BU5D_t375551806;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.DatabaseManager
struct  DatabaseManager_t4081956334  : public MonoBehaviour_t1158329972
{
public:
	// FGMNT.DatabaseEntry[] FGMNT.DatabaseManager::dbEntries
	DatabaseEntryU5BU5D_t375551806* ___dbEntries_2;

public:
	inline static int32_t get_offset_of_dbEntries_2() { return static_cast<int32_t>(offsetof(DatabaseManager_t4081956334, ___dbEntries_2)); }
	inline DatabaseEntryU5BU5D_t375551806* get_dbEntries_2() const { return ___dbEntries_2; }
	inline DatabaseEntryU5BU5D_t375551806** get_address_of_dbEntries_2() { return &___dbEntries_2; }
	inline void set_dbEntries_2(DatabaseEntryU5BU5D_t375551806* value)
	{
		___dbEntries_2 = value;
		Il2CppCodeGenWriteBarrier(&___dbEntries_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
