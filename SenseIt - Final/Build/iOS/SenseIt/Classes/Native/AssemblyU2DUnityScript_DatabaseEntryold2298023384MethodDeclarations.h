﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// DatabaseEntryold
struct DatabaseEntryold_t2298023384;

// System.Void DatabaseEntryold::.ctor()
extern "C"  void DatabaseEntryold__ctor_m1586463262 (DatabaseEntryold_t2298023384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DatabaseEntryold::Main()
extern "C"  void DatabaseEntryold_Main_m3714271935 (DatabaseEntryold_t2298023384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
