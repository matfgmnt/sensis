﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_TextWriter4027217640.h"

// System.Text.Encoding
struct Encoding_t663144255;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.IO.StreamWriter
struct StreamWriter_t3858580635;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamWriter
struct  StreamWriter_t3858580635  : public TextWriter_t4027217640
{
public:
	// System.Text.Encoding System.IO.StreamWriter::internalEncoding
	Encoding_t663144255 * ___internalEncoding_7;
	// System.IO.Stream System.IO.StreamWriter::internalStream
	Stream_t3255436806 * ___internalStream_8;
	// System.Boolean System.IO.StreamWriter::iflush
	bool ___iflush_9;
	// System.Byte[] System.IO.StreamWriter::byte_buf
	ByteU5BU5D_t3397334013* ___byte_buf_10;
	// System.Int32 System.IO.StreamWriter::byte_pos
	int32_t ___byte_pos_11;
	// System.Char[] System.IO.StreamWriter::decode_buf
	CharU5BU5D_t1328083999* ___decode_buf_12;
	// System.Int32 System.IO.StreamWriter::decode_pos
	int32_t ___decode_pos_13;
	// System.Boolean System.IO.StreamWriter::DisposedAlready
	bool ___DisposedAlready_14;
	// System.Boolean System.IO.StreamWriter::preamble_done
	bool ___preamble_done_15;

public:
	inline static int32_t get_offset_of_internalEncoding_7() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___internalEncoding_7)); }
	inline Encoding_t663144255 * get_internalEncoding_7() const { return ___internalEncoding_7; }
	inline Encoding_t663144255 ** get_address_of_internalEncoding_7() { return &___internalEncoding_7; }
	inline void set_internalEncoding_7(Encoding_t663144255 * value)
	{
		___internalEncoding_7 = value;
		Il2CppCodeGenWriteBarrier(&___internalEncoding_7, value);
	}

	inline static int32_t get_offset_of_internalStream_8() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___internalStream_8)); }
	inline Stream_t3255436806 * get_internalStream_8() const { return ___internalStream_8; }
	inline Stream_t3255436806 ** get_address_of_internalStream_8() { return &___internalStream_8; }
	inline void set_internalStream_8(Stream_t3255436806 * value)
	{
		___internalStream_8 = value;
		Il2CppCodeGenWriteBarrier(&___internalStream_8, value);
	}

	inline static int32_t get_offset_of_iflush_9() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___iflush_9)); }
	inline bool get_iflush_9() const { return ___iflush_9; }
	inline bool* get_address_of_iflush_9() { return &___iflush_9; }
	inline void set_iflush_9(bool value)
	{
		___iflush_9 = value;
	}

	inline static int32_t get_offset_of_byte_buf_10() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___byte_buf_10)); }
	inline ByteU5BU5D_t3397334013* get_byte_buf_10() const { return ___byte_buf_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_byte_buf_10() { return &___byte_buf_10; }
	inline void set_byte_buf_10(ByteU5BU5D_t3397334013* value)
	{
		___byte_buf_10 = value;
		Il2CppCodeGenWriteBarrier(&___byte_buf_10, value);
	}

	inline static int32_t get_offset_of_byte_pos_11() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___byte_pos_11)); }
	inline int32_t get_byte_pos_11() const { return ___byte_pos_11; }
	inline int32_t* get_address_of_byte_pos_11() { return &___byte_pos_11; }
	inline void set_byte_pos_11(int32_t value)
	{
		___byte_pos_11 = value;
	}

	inline static int32_t get_offset_of_decode_buf_12() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___decode_buf_12)); }
	inline CharU5BU5D_t1328083999* get_decode_buf_12() const { return ___decode_buf_12; }
	inline CharU5BU5D_t1328083999** get_address_of_decode_buf_12() { return &___decode_buf_12; }
	inline void set_decode_buf_12(CharU5BU5D_t1328083999* value)
	{
		___decode_buf_12 = value;
		Il2CppCodeGenWriteBarrier(&___decode_buf_12, value);
	}

	inline static int32_t get_offset_of_decode_pos_13() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___decode_pos_13)); }
	inline int32_t get_decode_pos_13() const { return ___decode_pos_13; }
	inline int32_t* get_address_of_decode_pos_13() { return &___decode_pos_13; }
	inline void set_decode_pos_13(int32_t value)
	{
		___decode_pos_13 = value;
	}

	inline static int32_t get_offset_of_DisposedAlready_14() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___DisposedAlready_14)); }
	inline bool get_DisposedAlready_14() const { return ___DisposedAlready_14; }
	inline bool* get_address_of_DisposedAlready_14() { return &___DisposedAlready_14; }
	inline void set_DisposedAlready_14(bool value)
	{
		___DisposedAlready_14 = value;
	}

	inline static int32_t get_offset_of_preamble_done_15() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635, ___preamble_done_15)); }
	inline bool get_preamble_done_15() const { return ___preamble_done_15; }
	inline bool* get_address_of_preamble_done_15() { return &___preamble_done_15; }
	inline void set_preamble_done_15(bool value)
	{
		___preamble_done_15 = value;
	}
};

struct StreamWriter_t3858580635_StaticFields
{
public:
	// System.IO.StreamWriter System.IO.StreamWriter::Null
	StreamWriter_t3858580635 * ___Null_16;

public:
	inline static int32_t get_offset_of_Null_16() { return static_cast<int32_t>(offsetof(StreamWriter_t3858580635_StaticFields, ___Null_16)); }
	inline StreamWriter_t3858580635 * get_Null_16() const { return ___Null_16; }
	inline StreamWriter_t3858580635 ** get_address_of_Null_16() { return &___Null_16; }
	inline void set_Null_16(StreamWriter_t3858580635 * value)
	{
		___Null_16 = value;
		Il2CppCodeGenWriteBarrier(&___Null_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
