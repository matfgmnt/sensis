﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Net.Mail.LinkedResource
struct LinkedResource_t830466835;
// System.String
struct String_t;

// System.Void System.Net.Mail.LinkedResource::.ctor(System.String,System.String)
extern "C"  void LinkedResource__ctor_m3490527260 (LinkedResource_t830466835 * __this, String_t* ___fileName0, String_t* ___mediaType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
