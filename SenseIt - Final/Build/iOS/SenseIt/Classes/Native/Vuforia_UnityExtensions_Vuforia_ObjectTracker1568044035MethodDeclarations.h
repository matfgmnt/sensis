﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTracker
struct ObjectTracker_t1568044035;

// System.Void Vuforia.ObjectTracker::.ctor()
extern "C"  void ObjectTracker__ctor_m3101494475 (ObjectTracker_t1568044035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
