﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"

// System.Net.Configuration.MailSettingsSectionGroup
struct MailSettingsSectionGroup_t25838306;
// System.Net.Configuration.SmtpSection
struct SmtpSection_t3258969749;

// System.Void System.Net.Configuration.MailSettingsSectionGroup::.ctor()
extern "C"  void MailSettingsSectionGroup__ctor_m1757510170 (MailSettingsSectionGroup_t25838306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Configuration.SmtpSection System.Net.Configuration.MailSettingsSectionGroup::get_Smtp()
extern "C"  SmtpSection_t3258969749 * MailSettingsSectionGroup_get_Smtp_m502950223 (MailSettingsSectionGroup_t25838306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
