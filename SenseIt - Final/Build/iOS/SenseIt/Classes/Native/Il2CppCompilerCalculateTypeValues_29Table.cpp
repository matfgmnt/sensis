﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundManager3515346924.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour407765638.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236537.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_DatabaseEntryold2298023384.h"




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (VideoBackgroundManager_t3515346924), -1, sizeof(VideoBackgroundManager_t3515346924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2900[1] = 
{
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mInstance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (VirtualButtonBehaviour_t2515041812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (VuMarkBehaviour_t2060629989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (VuforiaBehaviour_t359035403), -1, sizeof(VuforiaBehaviour_t359035403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2903[1] = 
{
	VuforiaBehaviour_t359035403_StaticFields::get_offset_of_mVuforiaBehaviour_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (WebCamBehaviour_t407765638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (WireframeBehaviour_t2494532455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2905[4] = 
{
	WireframeBehaviour_t2494532455::get_offset_of_lineMaterial_2(),
	WireframeBehaviour_t2494532455::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t2494532455::get_offset_of_LineColor_4(),
	WireframeBehaviour_t2494532455::get_offset_of_mLineMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (WireframeTrackableEventHandler_t1535150527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[1] = 
{
	WireframeTrackableEventHandler_t1535150527::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (WordBehaviour_t3366478421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2908[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DABA519E428DAD9CE185AEB6FA2FE954198E228E8_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (U24ArrayTypeU3D96_t3894236537)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D96_t3894236537 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (DatabaseEntryold_t2298023384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[13] = 
{
	DatabaseEntryold_t2298023384::get_offset_of_SensisID_2(),
	DatabaseEntryold_t2298023384::get_offset_of_contactName_3(),
	DatabaseEntryold_t2298023384::get_offset_of_contactLogo_4(),
	DatabaseEntryold_t2298023384::get_offset_of_contactPhoneNumber1_5(),
	DatabaseEntryold_t2298023384::get_offset_of_contactPhoneNumber2_6(),
	DatabaseEntryold_t2298023384::get_offset_of_contactWebsite1_7(),
	DatabaseEntryold_t2298023384::get_offset_of_contactWebsiteFakeImage_8(),
	DatabaseEntryold_t2298023384::get_offset_of_contactEmailAddress1_9(),
	DatabaseEntryold_t2298023384::get_offset_of_facebookAddress_10(),
	DatabaseEntryold_t2298023384::get_offset_of_nearestLocation_11(),
	DatabaseEntryold_t2298023384::get_offset_of_promotionText_12(),
	DatabaseEntryold_t2298023384::get_offset_of_promotionImage_13(),
	DatabaseEntryold_t2298023384::get_offset_of_twitterAddress_14(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
