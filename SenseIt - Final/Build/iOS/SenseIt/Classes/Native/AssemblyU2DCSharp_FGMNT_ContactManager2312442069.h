﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.String
struct String_t;
// FGMNT.FeedbackManager
struct FeedbackManager_t2969052828;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.ContactManager
struct  ContactManager_t2312442069  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text FGMNT.ContactManager::name
	Text_t356221433 * ___name_2;
	// UnityEngine.UI.Image FGMNT.ContactManager::logo
	Image_t2042527209 * ___logo_3;
	// UnityEngine.UI.Text FGMNT.ContactManager::callText
	Text_t356221433 * ___callText_4;
	// UnityEngine.UI.Text FGMNT.ContactManager::findText
	Text_t356221433 * ___findText_5;
	// UnityEngine.UI.Text FGMNT.ContactManager::mapText
	Text_t356221433 * ___mapText_6;
	// System.String FGMNT.ContactManager::number
	String_t* ___number_7;
	// System.String FGMNT.ContactManager::website
	String_t* ___website_8;
	// System.String FGMNT.ContactManager::address
	String_t* ___address_9;
	// FGMNT.FeedbackManager FGMNT.ContactManager::fbManager
	FeedbackManager_t2969052828 * ___fbManager_10;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(ContactManager_t2312442069, ___name_2)); }
	inline Text_t356221433 * get_name_2() const { return ___name_2; }
	inline Text_t356221433 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(Text_t356221433 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_logo_3() { return static_cast<int32_t>(offsetof(ContactManager_t2312442069, ___logo_3)); }
	inline Image_t2042527209 * get_logo_3() const { return ___logo_3; }
	inline Image_t2042527209 ** get_address_of_logo_3() { return &___logo_3; }
	inline void set_logo_3(Image_t2042527209 * value)
	{
		___logo_3 = value;
		Il2CppCodeGenWriteBarrier(&___logo_3, value);
	}

	inline static int32_t get_offset_of_callText_4() { return static_cast<int32_t>(offsetof(ContactManager_t2312442069, ___callText_4)); }
	inline Text_t356221433 * get_callText_4() const { return ___callText_4; }
	inline Text_t356221433 ** get_address_of_callText_4() { return &___callText_4; }
	inline void set_callText_4(Text_t356221433 * value)
	{
		___callText_4 = value;
		Il2CppCodeGenWriteBarrier(&___callText_4, value);
	}

	inline static int32_t get_offset_of_findText_5() { return static_cast<int32_t>(offsetof(ContactManager_t2312442069, ___findText_5)); }
	inline Text_t356221433 * get_findText_5() const { return ___findText_5; }
	inline Text_t356221433 ** get_address_of_findText_5() { return &___findText_5; }
	inline void set_findText_5(Text_t356221433 * value)
	{
		___findText_5 = value;
		Il2CppCodeGenWriteBarrier(&___findText_5, value);
	}

	inline static int32_t get_offset_of_mapText_6() { return static_cast<int32_t>(offsetof(ContactManager_t2312442069, ___mapText_6)); }
	inline Text_t356221433 * get_mapText_6() const { return ___mapText_6; }
	inline Text_t356221433 ** get_address_of_mapText_6() { return &___mapText_6; }
	inline void set_mapText_6(Text_t356221433 * value)
	{
		___mapText_6 = value;
		Il2CppCodeGenWriteBarrier(&___mapText_6, value);
	}

	inline static int32_t get_offset_of_number_7() { return static_cast<int32_t>(offsetof(ContactManager_t2312442069, ___number_7)); }
	inline String_t* get_number_7() const { return ___number_7; }
	inline String_t** get_address_of_number_7() { return &___number_7; }
	inline void set_number_7(String_t* value)
	{
		___number_7 = value;
		Il2CppCodeGenWriteBarrier(&___number_7, value);
	}

	inline static int32_t get_offset_of_website_8() { return static_cast<int32_t>(offsetof(ContactManager_t2312442069, ___website_8)); }
	inline String_t* get_website_8() const { return ___website_8; }
	inline String_t** get_address_of_website_8() { return &___website_8; }
	inline void set_website_8(String_t* value)
	{
		___website_8 = value;
		Il2CppCodeGenWriteBarrier(&___website_8, value);
	}

	inline static int32_t get_offset_of_address_9() { return static_cast<int32_t>(offsetof(ContactManager_t2312442069, ___address_9)); }
	inline String_t* get_address_9() const { return ___address_9; }
	inline String_t** get_address_of_address_9() { return &___address_9; }
	inline void set_address_9(String_t* value)
	{
		___address_9 = value;
		Il2CppCodeGenWriteBarrier(&___address_9, value);
	}

	inline static int32_t get_offset_of_fbManager_10() { return static_cast<int32_t>(offsetof(ContactManager_t2312442069, ___fbManager_10)); }
	inline FeedbackManager_t2969052828 * get_fbManager_10() const { return ___fbManager_10; }
	inline FeedbackManager_t2969052828 ** get_address_of_fbManager_10() { return &___fbManager_10; }
	inline void set_fbManager_10(FeedbackManager_t2969052828 * value)
	{
		___fbManager_10 = value;
		Il2CppCodeGenWriteBarrier(&___fbManager_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
