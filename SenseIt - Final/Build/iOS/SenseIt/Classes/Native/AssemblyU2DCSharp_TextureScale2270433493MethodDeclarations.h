﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// TextureScale
struct TextureScale_t2270433493;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Object
struct Il2CppObject;

// System.Void TextureScale::.ctor()
extern "C"  void TextureScale__ctor_m3763125110 (TextureScale_t2270433493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::Point(UnityEngine.Texture2D,System.Int32,System.Int32)
extern "C"  void TextureScale_Point_m3656306980 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, int32_t ___newWidth1, int32_t ___newHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::Bilinear(UnityEngine.Texture2D,System.Int32,System.Int32)
extern "C"  void TextureScale_Bilinear_m2561051556 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, int32_t ___newWidth1, int32_t ___newHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::ThreadedScale(UnityEngine.Texture2D,System.Int32,System.Int32,System.Boolean)
extern "C"  void TextureScale_ThreadedScale_m3694362636 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___tex0, int32_t ___newWidth1, int32_t ___newHeight2, bool ___useBilinear3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::BilinearScale(System.Object)
extern "C"  void TextureScale_BilinearScale_m1662616096 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextureScale::PointScale(System.Object)
extern "C"  void TextureScale_PointScale_m639785660 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TextureScale::ColorLerpUnclamped(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t2020392075  TextureScale_ColorLerpUnclamped_m2155218259 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___c10, Color_t2020392075  ___c21, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
