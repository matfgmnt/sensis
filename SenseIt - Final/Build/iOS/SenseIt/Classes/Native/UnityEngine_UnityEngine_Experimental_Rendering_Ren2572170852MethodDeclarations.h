﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;

// System.Boolean UnityEngine.Experimental.Rendering.RenderLoop::PrepareRenderLoop_Internal(UnityEngine.Camera[],System.IntPtr)
extern "C"  bool RenderLoop_PrepareRenderLoop_Internal_m760428110 (Il2CppObject * __this /* static, unused */, CameraU5BU5D_t3079764780* ___cameras0, IntPtr_t ___loopPtr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
