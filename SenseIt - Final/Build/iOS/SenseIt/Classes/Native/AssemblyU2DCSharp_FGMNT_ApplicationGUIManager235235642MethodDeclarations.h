﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FGMNT_DatabaseEntry2505461191.h"

// FGMNT.ApplicationGUIManager
struct ApplicationGUIManager_t235235642;
// FGMNT.DatabaseEntry
struct DatabaseEntry_t2505461191;

// System.Void FGMNT.ApplicationGUIManager::.ctor()
extern "C"  void ApplicationGUIManager__ctor_m1776770317 (ApplicationGUIManager_t235235642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationGUIManager::Awake()
extern "C"  void ApplicationGUIManager_Awake_m1895511710 (ApplicationGUIManager_t235235642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationGUIManager::showSearchScreen()
extern "C"  void ApplicationGUIManager_showSearchScreen_m3618681256 (ApplicationGUIManager_t235235642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationGUIManager::showContactScreen(FGMNT.DatabaseEntry)
extern "C"  void ApplicationGUIManager_showContactScreen_m2921876635 (ApplicationGUIManager_t235235642 * __this, DatabaseEntry_t2505461191 * ___business0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationGUIManager::ShowFeedbackScreen()
extern "C"  void ApplicationGUIManager_ShowFeedbackScreen_m552456411 (ApplicationGUIManager_t235235642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FGMNT.ApplicationGUIManager::EraseFeedbackText()
extern "C"  void ApplicationGUIManager_EraseFeedbackText_m231164301 (ApplicationGUIManager_t235235642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
