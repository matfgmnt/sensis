﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Net.CredentialCache/CredentialCacheForHostKey
struct CredentialCacheForHostKey_t1860630919;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

// System.String System.Net.CredentialCache/CredentialCacheForHostKey::get_Host()
extern "C"  String_t* CredentialCacheForHostKey_get_Host_m1188776459 (CredentialCacheForHostKey_t1860630919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.CredentialCache/CredentialCacheForHostKey::get_Port()
extern "C"  int32_t CredentialCacheForHostKey_get_Port_m578197661 (CredentialCacheForHostKey_t1860630919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheForHostKey::get_AuthType()
extern "C"  String_t* CredentialCacheForHostKey_get_AuthType_m1291656435 (CredentialCacheForHostKey_t1860630919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.CredentialCache/CredentialCacheForHostKey::GetHashCode()
extern "C"  int32_t CredentialCacheForHostKey_GetHashCode_m113537712 (CredentialCacheForHostKey_t1860630919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.CredentialCache/CredentialCacheForHostKey::Equals(System.Object)
extern "C"  bool CredentialCacheForHostKey_Equals_m3244161244 (CredentialCacheForHostKey_t1860630919 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheForHostKey::ToString()
extern "C"  String_t* CredentialCacheForHostKey_ToString_m2932559256 (CredentialCacheForHostKey_t1860630919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
