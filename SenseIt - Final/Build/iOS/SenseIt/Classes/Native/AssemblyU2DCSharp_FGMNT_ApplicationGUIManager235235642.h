﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// FGMNT.ContactManager
struct ContactManager_t2312442069;

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FGMNT.ApplicationGUIManager
struct  ApplicationGUIManager_t235235642  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject FGMNT.ApplicationGUIManager::searchScreen
	GameObject_t1756533147 * ___searchScreen_2;
	// UnityEngine.GameObject FGMNT.ApplicationGUIManager::contactScreen
	GameObject_t1756533147 * ___contactScreen_3;
	// UnityEngine.GameObject FGMNT.ApplicationGUIManager::introScreen
	GameObject_t1756533147 * ___introScreen_4;
	// UnityEngine.GameObject FGMNT.ApplicationGUIManager::websiteScreen
	GameObject_t1756533147 * ___websiteScreen_5;
	// UnityEngine.GameObject FGMNT.ApplicationGUIManager::waitingScreen
	GameObject_t1756533147 * ___waitingScreen_6;
	// UnityEngine.GameObject FGMNT.ApplicationGUIManager::feedbackScreen
	GameObject_t1756533147 * ___feedbackScreen_7;
	// UnityEngine.UI.Text FGMNT.ApplicationGUIManager::feedbackText
	Text_t356221433 * ___feedbackText_8;
	// FGMNT.ContactManager FGMNT.ApplicationGUIManager::contactManager
	ContactManager_t2312442069 * ___contactManager_9;

public:
	inline static int32_t get_offset_of_searchScreen_2() { return static_cast<int32_t>(offsetof(ApplicationGUIManager_t235235642, ___searchScreen_2)); }
	inline GameObject_t1756533147 * get_searchScreen_2() const { return ___searchScreen_2; }
	inline GameObject_t1756533147 ** get_address_of_searchScreen_2() { return &___searchScreen_2; }
	inline void set_searchScreen_2(GameObject_t1756533147 * value)
	{
		___searchScreen_2 = value;
		Il2CppCodeGenWriteBarrier(&___searchScreen_2, value);
	}

	inline static int32_t get_offset_of_contactScreen_3() { return static_cast<int32_t>(offsetof(ApplicationGUIManager_t235235642, ___contactScreen_3)); }
	inline GameObject_t1756533147 * get_contactScreen_3() const { return ___contactScreen_3; }
	inline GameObject_t1756533147 ** get_address_of_contactScreen_3() { return &___contactScreen_3; }
	inline void set_contactScreen_3(GameObject_t1756533147 * value)
	{
		___contactScreen_3 = value;
		Il2CppCodeGenWriteBarrier(&___contactScreen_3, value);
	}

	inline static int32_t get_offset_of_introScreen_4() { return static_cast<int32_t>(offsetof(ApplicationGUIManager_t235235642, ___introScreen_4)); }
	inline GameObject_t1756533147 * get_introScreen_4() const { return ___introScreen_4; }
	inline GameObject_t1756533147 ** get_address_of_introScreen_4() { return &___introScreen_4; }
	inline void set_introScreen_4(GameObject_t1756533147 * value)
	{
		___introScreen_4 = value;
		Il2CppCodeGenWriteBarrier(&___introScreen_4, value);
	}

	inline static int32_t get_offset_of_websiteScreen_5() { return static_cast<int32_t>(offsetof(ApplicationGUIManager_t235235642, ___websiteScreen_5)); }
	inline GameObject_t1756533147 * get_websiteScreen_5() const { return ___websiteScreen_5; }
	inline GameObject_t1756533147 ** get_address_of_websiteScreen_5() { return &___websiteScreen_5; }
	inline void set_websiteScreen_5(GameObject_t1756533147 * value)
	{
		___websiteScreen_5 = value;
		Il2CppCodeGenWriteBarrier(&___websiteScreen_5, value);
	}

	inline static int32_t get_offset_of_waitingScreen_6() { return static_cast<int32_t>(offsetof(ApplicationGUIManager_t235235642, ___waitingScreen_6)); }
	inline GameObject_t1756533147 * get_waitingScreen_6() const { return ___waitingScreen_6; }
	inline GameObject_t1756533147 ** get_address_of_waitingScreen_6() { return &___waitingScreen_6; }
	inline void set_waitingScreen_6(GameObject_t1756533147 * value)
	{
		___waitingScreen_6 = value;
		Il2CppCodeGenWriteBarrier(&___waitingScreen_6, value);
	}

	inline static int32_t get_offset_of_feedbackScreen_7() { return static_cast<int32_t>(offsetof(ApplicationGUIManager_t235235642, ___feedbackScreen_7)); }
	inline GameObject_t1756533147 * get_feedbackScreen_7() const { return ___feedbackScreen_7; }
	inline GameObject_t1756533147 ** get_address_of_feedbackScreen_7() { return &___feedbackScreen_7; }
	inline void set_feedbackScreen_7(GameObject_t1756533147 * value)
	{
		___feedbackScreen_7 = value;
		Il2CppCodeGenWriteBarrier(&___feedbackScreen_7, value);
	}

	inline static int32_t get_offset_of_feedbackText_8() { return static_cast<int32_t>(offsetof(ApplicationGUIManager_t235235642, ___feedbackText_8)); }
	inline Text_t356221433 * get_feedbackText_8() const { return ___feedbackText_8; }
	inline Text_t356221433 ** get_address_of_feedbackText_8() { return &___feedbackText_8; }
	inline void set_feedbackText_8(Text_t356221433 * value)
	{
		___feedbackText_8 = value;
		Il2CppCodeGenWriteBarrier(&___feedbackText_8, value);
	}

	inline static int32_t get_offset_of_contactManager_9() { return static_cast<int32_t>(offsetof(ApplicationGUIManager_t235235642, ___contactManager_9)); }
	inline ContactManager_t2312442069 * get_contactManager_9() const { return ___contactManager_9; }
	inline ContactManager_t2312442069 ** get_address_of_contactManager_9() { return &___contactManager_9; }
	inline void set_contactManager_9(ContactManager_t2312442069 * value)
	{
		___contactManager_9 = value;
		Il2CppCodeGenWriteBarrier(&___contactManager_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
